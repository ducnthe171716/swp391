var video = document.getElementById("myVideo");

video.addEventListener('ended', () => {
    var contentId = video.getAttribute("data-content-id"); // Lấy contentId của video hiện tại
    console.log("contend index"+contentId);
    var token = document.querySelector("meta[name='_csrf']").getAttribute("content");
        var header = document.querySelector("meta[name='_csrf_header']").getAttribute("content");

    var checkboxes = document.querySelectorAll(".btncheckbox");
    checkboxes.forEach((checkbox) => {
        var checkboxContentId = checkbox.getAttribute("data-content-id");
        console.log("checkbox index" + checkboxContentId);
        // Kiểm tra nếu checkbox chưa được đánh dấu và contentId của checkbox trùng khớp với contentId của video hiện tại

        if (!checkbox.checked && checkboxContentId === contentId) {
            checkbox.checked = true;
        }
    });
    sendVideoStatusToServer(contentId,token, header);
});
function sendVideoStatusToServer(contentId,token, header) {
    fetch("/coursecontent/saveVideoStatus", {
    credentials: 'same-origin',
        method: "POST",
        headers: {
            [header]: token,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({'contentId': contentId}),
    })
    .then(response => response.text())
    .then(data => {
        console.log(data.message);
    })
    .catch(error => {
        console.error("Lỗi khi gửi yêu cầu AJAX: " + error);
    });
}