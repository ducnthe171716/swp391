package com.swp391.onlinequiz.ExceptionHandling;

public class UsersNotfoundException extends Exception{
    public UsersNotfoundException(String message ) {
        super(message);
    }
}
