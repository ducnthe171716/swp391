package com.swp391.onlinequiz.dto;

import com.swp391.onlinequiz.model.Users;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class UsersDTO {
    private Integer userId;
    private String fullName;
    private String email;
    private Boolean gender;
    private LocalDateTime dob;
    private String address;
    private String userName;
    private String role;
    private LocalDate registerDob;
    private String avatar;
    private Boolean isenabled;
    private String password;



    public UsersDTO(Users u){
        this.userId = u.getUserid();
        this.fullName = u.getFullname();
        this.email = u.getEmail();
        this.dob = u.getDob();
        this.address = u.getAddress();
        this.userName = u.getUsername();
        this.role = u.getRole();
        this.registerDob = u.getRegisterdob();
        this.gender = u.getGender();
        this.avatar = u.getAvatar();
        this.isenabled = u.getIsenabled();
        this.password = u.getPassword();

    }

    @Override
    public String toString() {
        return "UsersDTO [fullName=" + fullName + ", email=" + email + ", gender=" + gender + ", dob=" + dob
                + ", address=" + address + ", userName=" + userName + ", role=" + role + ", registerDob=" + registerDob
                + "]";
    }


}
