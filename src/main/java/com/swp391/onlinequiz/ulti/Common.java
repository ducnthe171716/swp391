package com.swp391.onlinequiz.ulti;

import jakarta.persistence.Query;

import java.util.Map;
import java.util.Set;

public class Common {
    public static void setParams(Query query, Map<String, Object> params) {
        if (params != null && !params.isEmpty()) {
            Set<Map.Entry<String, Object>> set = params.entrySet();
            for (Map.Entry<String, Object> obj : set) {
                if (obj.getValue() == null) query.setParameter(obj.getKey(), ""); else query.setParameter(obj.getKey(), obj.getValue());
            }
        }
    }
}
