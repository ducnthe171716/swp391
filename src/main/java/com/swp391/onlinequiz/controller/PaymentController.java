package com.swp391.onlinequiz.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import com.swp391.onlinequiz.model.*;
import com.swp391.onlinequiz.repository.ContentStatusDetailRepository;
import com.swp391.onlinequiz.repository.CoursecontentRepository;
import com.swp391.onlinequiz.repository.SectionDetailRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.swp391.onlinequiz.config.PaypalPaymentIntent;
import com.swp391.onlinequiz.config.PaypalPaymentMethod;
import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.serviceimpl.CourseServiceImpl;
import com.swp391.onlinequiz.serviceimpl.PaypalService;
import com.swp391.onlinequiz.serviceimpl.UserServiceImpl;
import com.swp391.onlinequiz.ulti.PaymentUtils;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
@RequestMapping("/payment")
public class PaymentController {
    public static final String URL_PAYPAL_SUCCESS = "payment/success?courseid=";
	public static final String URL_PAYPAL_CANCEL = "payment/cancel?courseid=";
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private PaypalService paypalService;

    @Autowired
    private CourseServiceImpl courseServiceImpl;

	@Autowired
	private EnrollRepository enrollRepository;

	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private ContentStatusDetailRepository contentStatusDetailRepository;
	@Autowired
	private SectionDetailRepository sectionDetailRepository;
	@Autowired
	private CoursecontentRepository coursecontentRepository;

	public PaymentController(ContentStatusDetailRepository contentStatusDetailRepository, SectionDetailRepository sectionDetailRepository , CoursecontentRepository coursecontentRepository) {
		this.contentStatusDetailRepository = contentStatusDetailRepository;
		this.sectionDetailRepository = sectionDetailRepository;
		this.coursecontentRepository = coursecontentRepository;
	}

	@GetMapping("")
	public ModelAndView index(@RequestParam(name = "courseid") Integer courseid, HttpSession session){
		ModelAndView mv = new ModelAndView("pages/client/payment-index");
        //check session user
		if(session.getAttribute("user") == null){

			mv.setViewName("redirect:/client/signup");
		}
        
        Course course = courseServiceImpl.getCourseByID(courseid);
        mv.addObject("course", course);
        return mv;
    }
	
	@PostMapping("/pay")
	public String pay(HttpServletRequest request,@RequestParam("price") double price, @RequestParam(name = "courseid") Integer courseid, HttpSession session ){
		String cancelUrl = PaymentUtils.getBaseURL(request) + "/" + URL_PAYPAL_CANCEL+courseid;
		String successUrl = PaymentUtils.getBaseURL(request) + "/" + URL_PAYPAL_SUCCESS+courseid+"&price="+price;
		try {
			Payment payment = paypalService.createPayment(
					price, 
					"USD", 
					PaypalPaymentMethod.paypal, 
					PaypalPaymentIntent.sale,
					"payment description", 
					cancelUrl, 
					successUrl
					);
			for(Links links : payment.getLinks()){
				if(links.getRel().equals("approval_url")){
					return "redirect:" + links.getHref();
                }
            }
			
        } catch (PayPalRESTException e) {
			log.error(e.getMessage());
        }
		
		return "redirect:/";
    }

	@GetMapping("/cancel")
	public ModelAndView cancelPay(@RequestParam("courseid") Integer courseid){
		ModelAndView mv = new ModelAndView("redirect:/courseDetail/"+courseid);
		
		return mv;
    }

	@GetMapping("/success")
	public ModelAndView successPay(@RequestParam("paymentId") String paymentId, 
	@RequestParam("PayerID") String payerId, 
	@RequestParam("courseid") Integer courseid, 
	@RequestParam("price") Double price,
	HttpSession session){
		UsersDTO userdto = (UsersDTO)session.getAttribute("user");
		Course course = courseServiceImpl.getCourseByID(courseid);
		Users user = userServiceImpl.getUsersByUserDTOid(userdto.getUserId());
		//create enroll log
		Enroll enroll = new Enroll();
		enroll.setUser(user);
		enroll.setCourse(course);
		enroll.setCheckout(price);
		enroll.setEnrolldate(LocalDateTime.now());
		enroll.setProcessstatus(false);
		enroll.setEmailpaypaladdress("");

		enrollRepository.saveAndFlush(enroll);
		Set<Coursesection> listcoursesection = enroll.getCourse().getCoursesectionSet();
		System.out.println("enrollid" + enroll.getEnrollid());
		for(var coursesection : listcoursesection){
			Sectiondetail sectiondetail = new Sectiondetail();
			sectiondetail.setEnroll(enroll);
			sectiondetail.setCoursesection(coursesection);
			System.out.println("course section id" + coursesection.getSectionId());
			sectionDetailRepository.save(sectiondetail);
		}
		List<Sectiondetail> listSection = sectionDetailRepository.findByEnroll_Enrollid(enroll.getEnrollid());
		for (var sectionDetail: listSection) {
			List<Coursecontent> listcontent = coursecontentRepository.getCourseContentsBySectionID(sectionDetail.getCoursesection().getSectionId());
			for(var coursecontent:listcontent){
				ContentStatusDetail contentStatus = new ContentStatusDetail();
				contentStatus.setSectiondetail(sectionDetail);
				contentStatus.setStatus(false);
				contentStatus.setCoursecontent(coursecontent);
				contentStatusDetailRepository.save(contentStatus);
			}
		}
		try {
			Payment payment = paypalService.executePayment(paymentId, payerId);
			if(payment.getState().equals("approved")){
				ModelAndView mv = new ModelAndView("pages/client/payment-success");
				mv.addObject("courseid", courseid);
				return mv;
			}
				
			
        } catch (PayPalRESTException e) {
			log.error(e.getMessage());
        }
		return new ModelAndView("redirect:/");
    }
}

