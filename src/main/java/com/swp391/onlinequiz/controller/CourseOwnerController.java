package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.*;
import com.swp391.onlinequiz.repository.CourseRepository;
import com.swp391.onlinequiz.repository.CourseownerRepository;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.repository.ProgressStatusRepository;
import com.swp391.onlinequiz.service.CategoryService;
import com.swp391.onlinequiz.service.CourseService;
import com.swp391.onlinequiz.service.CourseownerService;
import com.swp391.onlinequiz.service.EnrollService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("")


public class CourseOwnerController {
    @Autowired
    private CourseownerRepository courseownerRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private CourseService courseService;

    @Autowired
    private ProgressStatusRepository progressStatusRepository;

    @GetMapping("/courseowner/home")
    public String showDashboard(Model model, HttpSession session,
                                @RequestParam(name = "timeInterval", required = false) String timeInterval) {
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        int teacherId = user.getUserId();

        Courseowner c = courseownerRepository.findById(teacherId).get();
        List<Object[]> revenueDataByTime = new ArrayList<>();
        List<Object[]> countUser = new ArrayList<>();
        long totalStudents = 0;
        double revenue1 = 0.0;

        if (timeInterval == null) {
            // Trường hợp không có timeInterval, truy vấn theo mặc định
            revenueDataByTime = courseownerRepository.findCourseOwnerStatistics(teacherId);
            countUser = courseownerRepository.findUsersRegisteredSince(teacherId);
        } else {
            // Trường hợp có timeInterval, sử dụng giá trị từ form
            if ("3d".equals(timeInterval)) {
                revenueDataByTime = courseownerRepository.findCourseOwnerStatisticsByTime(teacherId, "3");
                countUser = courseownerRepository.findUsersRegisterByDay(teacherId, "3");
            } else if ("1w".equals(timeInterval)) {
                revenueDataByTime = courseownerRepository.findCourseOwnerStatisticsByTime(teacherId, "7");
                countUser = courseownerRepository.findUsersRegisterByDay(teacherId, "7");
            } else if ("6m".equals(timeInterval)) {
                revenueDataByTime = courseownerRepository.findCourseOwnerStatisticsByTime(teacherId, "180");
                countUser = courseownerRepository.findUsersRegisterByTime(teacherId, "180");
            } else if ("all".equals(timeInterval)) {
                return "redirect:/courseowner/home";
            }
        }

        if (!revenueDataByTime.isEmpty()) {
            Object[] result = revenueDataByTime.get(0);
            totalStudents = ((Number) result[4]).longValue();
            revenue1 = ((Number) result[5]).doubleValue();
        }
//        DecimalFormat df = new DecimalFormat("#,###.#");
//        double revenue = Double.parseDouble(df.format(revenue1));
        model.addAttribute("results", courseownerRepository.listCheckOut(teacherId));
        model.addAttribute("revenueDataByTime", revenueDataByTime);
        model.addAttribute("totalstudents", totalStudents);
        model.addAttribute("revenue", revenue1);
        model.addAttribute("list", courseownerRepository.listtop5CheckOut(teacherId));
        model.addAttribute("totalFeedback", courseownerRepository.findTotalFeedback(teacherId));
        model.addAttribute("countUser", countUser);
        model.addAttribute("selectedtime", timeInterval);

        model.addAttribute("courseowner", c);
        return "pages/courseOwner/statitics";
    }

    //
    @GetMapping("/courseowner/mysubject")
    public String showListSubjectbyFilter(Model model, HttpSession session, @RequestParam(name = "flt") Integer flt, 
    @RequestParam(value = "page",required = false) Optional<Integer> page,
    @RequestParam(value = "size",required = false) Optional<Integer> size) {
        int pageNum = page.orElse(1);
        int pageSize = size.orElse(5);
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        int teacherId = user.getUserId();
        List<Course> courseList = new ArrayList<>();
        if(flt  == 4){
            courseList = courseRepository.findByCourseowner(courseownerRepository.findById(teacherId).get());
        }else{
            ProgressStatus current = progressStatusRepository.findByStatusId(flt).get(0);
            courseList = courseRepository.findByCourseownerAndStatus(courseownerRepository.findById(teacherId).get(), current);    
        }

        Page<Course> courses = courseService.findPaginated(PageRequest.of(pageNum - 1, pageSize),courseList);
        model.addAttribute("courses", courses);
        int totalPages = courses.getTotalPages();
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("pageNum",pageNum);
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        
        
        model.addAttribute("courses", courses);

        // Lặp qua danh sách khóa học và truy vấn revenueData cho từng khóa học
        List<Object[]> revenueData = new ArrayList<>();

        
            for (Course course : courses) {
                
                    List<Object[]> courseRevenueData = courseownerRepository
                            .findTotalRevenumBy1Course(course.getCourseid());
                    revenueData.addAll(courseRevenueData);
                

            }
            List<Object[]> revenueDataMonth = new ArrayList<>();
            for (Course course : courses) {
                
                    List<Object[]> courseRevenueDataMonth = courseownerRepository
                            .findTotalRevenumByMonth(course.getCourseid());
                    revenueDataMonth.addAll(courseRevenueDataMonth);
                
            }
            model.addAttribute("revenueDataMonth", revenueDataMonth);
            model.addAttribute("revenueData", revenueData);
        

        // model.addAttribute("listRegister",
        // courseownerRepository.listCheckOutByCourse(teacherId));
        
        model.addAttribute("flt", flt);
        return "pages/courseOwner/courses";
    }

    @GetMapping("/courseowner/listSubject")
    public String showListSubject(Model model, HttpSession session, @RequestParam(value = "page",required = false) Optional<Integer> page,
                                  @RequestParam(value = "size",required = false) Optional<Integer> size) {
        int pageNum = page.orElse(1);
        int pageSize = size.orElse(5);
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        int teacherId = user.getUserId();
        List<Course> courseList = courseRepository.findByCourseowner(courseownerRepository.findById(teacherId).get());
        Page<Course> courses = courseService.findPaginated(PageRequest.of(pageNum - 1, pageSize),courseList);
        model.addAttribute("courses", courses);
        int totalPages = courses.getTotalPages();
        model.addAttribute("totalPages",totalPages);
        model.addAttribute("pageNum",pageNum);
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        // Lặp qua danh sách khóa học và truy vấn revenueData cho từng khóa học
        List<Object[]> revenueData = new ArrayList<>();
        for (Course course : courses) {
            List<Object[]> courseRevenueData = courseownerRepository.findTotalRevenumBy1Course(course.getCourseid());
            revenueData.addAll(courseRevenueData);
        }
        List<Object[]> revenueDataMonth = new ArrayList<>();
        for (Course course : courses) {
            List<Object[]> courseRevenueDataMonth = courseownerRepository.findTotalRevenumByMonth(course.getCourseid());
            revenueDataMonth.addAll(courseRevenueDataMonth);
        }
        System.out.println(revenueDataMonth);
        //model.addAttribute("listRegister", courseownerRepository.listCheckOutByCourse(teacherId));
        model.addAttribute("revenueDataMonth", revenueDataMonth);
        model.addAttribute("revenueData", revenueData);
        model.addAttribute("flt", 4);

        return "pages/courseOwner/courses";
    }

    @GetMapping("/courseowner/revenue-report")
    public String showRevenueReport(Model model, HttpSession session, @RequestParam(name = "page", defaultValue = "1") int page,
                                    @RequestParam(name = "year", defaultValue = "all", required = false) String year,
                                    @RequestParam(name = "month", defaultValue = "all", required = false) String month,
                                    @RequestParam(name = "courseName", defaultValue = "",required = false) String courseName) {
        UsersDTO user = (UsersDTO) session.getAttribute("user");
//        Page<Enroll> list = enrollService.listRevenueByCourseOwnerID(user.getUserId(), page, size);

        List<Object[]> results = courseownerRepository.listCheckOut(user.getUserId());
        if (!courseName.isEmpty()) {
            if (!year.equals("all")) {
                if (!month.equals("all")) {
                    results = courseownerRepository.listCheckOutByName(user.getUserId(), courseName);
                    results = courseownerRepository.listCheckOutByMonth(user.getUserId(), Integer.parseInt(month));
                    results = courseownerRepository.listCheckOutByYear(user.getUserId(), Integer.parseInt(year));
                } else if (month.equals("all")) {
                    results = courseownerRepository.listCheckOutByName(user.getUserId(), courseName);
                    results = courseownerRepository.listCheckOutByYear(user.getUserId(), Integer.parseInt(year));
                }
            } else if (year.equals("all")) {
                if (!month.equals("all")) {
                    results = courseownerRepository.listCheckOutByName(user.getUserId(), courseName);
                    results = courseownerRepository.listCheckOutByMonth(user.getUserId(), Integer.parseInt(month));
                } else if (month.equals("all")) {
                    results = courseownerRepository.listCheckOutByName(user.getUserId(), courseName);
                }

            }
        } else {
            if (!year.equals("all")) {
                if (!month.equals("all")) {
                    results = courseownerRepository.listCheckOutByMonth(user.getUserId(), Integer.parseInt(month));
                    results = courseownerRepository.listCheckOutByYear(user.getUserId(), Integer.parseInt(year));
                } else if (month.equals("all")) {
                    results = courseownerRepository.listCheckOutByYear(user.getUserId(), Integer.parseInt(year));
                }
            } else if (year.equals("all")) {
                if (!month.equals("all")) {
                    results = courseownerRepository.listCheckOutByMonth(user.getUserId(), Integer.parseInt(month));
                } else if (month.equals("all")) {
                    results = courseownerRepository.listCheckOut(user.getUserId());
                }
            }

        }

//        model.addAttribute("results", results);
        List<Object[]> listForCurrentPage = new ArrayList<>();
        int totalPages = 0;
        if (results != null) {
            int pageSize = 6; // Số lượng khóa học trên mỗi trang
            totalPages = (int) Math.ceil((double) results.size() / pageSize);

            // Kiểm tra xem trang yêu cầu có hợp lệ không
            if (page < 1 || page > totalPages) {
                // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                return "redirect:/courseowner/revenue-report?page=1"; // Chuyển hướng về trang đầu tiên
            }
            // Xác định các loz cho trang hiện tại
            int startIndex = (page - 1) * pageSize;
            int endIndex = Math.min(startIndex + pageSize, results.size());
            listForCurrentPage = results.subList(startIndex, endIndex);
        }


        model.addAttribute("results", listForCurrentPage);
        model.addAttribute("pageNum", totalPages);
        model.addAttribute("currentPage", page);
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("courseName", courseName);
        return "redirect:/courseowner/revenue-report?courseName="+courseName+"&month="+month+"&year="+year+"&page="+page;
    }

    @GetMapping("/courseowner/time/{timeParam}")
    public String showEnrollByMonth(@PathVariable("timeParam") String timeParam, Model model, HttpSession session,
                                    @RequestParam(name = "page", defaultValue = "1") int page,
                                    @RequestParam(name = "courseName", defaultValue = "0") String courseName) {
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        int teacherId = user.getUserId();
        List<Object[]> results = courseownerRepository.showEnrollByMonth(teacherId, timeParam);


        List<Object[]> listForCurrentPage = new ArrayList<>();
        int totalPages = 0;
        if (results != null) {
            int pageSize = 20   ; // Số lượng khóa học trên mỗi trang
            totalPages = (int) Math.ceil((double) results.size() / pageSize);

            // Kiểm tra xem trang yêu cầu có hợp lệ không
            if (page < 1 || page > totalPages) {
                // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                return "redirect:/courseowner/time?page=1"; // Chuyển hướng về trang đầu tiên
            }
            // Xác định các loz cho trang hiện tại
            int startIndex = (page - 1) * pageSize;
            int endIndex = Math.min(startIndex + pageSize, results.size());
            listForCurrentPage = results.subList(startIndex, endIndex);
        }
        List<Object[]> CourseName = new ArrayList<>();
        List<Object[]> countCourse = new ArrayList<>();
        String totalCourse = "";
        int count = 0;

        if (!CourseName.isEmpty()) {
            Object[] result = CourseName.get(0);
            totalCourse = result[2].toString();
            count = ((Number) result[3]).intValue();
        }
        // Trong Controller hoặc Service của bạn
        List<String> colors = Arrays.asList("cornflowerblue",
                "olivedrab",
                "orange",
                "tomato",
                "crimson",
                "purple",
                "turquoise",
                "forestgreen",
                "navy");
        model.addAttribute("colors", colors);

        model.addAttribute("CourseName", courseownerRepository.competeCoure(teacherId, timeParam));
        model.addAttribute("countCourse",courseownerRepository.competeCoure(teacherId, timeParam));
        model.addAttribute("results", listForCurrentPage);
        model.addAttribute("pageNum", totalPages);
        model.addAttribute("currentPage", page);
        model.addAttribute("anotherPie", courseownerRepository.competeCategory(teacherId, timeParam));
        model.addAttribute("pie", courseownerRepository.competeCoure(teacherId, timeParam));
        model.addAttribute("totalRevenueMonth",courseownerRepository.showTotal(teacherId, timeParam));
        return "pages/courseOwner/revenue-details";
    }


    @GetMapping("courseowner/course-checkout/{courseid}")
    public String showCourseCheckout(@PathVariable("courseid") int courseid, Model model,
                                     HttpSession session, @RequestParam(name = "page", defaultValue = "1") int page,
                                     @RequestParam(name = "year", defaultValue = "all") String year,
                                     @RequestParam(name = "month", defaultValue = "0") String month) {
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        int teacherId = user.getUserId();
        List<Object[]> results = courseownerRepository.listCheckOutByCourse(courseid);
        System.out.println(results);
        List<Object[]> listForCurrentPage = new ArrayList<>();
        int totalPages = 0;
        if (results != null) {
            int pageSize = 3; // Số lượng khóa học trên mỗi trang
            totalPages = (int) Math.ceil((double) results.size() / pageSize);

            // Kiểm tra xem trang yêu cầu có hợp lệ không
            if (page < 1 || page > totalPages) {
                // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                return "redirect:/courseowner/course-checkout?page=1"; // Chuyển hướng về trang đầu tiên
            }
            // Xác định các loz cho trang hiện tại
            int startIndex = (page - 1) * pageSize;
            int endIndex = Math.min(startIndex + pageSize, results.size());
            listForCurrentPage = results.subList(startIndex, endIndex);
            if (year.equals("all")) {
                if (!month.equals("0")) {
                    results = courseownerRepository.listCheckOutByMonth(courseid, Integer.parseInt(month));
                }
            } else if (!year.equals("all")) {
                if (month.equals("0")) {
                    results = courseownerRepository.listCheckOutByYear(courseid, Integer.parseInt(year));
                } else if (!month.equals("0")) {
                    results = courseownerRepository.listCheckOutByMonth(courseid, Integer.parseInt(month));
                    results = courseownerRepository.listCheckOutByYear(courseid, Integer.parseInt(year));
                }

            }
        }
        model.addAttribute("results", listForCurrentPage);
        model.addAttribute("pageNum", totalPages);
        model.addAttribute("currentPage", page);
        model.addAttribute("year", year);
        model.addAttribute("month", month);

        return "pages/courseOwner/enroll-course-detail";
    }


}
