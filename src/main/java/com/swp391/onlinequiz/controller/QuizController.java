package com.swp391.onlinequiz.controller;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Myquizreport;
import com.swp391.onlinequiz.model.Quiz;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.MyquizreportRepository;
import com.swp391.onlinequiz.serviceimpl.MyQuizReportImpl;
import com.swp391.onlinequiz.serviceimpl.QuizServiceImpl;
import com.swp391.onlinequiz.serviceimpl.UserServiceImpl;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * QuizController
 */
@Controller
@RequestMapping(path = "/quiz")
public class QuizController {

    @Autowired
    private QuizServiceImpl quizServiceImpl;

    @Autowired
    private MyQuizReportImpl myQuizReportImpl;

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private MyquizreportRepository myquizreportRepository;

    @GetMapping("/doquiz")
    public ModelAndView showQuizPage(@RequestParam(name = "qrpid") Integer qrpid,
                                                                                @RequestParam(name = "qid") Integer qid,
                                                                                HttpServletResponse response, 
                                                                                HttpServletRequest request, HttpSession session) {
        ModelAndView mv = new ModelAndView("pages/client/doquiz");
        //check for invalid access
        if(session.getAttribute("user")!=null){
            UsersDTO sessionu = (UsersDTO)session.getAttribute("user");
            if(myquizreportRepository.findByMyquizreportid(qrpid).size() == 0 || quizServiceImpl.getQuizByQuizId(qid) == null){
                mv.setViewName("pages/client/error-404");
                return mv;
            }else if(myquizreportRepository.findByMyquizreportid(qrpid).get(0).getUser().getUserid() != sessionu.getUserId()){
                mv.setViewName("pages/client/error-404");
                return mv;
            }  
        }else{
            mv.setViewName("pages/client/error-404");
            return mv;
        }
        
        //check status of quizreport
        for (Cookie cookie : request.getCookies()) {
            if(cookie.getName().compareTo("sta"+qrpid) == 0){
                if(cookie.getValue().compareTo("submited") == 0){
                    mv.setViewName("redirect:/quiz/reviewq?quizid=" + qid);
                    return mv;
                }else if(cookie.getValue().compareTo("ondoing") == 0){
                    // in this 
                    Myquizreport ondoingQuiz = myquizreportRepository.findById(qrpid).get();
                    mv.addObject("quizreport", ondoingQuiz);
                    mv.addObject("qrpid", qrpid);
                    return mv;
                }
            }
        }

        Quiz quiz =quizServiceImpl.getQuizByQuizId(qid);
        //login user       

        Users user = userServiceImpl.getUsersByUserDTOid(((UsersDTO)session.getAttribute("user")).getUserId());
        //get quizreport
        //create new quiz report
        
        //

        Myquizreport newMyQuizReport = myQuizReportImpl.createMyQuizReport(quiz, user);  
    
        if (quiz != null) {
            //add cookie for status
            String qcookie = "ondoing";
            String ncoolie = "sta"+String.valueOf(newMyQuizReport.getMyquizreportid());
            Cookie cookie = new Cookie(ncoolie, qcookie);
            cookie.setMaxAge(60*5);
            response.addCookie(cookie);
        }
        mv.addObject("quizreport", newMyQuizReport);
        mv.addObject("qrpid", qrpid);
        return mv;
    }



    


    @GetMapping("/reviewq")
    public ModelAndView viewQuiz(@RequestParam(name = "quizid") Integer qid, HttpSession session, HttpServletRequest request) {
        //back
        String referer = "";
        if(session.getAttribute("back") != null){
            session.removeAttribute("back");

        }else{
            referer = request.getHeader("Referer");
            session.setAttribute("back", referer);

        }

        ModelAndView mv = new ModelAndView("pages/client/reviewquiz");

        //quiz
        Quiz quiz = quizServiceImpl.getQuizByQuizId(qid);
        //login user
        Users user = new Users();
        if(quiz == null){
            mv.setViewName("pages/client/error-404");
            return mv;
        }
        try {
            user = userServiceImpl.getUsersByUserDTOid(((UsersDTO)session.getAttribute("user")).getUserId());
        } catch (Exception e) {
            mv.setViewName("pages/client/error-404");
            return mv;
        }

        //get quizreport
        if(myQuizReportImpl.getMyQuizReport(user, quiz).size() == 0){
            Myquizreport newreport = new Myquizreport(user, quiz, null, LocalDateTime.now());
            myquizreportRepository.save(newreport);
        }
        Myquizreport lastreport = myQuizReportImpl.getMyQuizReport(user, quiz).get(0);

        // this is for test the fontend
        //Myquizreport lastreport = myQuizReportImpl.getMyQuizReportWithNoUser(quiz);
        mv.addObject("back", referer);
        mv.addObject("lastreport", lastreport);

        return mv;
    }


    //submit request
    @PostMapping("/submit")
    public ModelAndView showFinalQuizResult(HttpServletRequest request, HttpSession session, HttpServletResponse response) {
        
        //Integer remainingtime = Integer.parseInt(request.getParameter("remainingTime"));
        String referer = session.getAttribute("back").toString();


        ModelAndView mv = new ModelAndView("pages/client/quiz-result");
        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, List<String>> resultdata = new HashMap<String, List<String>>();
        
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            //
            //
            if (paramName.startsWith("q") && !paramName.equals("qrpid")) {
                String[] values = request.getParameterValues(paramName);

                resultdata.put(paramName, Arrays.asList(values));


            }

        }
        String[] flagq = request.getParameterValues("flagquestion");

        //restore the quiz report
        
        Users user = userServiceImpl.getUsersByUserDTOid(((UsersDTO)session.getAttribute("user")).getUserId());
        Integer qrpid = Integer.parseInt(request.getParameter("qrpid"));


        

        myQuizReportImpl.restoreQuizReport(user, resultdata, flagq);
        Myquizreport report = myQuizReportImpl.getMyQuizReportById(qrpid);
        if(report.isPassed()){
            report.setIspass(true);
            myquizreportRepository.save(report);
        }
        //report.setTimeleft(remainingtime);

        //change cookie status
        for (Cookie cookie : request.getCookies()) {
            if(cookie.getName().compareTo("sta"+qrpid) == 0){
                if(cookie.getValue().compareTo("ondoing") == 0){
                    cookie.setValue("submited");
                    cookie.setMaxAge(60*5);
                    response.addCookie(cookie);
                }
            }
        }
        mv.addObject("review", "http://localhost:8080/quiz/reviewq?quizid="+report.getQuiz().getQuizid());
        mv.addObject("back", referer);
        mv.addObject("finalresult", report);
        return mv;
    }

}