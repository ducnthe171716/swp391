package com.swp391.onlinequiz.controller;


import com.swp391.onlinequiz.ExceptionHandling.UsersNotfoundException;
import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Confirmationtoken;
import com.swp391.onlinequiz.model.Courseowner;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.ConfirmationtokenRepository;
import com.swp391.onlinequiz.repository.UsersRepository;
import com.swp391.onlinequiz.service.UserService;

import com.swp391.onlinequiz.site.Utility;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.UnsupportedEncodingException;

import java.util.Date;


import net.bytebuddy.utility.RandomString;

@Controller
/* Modify your requestMapping */
@RequestMapping(path = "")  // the first path
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private JavaMailSenderImpl mailSender;
    @Autowired
    private ConfirmationtokenRepository ConfirmationtokenRepository;
    /*
     * editor: VietNguyen
     * This will send to Login Page
     */
    @RequestMapping("/client/login")
    public ModelAndView showLoginPage() {
        ModelAndView mv = new ModelAndView("pages/client/login");
        Users user = new Users(); // Tạo một đối tượng user mới
        mv.addObject("user", user); // Thêm đối tượng user vào model

        return mv;
    }

    /*
     * editor: VietNguyen
     * This will send to Sign-up Page
     */

    @RequestMapping("/client/signup")
    public ModelAndView showSignupPage() {
        ModelAndView mv = new ModelAndView("pages/client/register");
        Users user = new Users(); // Tạo một đối tượng user mới
        mv.addObject("user", user); // Thêm đối tượng user vào model
        return mv;
    }

    /*
     * editor: VietNguyen
     * When user login their username and password in Form from view
     */
    @PostMapping("/client/loginSucces")
    public ModelAndView loginAction(@RequestParam("username") String identify, @RequestParam("password") String password,
                                    HttpSession session, @ModelAttribute("user") Users user) {
        ModelAndView mv = new ModelAndView("pages/client/login");
        UsersDTO users = userService.getLoginUser(identify, password);

        if (users == null) {
            mv.addObject("message", "Invalid username or password!");
            return mv; // Trả về trang đăng nhập với thông báo lỗi, giữ lại giá trị đã nhập.
        } else {
            if (!users.getIsenabled()) {
                mv.addObject("message", "Your account is not activated yet!");
                return mv;
            } else if (users.getRole().equals("ROLE_MENTEE")) {
                session.setAttribute("user", users);
                session.setMaxInactiveInterval(60 * 60 * 24);
                mv.setViewName("redirect:/home");
            } else if (users.getRole().equals("ROLE_ADMIN")) {
                session.setAttribute("user", users);
                session.setMaxInactiveInterval(60 * 60 * 24);
                mv.setViewName("redirect:/admin/dashboard");
            } else if (users.getRole().equals("ROLE_COURSEOWNER")) {
                session.setAttribute("user", users);
                session.setMaxInactiveInterval(60 * 60 * 24);
                mv.setViewName("redirect:/courseowner/home");
            }
        }
        return mv;
    }


    @GetMapping("/client/logout")
    public ModelAndView logout(HttpSession session) {
        session.removeAttribute("user");
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/home");
        return mv;
    }

    @PostMapping("/client/register")
    public String registerUser(@ModelAttribute("user") Users user, Model model) {
        Users u = usersRepository.findByEmail(user.getEmail());

        if (usersRepository.existsByEmail(user.getEmail()) && !u.getIsenabled()) {
            Confirmationtoken c = ConfirmationtokenRepository.findByUser(u).get(0);
            ConfirmationtokenRepository.delete(c);
            usersRepository.delete(u);
            userService.saveUser(user);
            model.addAttribute("message", "We have already sent a verification link to your email. Please check.");
            return "pages/client/register";
        }

        if (usersRepository.existsByEmail(user.getEmail()) && u.getIsenabled()) {
            model.addAttribute("message", "This email already exists!");
            return "pages/client/register";
        }

// Add a check for existing username
        if (usersRepository.existsByUsername(user.getUsername())) {
            model.addAttribute("message", "This username already exists!");
            return "pages/client/register";
        }

        if (!usersRepository.existsByEmail(user.getEmail())) {
            userService.saveUser(user);
            model.addAttribute("message", "We have already sent a verification link to your email. Please check.");
            return "pages/client/register";
        }

        return "pages/client/register";
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(@RequestParam("token") String confirmationToken) {
        return userService.confirmEmail(confirmationToken);
    }


    @GetMapping("/forgot_password")
    public String showForgotPasswordForm(Model model) {
        model.addAttribute("PageTile", "Forgot Password");
        return "pages/client/forget-password";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request, Model model, Users user) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);
        user.setCreatedDate(new Date());
        usersRepository.updateCreatedDateByEmail(user.getCreatedDate(), email);

        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

        } catch (UsersNotfoundException ex) {
            model.addAttribute("error", ex.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }

        return "pages/client/forget-password";
    }


    public void sendEmail(String email, String resetPasswordLink) throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("swp391@fpt.edu.vn", "hocbaidi");
        helper.setTo(email);

        String subject = "Here's the link to reset your password";

        String content = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "  <body>\n" +
                "    <div style=\"font-family: Helvetica, Arial, sans-serif; min-width: 1000px; overflow: auto; line-height: 2\">\n" +
                "      <div style=\"margin: 50px auto; width: 70%; padding: 20px 0\">\n" +
                "        <div style=\"border-bottom: 1px solid #eee\">\n" +
                "          <a href=\"http://localhost:8080/home\" style=\"font-size: 1.4em; color: #2a1777; text-decoration: none; font-weight: 600\">Educhamp</a>\n" +
                "        </div>\n" +
                "        <p style=\"font-size: 1.1em\">Hello,</p>\n" +
                "        <p>You have requested to reset your password.</p>\n" +
                "        <p>Click the link below to change your password:</p>\n" +
                "        <p><a href=\"" + resetPasswordLink + "\" style=\"text-decoration: none; color: #4b0082;\">Change my password</a></p>\n" +
                "        <p>Ignore this email if you do remember your password, or you have not made the request.</p>\n" +
                "        <hr style=\"border: none; border-top: 1px solid #eee\" />\n" +
                "        <div style=\"float: right; padding: 8px 0; color: #aaa; font-size: 0.8em; line-height: 1; font-weight: 300\">\n" +
                "          <p>Educhamp Inc</p>\n" +
                "          <p>FPT university</p>\n" +
                "          <p>Hoa Lac</p>\n" +
                "        </div>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </body>\n" +
                "</html>";



        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }

    @GetMapping("/reset_password")
    public String showResetPasswordForm(@RequestParam(value = "token") String token, Model model, Users user) {
        Users users = userService.getByResetPasswordToken(token);
        model.addAttribute("token", token);
        if (users == null) {
            model.addAttribute("message", "Token is expired!");

            return "/pages/client/message";
        }
        if (new Date().getTime() - users.getCreatedDate().getTime() > 30000) {
            model.addAttribute("message", "Invalid Token Or Your token is expired if you want to reset Please try again!");
            return "pages/client/message";
        }

        return "pages/client/reset-password";
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");

        Users customer = userService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (customer == null) {
            model.addAttribute("message", "Invalid Token");
            return "/pages/client/message";
        } else {
            userService.updatePassword(customer, password);

            model.addAttribute("message", "You have successfully changed your password.");
        }

        return "/pages/client/message";
    }

}

    
   


