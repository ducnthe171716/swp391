package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.*;
import com.swp391.onlinequiz.repository.*;
import com.swp391.onlinequiz.service.*;
import com.swp391.onlinequiz.serviceimpl.UserServiceImpl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RestController
@RequestMapping(path = "")
public class HomeController {
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private AnswerreportRepository answerreportRepository;
    @Autowired
    private QuestionReportRepository questionReportRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private SectionDetailRepository sectionDetailRepository;
    @Autowired
    private ContentStatusDetailRepository contentStatusDetailRepository;
    @Autowired
    private CourseprocessRepository courseprocessRepository;
    @Autowired
    private EnrollRepository enrollRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ProgressStatusRepository progressStatusRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;
    private final CategoryService categoryService;

    private final CourseService courseService;
    @Autowired
    private QuestionService questionService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private DisscussRepository disscussRepository;

    private final UserService userService;
    private UsersRepository usersRepository;
    private final FileUploadService fileUploadService;
    private final CourseownerService courseownerService;

    private final CoursecontentRepository coursecontentRepository;

    private final CoursesectionRepository coursesectionRepository;

    public HomeController(CategoryService categoryService, CourseService courseService, UserService userService,
            FileUploadService fileUploadService, CourseownerService courseownerService,
            CoursesectionRepository coursesectionRepository, CoursecontentRepository coursecontentRepository) {
        this.categoryService = categoryService;
        this.courseService = courseService;
        this.userService = userService;
        this.fileUploadService = fileUploadService;
        this.courseownerService = courseownerService;
        this.coursesectionRepository = coursesectionRepository;
        this.coursecontentRepository = coursecontentRepository;
        this.quizRepository = quizRepository;
    }

    @GetMapping("/home")
    public ModelAndView showSignupPage(HttpServletRequest request) {
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        ProgressStatus active = progressStatusRepository.findByStatusId(3).get(0);
        List<Course> listCourses = courseRepository.findByStatus(active);
        modelMap.put("listCate", listCategory);
        modelMap.put("listCourse", listCourses);
        request.getSession().setAttribute("categories", listCategory);
        ModelAndView mv = new ModelAndView("pages/client/index", modelMap);
        return mv;
    }

    @PreAuthorize("hasAuthority('ROLE_USER','ROLE_COURSEOWNER')")
    @GetMapping("/user")
    public String user() {
        return "User role";
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/admin")
    public String admin() {
        return "Admin role";
    }
    // Change demo

    @GetMapping("/courses")
    public ModelAndView showAllCourse(@RequestParam(name = "categoryID", required = false) Integer categoryID,
            @RequestParam(name = "searchValue", required = false) String searchValue,
            @RequestParam(value = "page", required = false) Integer page) {
        Map<String, Object> modelMap = new HashMap<>();
        List<Course> courses = new ArrayList<>();
        if (categoryID != null) {
            modelMap.put("categoryID", categoryID);
            courses = courseService.getCourseByCategory(categoryID);
        }
        if (searchValue != null) {
            modelMap.put("searchValue", searchValue);
            courses = searchValue.trim().isEmpty() ? courseService.getAll()
                    : courseService.getCourseBySearchValue(searchValue);
        }
        Page<Course> paginated = courseService.findPaginated(PageRequest.of(page != null ? page - 1 : 0, 9), courses);
        List<Category> listCategory = categoryService.getAll();
        int totalPages = paginated.getTotalPages();

        int pageNum = page != null ? page : 1;
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(pageNum, Math.min(totalPages, pageNum + 2))
                    .boxed()
                    .collect(Collectors.toList());
            modelMap.put("pageNumbers", pageNumbers);
        }
        modelMap.put("listCate", listCategory);
        modelMap.put("pagingCourses", paginated);
        ModelAndView mv = new ModelAndView("pages/client/courses", modelMap);
        return mv;
    }

    @GetMapping("/courseDetail/{id}")

    public ModelAndView getCourseByID(@PathVariable(value = "id") int id, HttpSession session) {
        Course course = courseService.getCourseByID(id);
        UsersDTO usersDTO = (UsersDTO) session.getAttribute("user");

        if (usersDTO != null) {
            Integer userid = usersDTO.getUserId();
            Users users = userServiceImpl.getUsersByUserDTOid(userid);
            if (enrollRepository.findByUserAndCourse(users, course).size() != 0) {
                return new ModelAndView("redirect:http://localhost:8080/coursecontent/mycourse/" + id);
            }
        }
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        modelMap.put("course", course);
        modelMap.put("listCate", listCategory);
        ModelAndView mv = new ModelAndView("pages/client/courses-details", modelMap);
        return mv;
    }

    @GetMapping("/course-owners")
    public ModelAndView getCourseOwner(@RequestParam(value = "page", required = false) Integer page,
            Principal principal,
            @RequestParam(value = "searchValue", required = false) String searchValue,
            @RequestParam(value = "review", required = false) Integer reviews,
            @RequestParam(value = "statusID", required = false) Integer statusID,
            @RequestParam(value = "orderBy", required = false) String orderBy,
            @RequestParam(value = "categoryID", required = false) Integer categoryID) {
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        modelMap.put("listCate", listCategory);
        // if (principal != null){
        // Users user = userService.getUserByUserName(principal.getName());
        Page<Course> list = courseService.getCourseByOwnerID(PageRequest.of(page != null ? page - 1 : 0, 3), 1,
                searchValue, reviews, statusID, orderBy, categoryID);
        modelMap.put("pagingCourses", list);
        // }
        int totalPages = list.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            modelMap.put("pageNumbers", pageNumbers);
        }
        ModelAndView mv = new ModelAndView("pages/admin/courses", modelMap);
        return mv;
    }

    @GetMapping("/addCourse")
    public ModelAndView AddCourse() {
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        modelMap.put("listCate", listCategory);
        ModelAndView mv = new ModelAndView("pages/admin/add-course", modelMap);
        return mv;
    }

    @PostMapping("/addCourse")
    public ModelAndView AddCourse(@RequestParam("coursename") String courseName,
            @RequestParam("price") String coursePrice,
            @RequestParam("category") Integer categoryID,
            @RequestParam("description") String description,
            @RequestParam("image") MultipartFile image) throws IOException {
        Course course = new Course();
        Category category = categoryService.getCategoryByID(categoryID);
        course.setCoursename(courseName);
        course.setCourseprice(Double.parseDouble(coursePrice));
        course.setCategory(category);
        course.setImage(fileUploadService.uploadFile(image));
        course.setDescription(description);
        course.setCreatedate(LocalDateTime.now());
        course.setCourseowner(courseownerService.getCourseownerById(1));
        course.setCourserating(0);
        courseService.save(course);
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        modelMap.put("listCate", listCategory);
        modelMap.put("report", "Add Successfully");
        ModelAndView mv = new ModelAndView("pages/admin/add-course", modelMap);
        return mv;
    }

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private PreneedRepository preneedRepository;

    @Autowired
    private LearningOutcomeRepository learningOutcomeRepository;

    @GetMapping("/editCourse")
    public ModelAndView EditCourse(@RequestParam("id") Integer id) {
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        List<Language> listLanguage = languageRepository.findAll();
        List<Level> listLevel = levelRepository.findAll();
        Course course = courseService.getCourseByID(id);
        String preneeditem = "";
        String outcomeitem = "";
        for (Preneeded pre : course.getPreneeds()) {
            preneeditem += pre.getNeededitem() + "\n";
        }
        for (Learningoutcome out : course.getOutcomes()) {
            outcomeitem += out.getOutcomeitem() + "\n";
        }
        modelMap.put("listCate", listCategory);
        modelMap.put("course", course);
        modelMap.put("listLang", listLanguage);
        modelMap.put("listLeve", listLevel);
        modelMap.put("preneeded", preneeditem);
        modelMap.put("outcome", outcomeitem);
        int levelID = course.getLevel().getLevelid();
        int languageID = course.getLanguage().getLanguageid();
        modelMap.put("levelID", levelID);
        modelMap.put("languageID", languageID);
        ModelAndView mv = new ModelAndView("pages/admin/edit-course", modelMap);
        return mv;
    }

    @PostMapping("/editCourse")
    public RedirectView EditCourse(@RequestParam("id") Integer id,
            @RequestParam("coursename") String courseName,
            @RequestParam("price") String coursePrice,
            @RequestParam("category") Integer categoryID,
            @RequestParam("language") Integer languageID,
            @RequestParam("level") Integer levelID,
            @RequestParam("description") String description,
            @RequestParam("preneeded") String preneeded,
            @RequestParam("outcome") String outcome,
            @RequestParam(value = "image", required = false) MultipartFile image) throws IOException {
        Course course = courseService.getCourseByID(id);
        Category category = categoryService.getCategoryByID(categoryID);
        course.setCoursename(courseName);
        course.setCourseprice(Double.parseDouble(coursePrice));
        course.setCategory(category);
        if (!image.isEmpty()) {
            course.setImage(fileUploadService.uploadFile(image));
        }
        course.setDescription(description);
        course.setCreatedate(LocalDateTime.now());
        course.setCourseowner(courseownerService.getCourseownerById(1));
        course.setCourserating(0);

        course.setLevel(levelRepository.findById(levelID).get());
        course.setLanguage(languageRepository.findById(languageID).get());

        String[] preneededlist = preneeded.split("\n");
        String[] outcomelist = outcome.split("\n");

        Course savedc = courseService.save(course);

        preneedRepository.deleteAll(savedc.getPreneeds());
        learningOutcomeRepository.deleteAll(savedc.getOutcomes());

        for (String out : outcomelist) {
            Learningoutcome outc = new Learningoutcome();
            outc.setOutcomeitem(out);
            outc.setCourse(savedc);
            learningOutcomeRepository.save(outc);
        }

        for (String pre : preneededlist) {
            Preneeded preneed = new Preneeded();
            preneed.setNeededitem(pre);
            preneed.setCourse(savedc);
            preneedRepository.save(preneed);
        }

        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();

        modelMap.put("listCate", listCategory);
        modelMap.put("report", "Edit Course Successfully");
        return new RedirectView("/editCourse?id=" + course.getCourseid());
    }

    @PostMapping("/add-Category")
    public RedirectView AddCategory(HttpServletRequest request, @RequestParam("category") String category) {
        categoryService.saveCategory(category);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @GetMapping("/edit-coursesection")
    public ModelAndView editCourseSection(@RequestParam("cid") Integer id) {
        List<Coursesection> list = coursesectionRepository.getCourseSectionsByCourseID(id);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", list);
        modelMap.put("cid", id);
        ModelAndView mv = new ModelAndView("pages/admin/edit-coursesection", modelMap);
        return mv;
    }
    // coursecontentRepository.deleteAll(coursesection.getCoursecontents());
    // quizRepository.delete(coursesection.getQuizSet());

    @GetMapping("/deleteCourseSection")
    public RedirectView deleteCourseSection(HttpServletRequest request, @RequestParam("coursesectionid") Integer id) {
        Coursesection coursesection = coursesectionRepository.findById(id).get();
        Quiz quiz = coursesection.getQuizSet();
        if (quiz != null) {
            for (Question question : quiz.getQuestionSet()) {
                answerRepository.deleteAll(question.getAnswerSet());
            }
            questionRepository.deleteAll(quiz.getQuestionSet());
        }
        if (coursesection.getQuizSet() != null) {
            quizRepository.delete(coursesection.getQuizSet());
        }
        if (coursesection.getCoursecontents() != null) {
            for (Coursecontent i : coursesection.getCoursecontents()) {
                if (i.getCourseprocessSet() != null) {
                    for (Courseprocess item : i.getCourseprocessSet()) {
                        courseprocessRepository.delete(item);
                    }
                }
                Set<Disscuss> disscussSet = i.getDisscussSet();
                if (disscussSet != null) {
                    for (Disscuss item : disscussSet) {
                        disscussRepository.delete(item);
                    }
                }
                contentStatusDetailRepository.delete(i.getContentId());
                coursecontentRepository.delete(i);
            }
        }
        sectionDetailRepository.deleteByCourseSectionID(id);
        coursesectionRepository.deleteById(id);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @GetMapping("/submitcourse")
    public ModelAndView SubmitCourse(@RequestParam("id") Integer id) {
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        List<Language> listLanguage = languageRepository.findAll();
        List<Level> listLevel = levelRepository.findAll();
        Course course = courseService.getCourseByID(id);
        if (course.getStatus().getStatusId() != 3) {
            course.setStatus(progressStatusRepository.findById(2).get());
            modelMap.put("report", "Submit successfully. Wait for admin acception");
        } else {
            modelMap.put("report", "This course have been actived by admin");
        }

        courseRepository.save(course);
        String preneeditem = "";
        String outcomeitem = "";
        for (Preneeded pre : course.getPreneeds()) {
            preneeditem += pre.getNeededitem() + "\n";
        }
        for (Learningoutcome out : course.getOutcomes()) {
            outcomeitem += out.getOutcomeitem() + "\n";
        }
        modelMap.put("listCate", listCategory);
        modelMap.put("course", course);
        modelMap.put("listLang", listLanguage);
        modelMap.put("listLeve", listLevel);
        modelMap.put("preneeded", preneeditem);
        modelMap.put("outcome", outcomeitem);
        int levelID = course.getLevel().getLevelid();
        int languageID = course.getLanguage().getLanguageid();
        modelMap.put("levelID", levelID);
        modelMap.put("languageID", languageID);

        ModelAndView mv = new ModelAndView("pages/admin/edit-course", modelMap);
        return mv;
    }
    @PostMapping("/editCourseSection")
    public RedirectView editCourseSection(@RequestParam("id") Integer id,
            @RequestParam("title") String title,
            @RequestParam("order") Integer order,
            HttpServletRequest request) {
        Coursesection c = coursesectionRepository.findById(id).get();
        c.setSectiontitle(title);
        c.setSectionorder(order);
        c.setSectiondob(LocalDateTime.now());
        coursesectionRepository.save(c);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

//    @PostMapping("/editCourseSection")
//    public RedirectView editCourseSection(@RequestParam("id") Integer id,
//                                          @RequestParam("title") String title,
//                                          @RequestParam("order") Integer order,
//                                          HttpServletRequest request) {
//        Coursesection c = coursesectionRepository.findById(id).get();
//        c.setSectiontitle(title);
//        c.setSectionorder(order);
//        c.setSectiondob(LocalDateTime.now());
//        coursesectionRepository.save(c);
//        String url = request.getHeader("referer");
//        return new RedirectView(url);
//    }

    @GetMapping("/course-content")
    public ModelAndView coursecontents(@RequestParam("sid") Integer id, @RequestParam("cid") Integer cid) {
        List<Coursecontent> list = coursecontentRepository.getCourseContentsBySectionID(id);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", list);
        modelMap.put("sid", id);
        modelMap.put("cid", cid);
        ModelAndView mv = new ModelAndView("pages/admin/edit-coursecontent", modelMap);
        return mv;
    }

    @GetMapping("/deleteCourseContent")
    public RedirectView deleteCourseContent(HttpServletRequest request, @RequestParam("contentID") Integer id) {
        Set<Courseprocess> courseprocessSet = coursecontentRepository.findById(id).get().getCourseprocessSet();
        if (courseprocessSet != null) {
            for (Courseprocess i : courseprocessSet) {
                courseprocessRepository.delete(i);
            }
        }
        Set<Disscuss> disscussSet = coursecontentRepository.findById(id).get().getDisscussSet();
        if (disscussSet != null) {
            for (Disscuss i : disscussSet) {
                disscussRepository.delete(i);
            }
        }
        contentStatusDetailRepository.delete(id);
        coursecontentRepository.deleteById(id);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/editContent")
    public RedirectView editCourseContent(@RequestParam("id") Integer id,
            @RequestParam("title") String title,
            @RequestParam("order") Integer order,
            @RequestParam(value = "free", required = false) Integer ispublic,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request) throws IOException {
        Coursecontent coursecontent = coursecontentRepository.findById(id).get();
        coursecontent.setContenttitle(title);
        coursecontent.setContentorder(order);
        coursecontent.setContentdob(LocalDateTime.now());
        if (ispublic != null && ispublic == 1) {
            coursecontent.setIspublic(true);
        } else {
            coursecontent.setIspublic(false);
        }
        if (!file.isEmpty()) {
            coursecontent.setLinkvideo(fileUploadService.uploadFile(file));
        }
        coursecontentRepository.save(coursecontent);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/addCourseContent")
    public RedirectView addCourseContent(@RequestParam("id") Integer id,
            @RequestParam("title") String title,
            @RequestParam("order") Integer order,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request) throws IOException {
        Coursecontent coursecontent = new Coursecontent();
        coursecontent.setContenttitle(title);
        coursecontent.setContentorder(order);
        coursecontent.setContentdob(LocalDateTime.now());
        coursecontent.setLinkvideo(fileUploadService.uploadFile(file));
        coursecontent.setCoursesection(coursesectionRepository.findById(id).get());
        coursecontentRepository.save(coursecontent);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @GetMapping("/course-quiz")
    public ModelAndView viewQuizPractice(@RequestParam("sid") Integer id) {
        List<Quiz> list = quizRepository.getQuizzesByCourseID(id);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", list);
        modelMap.put("sid", id);
        ModelAndView mv = new ModelAndView("pages/admin/edit-quiz", modelMap);
        return mv;
    }

    @PostMapping("/editCourseQuiz")
    public RedirectView editQuizPractice(@RequestParam("id") Integer id,
            @RequestParam("content") String content,
            @RequestParam("passpoint") Double passport,
            @RequestParam("time") Integer time,
            @RequestParam("randomquestion") Integer randquestion,
            HttpServletRequest request) {
        Quiz quiz = quizRepository.findById(id).get();
        quiz.setPasspoint(passport);
        quiz.setQuizcontent(content);
        quiz.setQuizdob(LocalDateTime.now());
        quiz.setTime(time);
        quiz.setRandquestion(randquestion);
        quizRepository.save(quiz);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @Autowired
    QuizService quizService;

    @PostMapping("/addQuiz")
    public RedirectView addQuiz(@RequestParam("sid") Integer idSection,
            @RequestParam("title") String title,
            @RequestParam("time") Integer time,
            @RequestParam("randomquestion") Integer randomquestion,
            @RequestParam("passpoint") Double passpoint,
            HttpServletRequest request) throws IOException {
        Quiz quiz = new Quiz();
        quiz.setQuizcontent(title);
        quiz.setTime(time);
        quiz.setQuizdob(LocalDateTime.now());
        quiz.setCoursesection(coursesectionRepository.findById(idSection).get());
        quiz.setRandquestion(randomquestion);
        quiz.setPasspoint(passpoint);
        quizService.save(quiz);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @GetMapping("/deleteCourseQuiz")
    public RedirectView deleteCourseQuiz(@RequestParam("coursequizid") Integer id, HttpServletRequest request) {
        Set<Question> questionSet = quizRepository.findById(id).get().getQuestionSet();
        if (questionSet != null) {
            for (Question question : questionSet) {
                answerRepository.deleteAll(question.getAnswerSet());
            }
            questionRepository.deleteAll(questionSet);
        }

        quizRepository.deleteById(id);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @GetMapping("/editquestions")
    public ModelAndView viewQuizQuestion(@RequestParam("id") Integer id,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        Page<Question> list = questionService.getQuestionByQuizId(PageRequest.of(currentPage - 1, pageSize), id);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", list);
        modelMap.put("quizID", id);
        if (!list.getContent().isEmpty()) {
            modelMap.put("courseSectionID", list.getContent().get(0).getQuiz().getCoursesection().getSectionId());
        } else {
            modelMap.put("courseSectionID", null);
        }
        int totalPages = list.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            modelMap.put("pageNumbers", pageNumbers);
        }
        ModelAndView mv = new ModelAndView("pages/admin/question-list", modelMap);
        return mv;
    }

    @GetMapping("/deleteQuestion")
    public RedirectView deleteQuestion(@RequestParam("coursequestionid") Integer id, HttpServletRequest request) {
        Question question = questionRepository.findById(id).get();
        Set<Answer> answerSet = question.getAnswerSet();
        List<Questionreport> questionreports = questionReportRepository.findByQuestion(id);
        questionReportRepository.deleteAll(questionreports);
        answerRepository.deleteAll(answerSet);
        questionRepository.delete(question);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/updateQuestion")
    public RedirectView updateQuestion(@RequestParam("id") Integer id,
            @RequestParam("content") String questionContent,
            @RequestParam("order") Integer order,
            HttpServletRequest request) {
        Question question = questionRepository.findById(id).get();
        question.setQuestioncontent(questionContent);
        question.setQuestionorder(order);
        questionRepository.save(question);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/updateAnswer")
    public RedirectView updateAnswer(@RequestParam Map<String, String> params, HttpServletRequest request) {
        Set<Answer> answerSet = questionRepository.findById(Integer.parseInt(params.get("id"))).get().getAnswerSet();
        for (Answer i : answerSet) {
            i.setContent(params.get("contentanswer" + i.getAnswerId()));
            boolean check = Integer.parseInt(params.get("group")) == i.getAnswerId();
            i.setCorrect(check);
        }
        answerRepository.saveAll(answerSet);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @GetMapping("/answer-list")
    public ModelAndView createAnswer(@RequestParam("id") Integer quesID) {
        List<Answer> listAnswer = answerService.getAnswerByQuesID(quesID);
        Map<String, Object> modelMap = new HashMap<>();

        if (!listAnswer.isEmpty()) {
            modelMap.put("list", listAnswer);
            modelMap.put("quizID", listAnswer.get(0).getQuestion().getQuiz().getQuizid());
        } else {
            // Xử lý trường hợp danh sách rỗng, có thể là thông báo hoặc hành động khác tùy
            // thuộc vào logic ứng dụng của bạn.
            modelMap.put("list", Collections.emptyList()); // hoặc giá trị mặc định khác tùy thuộc vào logic ứng dụng
                                                           // của bạn.
            modelMap.put("quizID", null); // hoặc giá trị mặc định khác tùy thuộc vào logic ứng dụng của bạn.
        }

        Question question = questionService.getQuestionById(quesID);
        modelMap.put("question", question);

        ModelAndView mv = new ModelAndView("pages/admin/answer-list", modelMap);
        return mv;
    }

}
