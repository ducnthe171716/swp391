package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.*;
import com.swp391.onlinequiz.repository.*;
import com.swp391.onlinequiz.service.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.thymeleaf.model.IModel;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/course")
public class Coursecontroller {

    @Autowired
    private PreneedRepository preneedRepository;

    @Autowired
    private LearningOutcomeRepository learningOutcomeRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private LanguageRepository languageRepository;

    private CategoryService categoryService;

    private CourseService courseService;

    private UserService userService;
    private UsersRepository usersRepository;
    private FileUploadService fileUploadService;
    private CourseownerService courseownerService;

    private CoursecontentRepository coursecontentRepository;

    private CoursesectionRepository coursesectionRepository;
    private QuizService quizService;
    private QuestionService questionService;
    private CoursesectionService coursesectionService;
    private AnswerService answerService;
    @Autowired
    private ProgressStatusRepository progressStatusRepository;

    public Coursecontroller(CategoryService categoryService, CourseService courseService, UserService userService, FileUploadService fileUploadService, CourseownerService courseownerService, CoursesectionRepository coursesectionRepository,
                            CoursecontentRepository coursecontentRepository, QuizService quizRepository, CoursesectionService coursesectionService,QuestionService questionService, AnswerService answerService) {
        this.categoryService = categoryService;
        this.courseService = courseService;
        this.userService = userService;
        this.fileUploadService = fileUploadService;
        this.courseownerService = courseownerService;
        this.coursesectionRepository = coursesectionRepository;
        this.coursecontentRepository = coursecontentRepository;
        this.quizService = quizRepository;
        this.coursesectionService = coursesectionService;
        this.questionService = questionService;
        this.answerService = answerService;
    }

    @GetMapping("/createCourse")
    public ModelAndView CreateCourse() {
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        List<Language> listLanguage = languageRepository.findAll();
        List<Level> listLevel = levelRepository.findAll();
        modelMap.put("listCate", listCategory);
        modelMap.put("listLang", listLanguage);
        modelMap.put("listLeve", listLevel);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-infor-course-page", modelMap);
        return mv;
    }


    @PostMapping("/createCourse")
    public ModelAndView EditCourse(
            @RequestParam("coursename") String courseName,
            @RequestParam("price") String coursePrice,
            @RequestParam("category") Integer categoryID,
            @RequestParam("language") Integer languageID,
            @RequestParam("level") Integer levelID,
            @RequestParam("description") String description,
            @RequestParam("preneeded") String preneeded,
            @RequestParam("outcome") String outcome,
            @RequestParam("image") MultipartFile image, HttpSession session) throws IOException {
        LocalDateTime datecreate = LocalDateTime.now();
        Integer courserating = 0;
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        Courseowner courseowner = courseownerService.getCourseownerById(user.getUserId());
        Category category = categoryService.getCategoryByID(categoryID);
        String imageCourse = fileUploadService.uploadFile(image);
        Double price = Double.parseDouble(coursePrice);
        Course course = new Course(courseName, category, datecreate, price, courseowner, description, imageCourse, courserating);
        course.setLevel(levelRepository.findById(levelID).get());
        course.setLanguage(languageRepository.findById(languageID).get());
        String[] preneededlist = preneeded.split("\n");
        String[] outcomelist = outcome.split("\n");
        if(course.getStatus() == null){
            course.setStatus(progressStatusRepository.findById(1).get());
        }



        

        Course savedc = courseService.save(course);

        for (String out : outcomelist) {
            Learningoutcome outc = new Learningoutcome();
            outc.setOutcomeitem(out);
            outc.setCourse(savedc);
            learningOutcomeRepository.save(outc);
        }

        for (String pre : preneededlist) {
            Preneeded preneed = new Preneeded();
            preneed.setNeededitem(pre);
            preneed.setCourse(savedc);
            preneedRepository.save(preneed);
        }

        

        Map<String, Object> modelMap = new HashMap<>();
        List<Language> listLanguage = languageRepository.findAll();
        List<Level> listLevel = levelRepository.findAll();
        List<Category> listCategory = categoryService.getAll();
        modelMap.put("listCate", listCategory);
        modelMap.put("listLang", listLanguage);
        modelMap.put("listLeve", listLevel);
        modelMap.put("courseName", courseName);
        modelMap.put("coursePrice", coursePrice);
        modelMap.put("categoryID", categoryID);
        modelMap.put("levelID", levelID);
        modelMap.put("languageID", languageID);
        modelMap.put("description", description);
        modelMap.put("image", imageCourse);
        modelMap.put("preneeded", preneeded);
        modelMap.put("outcome", outcome);
        modelMap.put("report", "Action successfully");
        modelMap.put("course", course);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-infor-course-page", modelMap);
        return mv;
    }

    // @RequestMapping()
    // public 

    public LocalDateTime getCurrentDate() {
        return LocalDateTime.now();
    }

    @GetMapping("/createCourseSection")
    public ModelAndView editCourseSection(@RequestParam("id") Integer id) {
        List<Coursesection> list = coursesectionRepository.getCourseSectionsByCourseID(id);
        int count = 0;
        for(Coursesection c  : list ){
            for(Coursecontent ct : c.getCoursecontents()){
                count++;
            }
        }
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("count", count);
        modelMap.put("list", list);
        modelMap.put("courseID", id);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-course-section", modelMap);
        return mv;
    }
    @PostMapping("/addCourseSection")
    public RedirectView addCourseSection(@RequestParam("id") Integer id,
                                         @RequestParam("title") String title,
                                         @RequestParam("order") Integer order,
                                         HttpServletRequest request) {

        Coursesection c = new Coursesection();
        c.setSectiontitle(title);
        c.setSectionorder(order);
        c.setSectiondob(LocalDateTime.now());
        c.setCourse(courseService.getCourseByID(id));
        coursesectionRepository.save(c);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/editCourseSection")
    public RedirectView editCourseSection(@RequestParam("id") Integer id,
                                          @RequestParam("title") String title,
                                          @RequestParam("order") Integer order,
                                          HttpServletRequest request) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        Coursesection c = coursesectionRepository.findById(id).get();
        c.setSectiontitle(title);
        c.setSectionorder(order);
        c.setSectiondob(LocalDateTime.now());
        coursesectionRepository.save(c);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @GetMapping("/createContent")
    public ModelAndView createContent(@RequestParam("id") Integer id) {
        List<Coursecontent> list = coursecontentRepository.getCourseContentsBySectionID(id);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("count", list.size());
        modelMap.put("list", list);
        Coursesection coursesection = coursesectionService.findById(id);
        modelMap.put("coursesection", coursesection);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-course-content", modelMap);
        return mv;
    }
    @GetMapping("/createQuiz")
    public ModelAndView createQuiz(@RequestParam("id") Integer id) {
        List<Quiz> listQuiz = quizService.getQuizBySectionID(id);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", listQuiz);
        Coursesection coursesection = coursesectionService.findById(id);
        modelMap.put("coursesection", coursesection);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-course-quiz", modelMap);
        return mv;
    }
    @GetMapping("/createQuestion")
    public ModelAndView createQuestion(@RequestParam("id") Integer quizId) {
        List<Question> listQuestion = questionService.getQuestionBySectionId(quizId);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", listQuestion);
        Quiz quiz = quizService.getQuizByQuizId(quizId);
        modelMap.put("quiz", quiz);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-course-question", modelMap);
        return mv;
    }
    @GetMapping("/createAnswer")
    public ModelAndView createAnswer(@RequestParam("id") Integer quesID) {
        List<Answer> listAnswer = answerService.getAnswerByQuesID(quesID);
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("list", listAnswer);
        Question question = questionService.getQuestionById(quesID);
        modelMap.put("question", question);
        ModelAndView mv = new ModelAndView("pages/courseOwner/create-course-answer", modelMap);
        return mv;
    }

    @PostMapping("/addCourseContent")
    public RedirectView addCourseContent(@RequestParam("id") Integer id,
                                         @RequestParam("title") String title,
                                         @RequestParam("order") Integer order,
                                         @RequestParam(value = "free", required = false) Integer ispublic,
                                         @RequestParam("file") MultipartFile file,
                                         HttpServletRequest request) throws IOException {
        Coursecontent coursecontent = new Coursecontent();
        coursecontent.setContenttitle(title);
        coursecontent.setContentorder(order);
        coursecontent.setContentdob(LocalDateTime.now());
        if (ispublic != null && ispublic == 1) {
            coursecontent.setIspublic(true);
        } else {
            coursecontent.setIspublic(false);
        }
        coursecontent.setLinkvideo(fileUploadService.uploadFile(file));
        coursecontent.setCoursesection(coursesectionRepository.findById(id).get());
        coursecontentRepository.save(coursecontent);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @PostMapping("/addQuestion")
    public RedirectView addQuestion(@RequestParam("idQuiz") Integer idQuiz,
                                         @RequestParam("title") String title,
                                         HttpServletRequest request) throws IOException {
        Question question = new Question();
        question.setQuestioncontent(title);
        Quiz quiz = quizService.getQuizByQuizId(idQuiz);
        question.setQuiz(quiz);
        questionService.save(question);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @PostMapping("/addAnswer")
    public RedirectView addAnswer(@RequestParam("idQuestion") Integer idQuestion,
                                    @RequestParam("title") String title,
                                    HttpServletRequest request) throws IOException {
        Answer answer = new Answer();
        answer.setContent(title);
        answer.setCorrect(false);
        Question question = questionService.getQuestionById(idQuestion);
        answer.setQuestion(question);
        answerService.saveAnswer(answer);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/addQuiz")
    public RedirectView addQuiz(@RequestParam("idSection") Integer idSection,
                                @RequestParam("title") String title,
                                @RequestParam("time") Integer timer,
                                @RequestParam("ranquestion") Integer ranquestion,
                                @RequestParam("passpoint") Double passpoint,
                                HttpServletRequest request) throws IOException {
        Quiz quiz = new Quiz();
        quiz.setQuizcontent(title);
        quiz.setTime(timer);
        quiz.setQuizdob(LocalDateTime.now());
        quiz.setCoursesection(coursesectionRepository.findById(idSection).get());
        quiz.setRandquestion(ranquestion);
        quiz.setPasspoint(passpoint);
        quizService.save(quiz);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @PostMapping("/editQuiz")
    public RedirectView editQuiz(
                                 @RequestParam("idQuiz") Integer idQuiz,
                                 @RequestParam("title") String title,
                                 @RequestParam("time") Integer timer,
                                 @RequestParam("ranquestion") Integer ranquestion,
                                 @RequestParam("passpoint") Double passpoint,
                                 HttpServletRequest request) {
        quizService.updateQuiz(title,timer,ranquestion,passpoint,LocalDateTime.now(),idQuiz);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @PostMapping("/editQuestion")
    public RedirectView editQuestion(
            @RequestParam("idQuiz") Integer idQuiz,
            @RequestParam("title") String title,
            HttpServletRequest request) {
        questionService.updateQuestion(title, idQuiz);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @PostMapping("/editAnswer")
    public RedirectView editAnswer(
            @RequestParam("idAnswer") Integer idAnswer,
            @RequestParam("title") String title,
            HttpServletRequest request) {
        answerService.updateAnswer(title, idAnswer);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }

    @Autowired
    CourseprocessRepository courseprocessRepository;
    @Autowired
    DisscussRepository disscussRepository;
    @Autowired
    ContentStatusDetailRepository contentStatusDetailRepository;
    @GetMapping("/deleteCourseContent")
    public RedirectView deleteCourseContent(HttpServletRequest request, @RequestParam("contentID") Integer id) {
        Set<Courseprocess> courseprocessSet = coursecontentRepository.findById(id).get().getCourseprocessSet();
       if(courseprocessSet != null){
           for(Courseprocess i : courseprocessSet){
               courseprocessRepository.delete(i);
           }
       }
       Set<Disscuss> disscussSet = coursecontentRepository.findById(id).get().getDisscussSet();
       if(disscussSet != null){
           for(Disscuss i : disscussSet){
               disscussRepository.delete(i);
           }
       }
       contentStatusDetailRepository.delete(id);
       coursecontentRepository.deleteById(id);
       String url = request.getHeader("referer");
       return new RedirectView(url);
    }
    @Autowired
    QuizRepository quizRepository;
    @GetMapping("/deleteCourseQuiz")
    public RedirectView deleteCourseQuiz(@RequestParam("coursequizid") Integer id,HttpServletRequest request) {
        Set<Question> questionSet = quizRepository.findById(id).get().getQuestionSet();
        if(questionSet != null){
            for (Question question : questionSet) {
                answerRepository.deleteAll(question.getAnswerSet());
            }
            questionRepository.deleteAll(questionSet);
        }
 
        quizRepository.deleteById(id);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @Autowired
    QuestionReportRepository questionReportRepository;
    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    AnswerRepository answerRepository;
    @GetMapping("/deleteQuestion")
    public RedirectView deleteQuestion(HttpServletRequest request, @RequestParam("coursequestionid") Integer id) {
        Question question = questionRepository.findById(id).get();
       Set<Answer> answerSet = question.getAnswerSet();
       List<Questionreport> questionreports = questionReportRepository.findByQuestion(id);
       questionReportRepository.deleteAll(questionreports);
       answerRepository.deleteAll(answerSet);
       questionRepository.delete(question);
       String url = request.getHeader("referer");
       return new RedirectView(url);
    }
    @GetMapping("/deleteAnswer")
    public RedirectView deleteAnswer(HttpServletRequest request, @RequestParam("idAnswer") Integer id) {
        answerService.deleteAnswer(id);
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
    @Autowired
    SectionDetailRepository sectionDetailRepository;
    @GetMapping("/deleteCourseSection")
    public RedirectView deleteCourseSection(HttpServletRequest request, @RequestParam("coursesectionid") Integer id) {
        Coursesection coursesection = coursesectionRepository.findById(id).get();
       Quiz quiz = coursesection.getQuizSet();
       if(quiz != null){
           for (Question question : quiz.getQuestionSet()) {
               answerRepository.deleteAll(question.getAnswerSet());
           }
           questionRepository.deleteAll(quiz.getQuestionSet());
       }
       if(coursesection.getQuizSet() != null){
           quizRepository.delete(coursesection.getQuizSet());
       }
       if(coursesection.getCoursecontents() != null){
           for(Coursecontent i : coursesection.getCoursecontents()){
               if(i.getCourseprocessSet() != null){
                   for(Courseprocess item : i.getCourseprocessSet()){
                       courseprocessRepository.delete(item);
                   }
               }
               Set<Disscuss> disscussSet = i.getDisscussSet();
               if(disscussSet != null){
                   for(Disscuss item : disscussSet){
                       disscussRepository.delete(item);
                   }
               }
               contentStatusDetailRepository.delete(i.getContentId());
               coursecontentRepository.delete(i);
           }
       }
       sectionDetailRepository.deleteByCourseSectionID(id);
       coursesectionRepository.deleteById(id);
       String url = request.getHeader("referer");
       return new RedirectView(url);
    }

    @RequestMapping(path = "/submitAnswer", method = RequestMethod.POST)
    public RedirectView submitAnswer(@RequestBody Map<String, String> requestBody,HttpServletRequest request) {
        String selectedOption = requestBody.get("selectedOption");
        Integer idRightAnswer = Integer.parseInt(selectedOption);
        answerService.updateRightAnswer(idRightAnswer);
        System.out.println("Sucessfull");
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
}
