package com.swp391.onlinequiz.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.serviceimpl.CertificateService;

@Controller
@RequestMapping("/certificate")
public class CertificateController {

    @Autowired
    private EnrollRepository enrollRepository;

    @GetMapping(value = "/getcertificate", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> myCertificate(@RequestParam("enroll") Integer enrollid)  throws IOException {
        Enroll enroll = enrollRepository.findByEnrollid(enrollid).get(0);
        
 
        ByteArrayInputStream bis = CertificateService.certificateExport(enroll);
 
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=certificate.pdf");
 
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
