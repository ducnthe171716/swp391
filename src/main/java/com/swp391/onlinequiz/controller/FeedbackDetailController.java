package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Feedbackdetail;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.service.CourseService;
import com.swp391.onlinequiz.service.FeedbackdetailService;
import com.swp391.onlinequiz.service.UserService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/feedback")
public class FeedbackDetailController {
    public FeedbackdetailService feedbackdetailService;
    public UserService userService;
    public CourseService courseService;

    @Autowired
    public FeedbackDetailController(FeedbackdetailService feedbackdetailService, UserService userService, CourseService courseService) {
        this.feedbackdetailService = feedbackdetailService;
        this.userService = userService;
        this.courseService = courseService;
    }


    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    @GetMapping("rating")
    public Map<Integer, Integer> getRating(HttpSession session) {
        Course course = (Course) session.getAttribute("course");
        System.out.println(course.getCoursename());
        Map<Integer, Integer> ratingFeedback = feedbackdetailService.countRating(course.getCourseid());
        System.out.println(ratingFeedback.toString());
        return ratingFeedback;
    }

    @PostMapping("/saveFeedback")
    public RedirectView saveRating(@RequestParam("rating") int rating,
                                   @RequestParam(value = "opinion", required = false) String opinion,
                                   @RequestParam("userId") int userId,
                                   @RequestParam("courseID") Integer courseID ) {
        String datepost = getCurrentDate().toString();
        System.out.println("rating: "+ rating + "userid: " + userId + "course" + courseID);
        Users user = userService.getUserByID(userId);
        Course course = courseService.getCourseByID(courseID);
        Feedbackdetail feedbackdetail = new Feedbackdetail(user,course, opinion,datepost,rating);
        feedbackdetailService.saveFeedback(feedbackdetail);

        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/coursecontent/mycourse/" + courseID);
        return redirectView;
    }
    @PostMapping("/updateFeedback")
    public RedirectView updateRating(@RequestParam("ratingExist") Integer rating,
                                   @RequestParam(value = "opinionEdit", required = false) String opinion,
                                   @RequestParam("userId") Integer userId,
                                   @RequestParam("courseID") Integer courseID ) {
        String datepost = getCurrentDate().toString();
        System.out.println("rating: "+ rating + "userid: " + userId + "course" + courseID+"opinion"+opinion+"date: "+ datepost);
        feedbackdetailService.updateFeedback(rating,datepost,opinion,userId);
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/coursecontent/mycourse/" + courseID);
        System.out.println("Success");
        return redirectView;
    }

}
