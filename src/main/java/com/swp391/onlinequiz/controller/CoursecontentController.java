package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.*;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.repository.MyquizreportRepository;
import com.swp391.onlinequiz.service.*;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/coursecontent")
public class CoursecontentController {
    private CourseService courseService;
    private CoursecontentService coursecontentService;
    private DisscussService disscussService;
    private FeedbackdetailService feedbackdetailService;
    private UserService userService;
    private EnrollService enrollService;
    private ContentStatusDetailService contentStatusDetailService;
    private SectionDetailService sectionDetailService;
    private EnrollRepository enrollRepository;
    private MyquizreportRepository myquizreportRepository;
    @Autowired
    private HttpSession httpSession;

    public CoursecontentController(CourseService courseService, CoursecontentService coursecontentService, DisscussService disscussService, FeedbackdetailService feedbackdetailService, UserService userService, EnrollService enrollService, ContentStatusDetailService contentStatusDetailService, SectionDetailService sectionDetailService, EnrollRepository enrollRepository, MyquizreportRepository myquizreportRepository) {
        this.courseService = courseService;
        this.coursecontentService = coursecontentService;
        this.disscussService = disscussService;
        this.feedbackdetailService = feedbackdetailService;
        this.userService = userService;
        this.enrollService = enrollService;
        this.contentStatusDetailService = contentStatusDetailService;
        this.sectionDetailService = sectionDetailService;
        this.enrollRepository = enrollRepository;
        this.myquizreportRepository = myquizreportRepository;
    }

    public LocalDateTime getCurrentDate() {
        return LocalDateTime.now();
    }

    @GetMapping("/mycourse/{id}")
    public ModelAndView showCourseContent(@PathVariable(value = "id") Integer id,
                                          @RequestParam(name = "contentId", required = false) Integer contentId,
                                          @RequestParam(name = "parentId", required = false) Integer parentId,
                                          HttpSession session) {
        Map<String, Object> modelMap = new HashMap<>();
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        Course course = courseService.getCourseByID(id);

        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            ModelAndView mv = new ModelAndView("redirect:/client/login");
            return mv;
        }
        Users userFind = userService.getUserByID(user.getUserId());
        Feedbackdetail userFeedback = feedbackdetailService.findFeedbackByUserAndCourse(userFind, course);
        if (userFeedback != null) {
            modelMap.put("userFeedback", userFeedback);
        } else {
            modelMap.put("userFeedback", null);
        }
        modelMap.put("user", user);
        session.setAttribute("course", course);
        session.setMaxInactiveInterval(60 * 60 * 24);
        Coursecontent coursecontent = coursecontentService.findByContentID(contentId);
        List<Disscuss> subComment;
        Disscuss parentCmtOne;
        if (coursecontent != null) {
            modelMap.put("coursecontent", coursecontent);
        } else {
            modelMap.put("coursecontent", coursecontentService.findFirstContent(course.getCourseid()));
        }
        if (parentId != null) {
            subComment = disscussService.findAllSubComment(id, parentId);
            parentCmtOne = disscussService.findParentById(id, parentId);
        } else {
            subComment = null;
            parentCmtOne = null;
        }
        modelMap.put("subComment", subComment);
        modelMap.put("course", course);
        modelMap.put("parentCmtOne", parentCmtOne);
        List<Disscuss> parentComment = disscussService.findAllParentComment(id);
        modelMap.put("parentComment", parentComment);
        Integer count = parentComment.size();
        modelMap.put("count", count);
        Enroll enroll = enrollService.findByUserAndCourse(userFind.getUserid(), course.getCourseid());
        modelMap.put("enroll", enroll);
        List<Sectiondetail> sectiondetails = sectionDetailService.findByEnrollID(enroll.getEnrollid());
        modelMap.put("sectiondetails", sectiondetails);
        List<ContentStatusDetail> contentStatusDetails = contentStatusDetailService.listContentStatusDetail(enroll.getEnrollid());
        int totalContent = 0;
        int totalleaned = 0;
        Boolean check = true;
        for (int i = 0; i < sectiondetails.size(); i++) {
            Quiz quiz = sectiondetails.get(i).getCoursesection().getQuizSet();
            List<Myquizreport> myquizreports = myquizreportRepository.findByUserAndQuiz(userFind, quiz);
            if(myquizreports !=null){
                for(Myquizreport myquizreport: myquizreports){
                    if(!myquizreport.getIspass()){
                        check=false;
                    }
                }
            }
            // else{
            //     check = false;
            // }

            Set<ContentStatusDetail> list = sectiondetails.get(i).getContentStatusDetailSet();
            for (var j : list) {
                if (j.isStatus()) {
                    totalleaned += 1;
                }
                totalContent += 1;
            }
        }
        System.out.println("Status: "+ check);
        modelMap.put("check", check);
        System.out.println("total learned" + totalleaned);
        System.out.println("total content " + totalContent);
        double progressRaw = ((double) totalleaned / totalContent) * 100;
        DecimalFormat df = new DecimalFormat("#,###.#");
        double progress = Double.parseDouble(df.format(progressRaw));
        if (progress == 100) {
            LocalDateTime datepost = getCurrentDate();
            enroll.setProcessstatus(true);
            enroll.setFinishdate(datepost);
            enrollRepository.save(enroll);
        }
        List<Myquizreport> myquizreportList = myquizreportRepository.findByUser(userFind);
        modelMap.put("progress", progress);
        modelMap.put("contentStatusDetails", contentStatusDetails);
        ModelAndView mv = new ModelAndView("pages/client/my-course-detail", modelMap);
        return mv;

    }

    @RequestMapping(path = "/saveVideoStatus", method = RequestMethod.POST)
    public RedirectView submitAnswer(@RequestBody Map<String, String> requestBody, HttpServletRequest
            request, HttpSession session) {
        String DonecontentId = requestBody.get("contentId");
        Integer contentId = Integer.parseInt(DonecontentId);
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        Users userFind = userService.getUserByID(user.getUserId());
        Coursecontent coursecontent = coursecontentService.findByContentID(contentId);
        Coursesection coursesection = coursecontent.getCoursesection();
        Course course = coursecontent.getCoursesection().getCourse();
        Enroll enroll = enrollService.findByUserAndCourse(userFind.getUserid(), course.getCourseid());
        Sectiondetail sectiondetail = sectionDetailService.findSectionDetail(enroll.getEnrollid(), coursesection.getSectionId());
        contentStatusDetailService.updateStatus(contentId, sectiondetail.getSectionDeatailId());
        System.out.println("Sucessfull");
        String url = request.getHeader("referer");
        return new RedirectView(url);
    }
}
