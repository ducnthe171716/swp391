package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Schedule;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.ScheduleRepository;
import com.swp391.onlinequiz.repository.UsersRepository;
import com.swp391.onlinequiz.service.EnrollService;
import com.swp391.onlinequiz.service.ScheduleService;
import com.swp391.onlinequiz.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
/* Modify your requestMapping */
@RequestMapping(path = "")  // the first path
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private EnrollService enrollService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private UserService userService;

    @RequestMapping(path = "/client/schedule" )
    public ModelAndView showSchedulePage(HttpSession session) {
        ModelAndView mv = new ModelAndView("pages/client/schedule");

        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/client/login");
        }
        List<Course> listAllMyRegister = enrollService.showMyCoursesByUserID(user.getUserId());
        List<Schedule> scheduleList = scheduleService.getAllByUserID(user.getUserId());
        mv.addObject("scheduleList", scheduleList);
        mv.addObject("sublistCourseRegister", listAllMyRegister);
        return mv;
    }
    @RequestMapping(path = "/client/addSchedule", method = RequestMethod.POST)
    public ModelAndView addSchedulePage(HttpSession session, @ModelAttribute("schedule") Schedule schedule){
        ModelAndView mv = new ModelAndView();
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/client/login");
            return mv;
        }
        List<Course> listAllMyRegister = enrollService.showMyCoursesByUserID(user.getUserId());
        List<Schedule> scheduleList = scheduleService.getAllByUserID(user.getUserId());
        mv.addObject("scheduleList", scheduleList);
        mv.addObject("sublistCourseRegister", listAllMyRegister);
        LocalDateTime setTime = LocalDateTime.now();
        LocalDateTime reminderAfter;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(schedule.getReminderafter().toString());
            reminderAfter = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            // Kiểm tra nếu ngày là tương lai
            if (reminderAfter.isBefore(LocalDateTime.now())) {
                mv.addObject("error", "Không thể đặt lịch nhắc trong quá khứ");
                mv.setViewName("pages/client/addSchedule"); // Điều hướng đến trang thông báo lỗi
                return mv;
            }
            Users u = new Users();
            u.setUserid(user.getUserId());
            schedule.setUser(u);
            schedule.setSettime(setTime);
            schedule.setCourse(schedule.getCourse());
            scheduleService.saveSchedule(schedule);
            mv.setViewName("pages/client/schedule");
        } catch (ParseException e) {
            mv.addObject("error", "Định dạng ngày tháng không hợp lệ.");
            mv.setViewName("pages/client/addSchedule"); // Điều hướng đến trang thông báo lỗi
            return mv;
        }
        return mv;
    }
    @RequestMapping(path = "/client/addSchedule", method = RequestMethod.GET)
    public ModelAndView showAddSchedulePage(HttpSession session){
        Schedule schedule = new Schedule();
        ModelAndView mv = new ModelAndView();
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/client/login");
            return mv;
        }
        List<Course> listAllMyRegister = enrollService.showMyCoursesByUserID(user.getUserId());
        mv.addObject("schedule", schedule);
        mv.addObject("sublistCourseRegister", listAllMyRegister);
        mv.setViewName("pages/client/addSchedule");
        return mv;
    }
}
