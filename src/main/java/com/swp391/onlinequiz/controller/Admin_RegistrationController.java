package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.service.CourseService;
import com.swp391.onlinequiz.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class Admin_RegistrationController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private EnrollService enrollService;
    @RequestMapping("/admin/user_registration")
    public ModelAndView showUserRegistration(@RequestParam(name = "courseId", defaultValue = "0") int courseID,
                                             @RequestParam(name = "page", defaultValue = "1") int page) {
        ModelAndView mv = new ModelAndView("pages/admin/myregistration");
        List<Course> courseList = courseService.getAll();
        mv.addObject("courseList", courseList);
        if (courseID==0){
            return mv;
        }
        else {
            List<Users> userListByCourse = enrollService.listUsersByCourseID(courseID);
            List<Users> listForCurrentPage = new ArrayList<>();
            int totalPages = 0;
            if (!userListByCourse.isEmpty()){
                int pageSize = 4; // Số lượng khóa học trên mỗi trang
                totalPages = (int) Math.ceil((double) userListByCourse.size() / pageSize);

                // Kiểm tra xem trang yêu cầu có hợp lệ không
                if (page < 1 || page > totalPages) {
                    // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                    mv.setViewName("redirect:/admin/user_registration?page=1"); // Chuyển hướng về trang đầu tiên
                    return mv;
                }
                // Xác định các khóa học cho trang hiện tại
                int startIndex = (page - 1) * pageSize;
                int endIndex = Math.min(startIndex + pageSize, userListByCourse.size());
                listForCurrentPage = userListByCourse.subList(startIndex, endIndex);
            }

            mv.addObject("userList", listForCurrentPage);
            mv.addObject("pageNum", totalPages);
            mv.addObject("currentPage", page);
            mv.addObject("courseId", courseID);
            return mv;
        }
    }
}
