package com.swp391.onlinequiz.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/content")
public class VideoStatusController {

    @PostMapping("/saveVideoStatus")
    public ResponseEntity<String> saveVideoStatus(@RequestBody Map<String, Object> jsonData) {
        // Lấy contentId từ dữ liệu yêu cầu
        String contentId = (String) jsonData.get("contentId");
        System.out.println(contentId);
        // Trả về phản hồi
        return ResponseEntity.ok("Dữ liệu video đã được lưu trữ thành công.");
    }
}