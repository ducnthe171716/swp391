package com.swp391.onlinequiz.controller;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Coursecontent;
import com.swp391.onlinequiz.model.Disscuss;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.DisscussRepository;
import com.swp391.onlinequiz.service.CourseService;
import com.swp391.onlinequiz.service.CoursecontentService;
import com.swp391.onlinequiz.service.DisscussService;
import com.swp391.onlinequiz.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Controller
@RequestMapping(path = "/disscuss")
public class DisscussController {
    public DisscussService disscussService;
    public UserService userService;
    public CourseService courseService;
    public CoursecontentService coursecontentService;

    public DisscussController(DisscussService disscussService, UserService userService, CourseService courseService,CoursecontentService coursecontentService) {
        this.disscussService = disscussService;
        this.userService = userService;
        this.courseService = courseService;
        this.coursecontentService = coursecontentService;
    }

    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }
    @PostMapping("/savecomment")
    public String saveEmployee(@RequestParam("content") String content,
                             @RequestParam("userId") int userId,
                             @RequestParam("courseID") Integer courseID,
                             @RequestParam(value = "parentID", required = false) Integer parentID ,
                               @RequestParam(value = "contentId",required = false) Integer contentId  ) {
        String datepost = getCurrentDate().toString();
        Course course = courseService.getCourseByID(courseID);
        Users user = userService.getUserByID(userId);
        if(parentID != null){
            Disscuss disscuss = new Disscuss(course,user,parentID,content,datepost);
            disscussService.savecomment(disscuss);
        }
        if(contentId != null){
            Coursecontent coursecontent = coursecontentService.findByContentID(contentId);
            Disscuss disscuss = new Disscuss(course,user,coursecontent,content,datepost);
            disscussService.savecomment(disscuss);
        }
        return "redirect:/coursecontent/mycourse/"+courseID;
    }
}
