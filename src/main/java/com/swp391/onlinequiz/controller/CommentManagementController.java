package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Disscuss;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.service.CourseService;
import com.swp391.onlinequiz.service.DisscussService;
import com.swp391.onlinequiz.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CommentManagementController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private DisscussService disscussService;

    @RequestMapping("/admin/comment-management")
    public ModelAndView showUserRegistration(@RequestParam(name = "courseId", defaultValue = "0") int courseID,
                                             @RequestParam(name = "page", defaultValue = "1") int page
    ) {
        ModelAndView mv = new ModelAndView("pages/admin/comment-management");
        List<Course> courseList = courseService.getAll();
        mv.addObject("courseList", courseList);
        if (courseID == 0) {
            List<Disscuss> duplicate = disscussService.findDisscussDuplicate();
            List<Disscuss> listForCurrentPage = new ArrayList<>();
            int totalPages = 0;
            if (!duplicate.isEmpty()) {
                int pageSize = 4; // Số lượng khóa học trên mỗi trang
                totalPages = (int) Math.ceil((double) duplicate.size() / pageSize);

                // Kiểm tra xem trang yêu cầu có hợp lệ không
                if (page < 1 || page > totalPages) {
                    // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                    mv.setViewName("redirect:/admin/comment-management?page=1"); // Chuyển hướng về trang đầu tiên
                    return mv;
                }
                // Xác định các khóa học cho trang hiện tại
                int startIndex = (page - 1) * pageSize;
                int endIndex = Math.min(startIndex + pageSize, duplicate.size());
                listForCurrentPage = duplicate.subList(startIndex, endIndex);
            }
            mv.addObject("duplicate",listForCurrentPage);
            mv.addObject("pageNum", totalPages);
            mv.addObject("currentPage", page);
            mv.addObject("courseId", courseID);
            return mv;
        } else {
            List<Disscuss> commentListByCourse = disscussService.findAllCommentById(courseID);
            List<Disscuss> listForCurrentPage = new ArrayList<>();
            int totalPages = 0;
            if (!commentListByCourse.isEmpty()) {
                int pageSize = 4; // Số lượng khóa học trên mỗi trang
                totalPages = (int) Math.ceil((double) commentListByCourse.size() / pageSize);

                // Kiểm tra xem trang yêu cầu có hợp lệ không
                if (page < 1 || page > totalPages) {
                    // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                    mv.setViewName("redirect:/admin/comment-management?page=1"); // Chuyển hướng về trang đầu tiên
                    return mv;
                }
                // Xác định các khóa học cho trang hiện tại
                int startIndex = (page - 1) * pageSize;
                int endIndex = Math.min(startIndex + pageSize, commentListByCourse.size());
                listForCurrentPage = commentListByCourse.subList(startIndex, endIndex);
            }

            mv.addObject("commentList", listForCurrentPage);
            mv.addObject("pageNum", totalPages);
            mv.addObject("currentPage", page);
            mv.addObject("courseId", courseID);
            return mv;
        }
    }

    @RequestMapping("/admin/comment-management/hide-comment")
    public String hideComment(@RequestParam(name = "commentId", defaultValue = "0") int commentId,
                              @RequestParam(name = "courseId", defaultValue = "0") int courseID,
                              @RequestParam(name = "page", defaultValue = "1") int page) {
        if(commentId != 0){
            disscussService.changeStatus(commentId, true);
        }
        return "redirect:/admin/comment-management?courseId="+courseID+"&page="+page;
    }
    @RequestMapping("/admin/comment-management/unhide-comment")
    public String unhideComment(@RequestParam(name = "commentId", defaultValue = "0") int commentId,
                                @RequestParam(name = "courseId", defaultValue = "0") int courseID,
                                @RequestParam(name = "page", defaultValue = "1") int page) {
        if(commentId != 0){
            disscussService.changeStatus(commentId, false);
        }
        return "redirect:/admin/comment-management?courseId="+courseID+"&page="+page;
    }
}
