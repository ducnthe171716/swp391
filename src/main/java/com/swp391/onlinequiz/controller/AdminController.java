package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.model.Category;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.ProgressStatus;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.AdminRepository;
import com.swp391.onlinequiz.repository.CourseRepository;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.repository.ProgressStatusRepository;
import com.swp391.onlinequiz.repository.UsersRepository;
import com.swp391.onlinequiz.service.AdminService;
import com.swp391.onlinequiz.service.EnrollService;
import com.swp391.onlinequiz.serviceimpl.AdminServiceImpls;
import com.swp391.onlinequiz.serviceimpl.CategoryServiceImpl;
import com.swp391.onlinequiz.serviceimpl.CourseServiceImpl;

import org.apache.catalina.User;
import org.checkerframework.checker.units.qual.min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class AdminController {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    AdminService adminService;
    @Autowired
    private AdminServiceImpls adminServiceImpls;
    @Autowired
    private UsersRepository userRepository;
    @Autowired
    private UsersRepository usersRepository;


    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseServiceImpl courseServiceImpl;

    @Autowired
    private ProgressStatusRepository progressStatusRepository;

    @GetMapping("admin/filter")
    public ModelAndView courseControlFilter(@RequestParam(name = "flt") Integer flt ){
        
        ModelAndView mv = new ModelAndView("pages/admin/course-manager");
        List<Course> selectfilterc = new ArrayList<>();
        if(flt == 3){
            ProgressStatus psactive = progressStatusRepository.findByStatusId(3).get(0);
            selectfilterc = courseRepository.findByStatus(psactive);
        }else if(flt == 2){
            ProgressStatus psunactive = progressStatusRepository.findByStatusId(2).get(0);
            selectfilterc = courseRepository.findByStatus(psunactive);
        }else{
            ProgressStatus psactive = progressStatusRepository.findByStatusId(3).get(0);
            ProgressStatus psunactive = progressStatusRepository.findByStatusId(2).get(0);

            
            List<Course> activecourse = courseRepository.findByStatus(psactive);
            List<Course> unactivecourse = courseRepository.findByStatus(psunactive);
            selectfilterc.addAll(unactivecourse);
            selectfilterc.addAll(activecourse);
        }
        

        
        

        mv.addObject("allcourse", selectfilterc);
        mv.addObject("flt", flt);
        return mv;

        
    }

    @GetMapping("admin/control")
    public ModelAndView courseControl(@RequestParam(name = "cid") Integer cid ){
        

        Course editc = courseServiceImpl.getCourseByID(cid);
        ProgressStatus current = editc.getStatus();
        
        if(current.getStatusId() == 3){
            ProgressStatus incoming = progressStatusRepository.findByStatusId(2).get(0);
            editc.setStatus(incoming);
        }else{
            ProgressStatus incoming = progressStatusRepository.findByStatusId(3).get(0);
            editc.setStatus(incoming);
        }
        courseRepository.save(editc);

        ModelAndView mv = new ModelAndView("pages/admin/course-manager");
        ProgressStatus psactive = progressStatusRepository.findByStatusId(3).get(0);
        ProgressStatus psunactive = progressStatusRepository.findByStatusId(2).get(0);

        List<Course> allcourse = new ArrayList<>();
        List<Course> activecourse = courseRepository.findByStatus(psactive);
        List<Course> unactivecourse = courseRepository.findByStatus(psunactive);
        allcourse.addAll(unactivecourse);
        allcourse.addAll(activecourse);

        mv.addObject("allcourse", allcourse);
        return mv;

        
    }

    @GetMapping("/admin/course-manager")
    public ModelAndView courseManager(){
        ModelAndView mv = new ModelAndView("pages/admin/course-manager");
        ProgressStatus psactive = progressStatusRepository.findByStatusId(3).get(0);
        ProgressStatus psunactive = progressStatusRepository.findByStatusId(2).get(0);

        List<Course> allcourse = new ArrayList<>();
        List<Course> activecourse = courseRepository.findByStatus(psactive);
        List<Course> unactivecourse = courseRepository.findByStatus(psunactive);
        allcourse.addAll(unactivecourse);
        allcourse.addAll(activecourse);

        mv.addObject("allcourse", allcourse);
        mv.addObject("flt", 4);
        return mv;
    }

    @GetMapping("/admin/view-register")
    public String showViewRegister(Model model) {
        List<Users> user = adminServiceImpls.listAllRegister();
        model.addAttribute("users", user);
        return "pages/admin/index";
    }
    @Autowired
    private CategoryServiceImpl categoryServiceImpl;

    @Autowired
    private EnrollRepository enrollRepository;

    @GetMapping("/admin/dashboard")
    public String showViewRegister(Model model, @RequestParam(name = "selectedYear", required = false) Integer selectedYear) {
        List<Users> user = adminServiceImpls.listAllRegister();
        model.addAttribute("users", user);

        List<Double> revenue = new ArrayList<>();
        List<Integer> year = enrollRepository.findDistinctEnrollyears();
        List<Integer> yearrange = new ArrayList<>();
        if (selectedYear != null) {
            for (int i = 0; i < 12; i++) {
                List<Enroll> me = enrollRepository.findByEnrolldate(i + 1, selectedYear);
                double totalamonth = 0;
                for (Enroll r : me) {
                    totalamonth += r.getCheckout();
                }
                revenue.add(totalamonth);
            }
        }else{
            selectedYear = year.get(0);
            for (int i = 0; i < 12; i++) {
                List<Enroll> me = enrollRepository.findByEnrolldate(i + 1, selectedYear);
                double totalamonth = 0;
                for (Enroll r : me) {
                    totalamonth += r.getCheckout();
                }
                revenue.add(totalamonth);
            }
        }
        //month of year
        Map<String, Object> chartData = new HashMap<>();
        chartData.put("categories", Arrays.asList("\"Jan\"", "\"Feb\"", "\"Mar\"", "\"Apr\"", "\"May\"", "\"Jun\"",
                "\"Jul\"", "\"Aug\"", "\"Sep\"", "\"Oct\"", "\"Nov\"", "\"Dec\""));
        chartData.put("seriesData", revenue);
        model.addAttribute("data", chartData);

        if(selectedYear != null){
            model.addAttribute("selectyear", selectedYear);
        }else{
            model.addAttribute("selectyear", year.get(0));
        }

        //year
        int miny = year.get(year.size()-1);
        int maxy = year.get(0);
        int cyear = miny;
        List<Double> yearincome = new ArrayList<>();
        List<String> years = new ArrayList<>();


        for(int i = 0; i <= maxy-miny; i++){
            Double totalayear = 0.00;
            for (int j = 0; j < 12; j++) {
                List<Enroll> me = enrollRepository.findByEnrolldate(j + 1, cyear);
                double totalamonth = 0;
                for (Enroll r : me) {
                    totalamonth += r.getCheckout();
                }
                totalayear += totalamonth;

            }
            yearincome.add(totalayear);
            years.add("\""+cyear+"\"");
            yearrange.add(cyear);
            cyear++;
        }
        Map<String, Object> yearData = new HashMap<>();
        yearData.put("categories", years);
        yearData.put("seriesData", yearincome);
        model.addAttribute("data2", yearData);

        model.addAttribute("year", yearrange);
        List<Category> lc = categoryServiceImpl.getAll();

        String data = "";
        for (Category category : lc) {
            data += "{ \"name\": \"" + category.getCategoryname() + "\", \"y\": " + category.getCourseSet().size() + " },";
        }

        model.addAttribute("piedata", "[" + data.substring(0, data.length() - 1) + "]");

        return "pages/admin/index";
    }



    @GetMapping("/admin/view-user")
    public String showViewDetailUser(Model model, @RequestParam(name = "userid") int userid) {
        Users user = adminRepository.findByUserid(userid);
        model.addAttribute("user", user);
        return "pages/admin/user-profile";
    }

    @GetMapping("/admin/activate")
    public String activate(Model model, @RequestParam(name = "userid") int userid) {
        Users user = adminRepository.findByUserid(userid);
        user.setIsenabled(!user.getIsenabled());
        adminRepository.save(user);
        return "redirect:/admin/view-account";
    }

    @GetMapping("/admin/view-account")
    public String view(Model model, @RequestParam(name = "email", defaultValue = "") String email,
            @RequestParam(name = "id", defaultValue = "0") String id) {
        int roleId = Integer.parseInt(id);
        List<Users> users = new ArrayList<>();

        if (roleId == 0) {
            users = userRepository.findAllByEmail(email);
            // Loại bỏ người dùng có vai trò "admin" (điều này phụ thuộc vào tên cột và giá
            // trị của vai trò trong cơ sở dữ liệu)
            users.removeAll(adminRepository.findBy1Role("ROLE_ADMIN"));
        } else if (roleId == 1) {
            // Truy vấn danh sách người dùng có vai trò "User" từ cơ sở dữ liệu
            List<Users> userUsers = adminRepository.findBy1Role("ROLE_MENTEE");
            // Lọc danh sách theo email nếu cần
            if (!email.isEmpty()) {
                userUsers = userUsers.stream()
                        .filter(user -> user.getEmail().contains(email))
                        .collect(Collectors.toList());
            }
            users = userUsers;
        } else if (roleId == 2) {
            // Truy vấn danh sách người dùng có vai trò "Course Owner" từ cơ sở dữ liệu
            List<Users> courseOwners = adminRepository.findBy1Role("ROLE_COURSEOWNER");
            // Lọc danh sách theo email nếu cần
            if (!email.isEmpty()) {
                courseOwners = courseOwners.stream()
                        .filter(user -> user.getEmail().contains(email))
                        .collect(Collectors.toList());
            }
            users = courseOwners;
        } else {
            // Thực hiện truy vấn tùy ý dựa trên cơ sở dữ liệu của bạn
            // Đảm bảo rằng bạn thay thế "userRepository" bằng repository tương ứng với cơ
            // sở dữ liệu của bạn
            // Ví dụ: users = yourCustomRepository.findByEmailAndRoleId(email, id);
        }

        List<String> roles = Arrays.asList("All", "User", "Course Owner");
        model.addAttribute("id", roleId);
        model.addAttribute("users", users);
        model.addAttribute("roles", roles);
        model.addAttribute("selectedRole", id); // Đây là để chọn giá trị mặc định trong dropdown
        model.addAttribute("email", email);

        return "pages/admin/account-manager";
    }

    @GetMapping("/admin/create-account")
    public String ShowCreateAccount(Model model, Users users) {

        model.addAttribute("users", users);
        return "pages/admin/Create-account";
    }

    @PostMapping("/admin/create-account")
    public String createAccount(Model model, Users users, @RequestParam(name = "id", defaultValue = "0") String id,
            @RequestParam(name = "date", required = false) String dateStr) {
        model.addAttribute("users", users);
        int roleId = Integer.parseInt(id);

        if (usersRepository.existsByEmail(users.getEmail())) {
            model.addAttribute("error", "This email already exists!");
            return "pages/admin/Create-account";
        }
        if (usersRepository.existsByUsername(users.getUsername())) {
            model.addAttribute("error", "This username already exists!");
            return "pages/admin/Create-account";
        }
        if (users.getUsername().isEmpty()) {
            model.addAttribute("error", "user required");
            return "pages/admin/Create-account";
        }
        // Xử lý ngày giờ nếu date không null và có định dạng hợp lệ

        LocalDateTime dob;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(dateStr);
            dob = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            // Kiểm tra nếu ngày là tương lai
            if (dob.isAfter(LocalDateTime.now())) {
                model.addAttribute("error",
                        "Date and time cannot be in the future. Please provide a valid date and time.");
                return "pages/admin/Create-account";
            }
        } catch (ParseException e) {
            // Xử lý tình huống khi định dạng ngày giờ không hợp lệ
            model.addAttribute("error", "Invalid date format. Please provide a valid date.");
            return "pages/admin/Create-account";
        }


        // Đảm bảo định rõ giới tính (boolean)
        users.setGender(users.getGender() != null && users.getGender());

        if (roleId == 0) {
            users.setRole("ROLE_COURSEOWNER");
        } else {
            users.setRole("ROLE_ADMIN");
        }

        adminService.CreateAccountByAdmin(users);
        model.addAttribute("message", "Create account successfully!");
        return "pages/admin/Create-account";
    }

}
