package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Category;
import com.swp391.onlinequiz.model.Course;

import com.swp391.onlinequiz.model.Courseowner;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.service.CategoryService;
import com.swp391.onlinequiz.service.CourseownerService;
import com.swp391.onlinequiz.service.EnrollService;
import com.swp391.onlinequiz.serviceimpl.EnrollServiceImpl;

import jakarta.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("")
public class EnrollController {
    private final EnrollService enrollService;

    @Autowired
    private EnrollRepository enrollRepository;

    private final CategoryService categoryService;

    private final CourseownerService courseownerService;

    public EnrollController(EnrollService enrollService, CategoryService categoryService, CourseownerService courseownerService) {
        this.enrollService = enrollService;
        this.categoryService = categoryService;
        this.courseownerService = courseownerService;
    }

    @RequestMapping("/client/mycourse")
    public ModelAndView showProfilePage(@RequestParam(name = "page", defaultValue = "1") int page, HttpSession session,
                                        @RequestParam(name = "search", required = false) String search,
                                        @RequestParam(name = "categoryID", defaultValue = "all", required = false) String categoryID,
                                        @RequestParam(name = "sort", defaultValue = "A-Z", required = false) String sort,
                                        @RequestParam(name = "ownerID", defaultValue = "all", required = false) String courseOwnerID) {
        ModelAndView mv = new ModelAndView("pages/client/mycourse");

        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/client/login");
            return mv;
        }

        List<Enroll> finishcourse = enrollRepository.findFinishCourse(user.getUserId());

        List<Course> listAllMyRegister = enrollService.showMyCoursesByUserID(user.getUserId());

        if (search != null) {
            mv.addObject("search", search);
            listAllMyRegister = enrollService.searchCoursesByName(search);
        }
        if (categoryID != null && courseOwnerID != null) {
            if (categoryID.equals("all")){
                if(courseOwnerID.equals("all")){

                }else{
                    mv.addObject("courseOwnerID", courseOwnerID);
                    listAllMyRegister = enrollService.listCoursesByCourseOwnerID(user.getUserId(),Integer.parseInt(courseOwnerID));
                }
            }else {
                if (courseOwnerID.equals("all")){
                    mv.addObject("categoryID", categoryID);
                    listAllMyRegister = enrollService.listCoursesByCategoryID(user.getUserId(),Integer.parseInt(categoryID));
                }else {
                    mv.addObject("categoryID", categoryID);
                    mv.addObject("courseOwnerID", courseOwnerID);
                    List<Course> listCate = enrollService.listCoursesByCategoryID(user.getUserId(),Integer.parseInt(categoryID));
                    List<Course> listOwn = enrollService.listCoursesByCourseOwnerID(user.getUserId(),Integer.parseInt(courseOwnerID));
                    listAllMyRegister = new ArrayList<>();
                    for (Course course : listCate) {
                        if (listOwn.contains(course)) {
                            listAllMyRegister.add(course);
                        }
                    }
                }
            }
        }

        if(!listAllMyRegister.isEmpty()){
            if ("A-Z".equals(sort)) {
                // Sắp xếp danh sách khóa học theo A-Z
                listAllMyRegister.sort(Comparator.comparing(Course::getCoursename));
            } else if ("Z-A".equals(sort)) {
                // Sắp xếp danh sách khóa học theo Z-A
                listAllMyRegister.sort((course1, course2) -> course2.getCoursename().compareTo(course1.getCoursename()));
            }
        }
        List<Course> coursesForCurrentPage = new ArrayList<>();
        int totalPages = 0;
        if (!listAllMyRegister.isEmpty()){
            int pageSize = 6; // Số lượng khóa học trên mỗi trang
            totalPages = (int) Math.ceil((double) listAllMyRegister.size() / pageSize);

            // Kiểm tra xem trang yêu cầu có hợp lệ không
            if (page < 1 || page > totalPages) {
                // Trang không hợp lệ, có thể xử lý redirect hoặc thông báo lỗi tùy ý
                mv.setViewName("redirect:/client/mycourse?page=1"); // Chuyển hướng về trang đầu tiên
                return mv;
            }
            // Xác định các khóa học cho trang hiện tại
            int startIndex = (page - 1) * pageSize;
            int endIndex = Math.min(startIndex + pageSize, listAllMyRegister.size());
            coursesForCurrentPage = listAllMyRegister.subList(startIndex, endIndex);
        }

        mv.addObject("sublistCourseRegister", coursesForCurrentPage);
        mv.addObject("pageNum", totalPages);
        mv.addObject("currentPage", page);
        mv.addObject("finishcourse", finishcourse);
        List<Category> courseCategory = categoryService.getAll();
        mv.addObject("courseCategory", courseCategory);
        List<Courseowner> courseOwner = courseownerService.getAll();
        mv.addObject("courseOwner", courseOwner);




        return mv;
    }

}