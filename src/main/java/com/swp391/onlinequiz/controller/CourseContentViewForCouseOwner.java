package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.*;
import com.swp391.onlinequiz.service.*;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/courseview")
public class CourseContentViewForCouseOwner {
    private  CourseService courseService;
    private CoursecontentService coursecontentService;
    private DisscussService disscussService;
    private FeedbackdetailService feedbackdetailService;
    private UserService userService;
    @Autowired
    private HttpSession httpSession;

    public CourseContentViewForCouseOwner(CourseService courseService, CoursecontentService coursecontentService, DisscussService disscussService, FeedbackdetailService feedbackdetailService, UserService userService) {
        this.courseService = courseService;
        this.coursecontentService = coursecontentService;
        this.disscussService = disscussService;
        this.feedbackdetailService = feedbackdetailService;
        this.userService = userService;
    }
    @GetMapping("/mycourse/{id}")
    public ModelAndView showCourseContent(@PathVariable(value ="id") Integer id,
                                          @RequestParam(name = "contentId", required = false) Integer contentId,
                                          HttpSession session) {
        Map<String, Object> modelMap = new HashMap<>();
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            ModelAndView mv = new ModelAndView("redirect:/client/login");
            return mv;
        }
        System.out.println("Au mai got");
        modelMap.put("user",user);
        Course course = courseService.getCourseByID(id);
        System.out.println("CourseID" +  course.getCourseid());
        session.setAttribute("course", course);
        modelMap.put("course", course);
        session.setMaxInactiveInterval(60 * 60 * 24);
        Coursecontent coursecontent = coursecontentService.findByContentID(contentId);
        if(coursecontent != null){
            modelMap.put("coursecontent",coursecontent);
        }
        else {
            modelMap.put("coursecontent",coursecontentService.findFirstContent(course.getCourseid()));
        }
        ModelAndView mv = new ModelAndView("pages/client/my-course-detail-for-courseowner",modelMap);
        return mv;
    }
}
