package com.swp391.onlinequiz.controller;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Category;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Courseowner;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.CourseownerRepository;
import com.swp391.onlinequiz.repository.UsersRepository;
import com.swp391.onlinequiz.service.CategoryService;
import com.swp391.onlinequiz.service.EnrollService;
import com.swp391.onlinequiz.service.FileUploadService;
import com.swp391.onlinequiz.service.UserService;
import com.swp391.onlinequiz.serviceimpl.CourseOwnerImpls;
import com.swp391.onlinequiz.serviceimpl.UserServiceImpl;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Controller
@RequestMapping(path = "")
public class ProfileController {
    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private final EnrollService enrollService;
    @Autowired
    FileUploadService fileUploadService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    UserServiceImpl userServiceImpl;

    public ProfileController(EnrollService enrollService) {
        this.enrollService = enrollService;
    }

    @Autowired
    private CourseOwnerImpls courseOwnerImpls;

    @Autowired
    private CourseownerRepository courseownerRepository;

    @PostMapping("/client/editProfile")
    public ModelAndView changeProfilePage(@RequestParam("fullname") String fullname, @RequestParam("email") String email,
                                          @RequestParam("dob") String dobStr, @RequestParam("address") String address,
                                          @RequestParam(value = "aboutme", required = false) String aboutme,
                                          @RequestParam("gender") String genderStr, HttpSession session) {
        ModelAndView mv = new ModelAndView("/pages/client/profile");

        System.out.println("dobStr: " + dobStr);

        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        

        // Kiểm tra định dạng của dobStr
        LocalDateTime dob;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(dobStr);
            dob = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            // Kiểm tra nếu ngày là tương lai
            if (dob.isAfter(LocalDateTime.now())) {
                mv.addObject("error", "Ngày tháng không thể nằm trong tương lai.");
                mv.setViewName("pages/client/profile"); // Điều hướng đến trang thông báo lỗi
                return mv;
            }
        } catch (ParseException e) {
            mv.addObject("error", "Định dạng ngày tháng không hợp lệ.");
            mv.setViewName("pages/client/profile"); // Điều hướng đến trang thông báo lỗi
            return mv;
        }
        boolean gender = false; // Giá trị mặc định nếu không thể chuyển đổi
        if ("true".equals(genderStr)) {
            gender = true;
        }

        if(aboutme != null){
            Users u = usersRepository.findById(user.getUserId()).get();
            Courseowner c = courseownerRepository.findByUser(u).get(0);
            c.setAboutme(aboutme);
            System.out.println(aboutme);
            courseownerRepository.save(c);
            mv.addObject("aboutme", aboutme);
        }
        
        // đổi thông tin cá nhân
        userService.updateUserProfile(user.getUserId(), fullname, email, dob, address, gender);
        UsersDTO newu = userServiceImpl.getUserDTObyUserid(user.getUserId());
        // Điều hướng người dùng đến trang hiển thị thông tin sau khi cập nhật
        session.removeAttribute("user");
        session.setAttribute("user", newu);
        mv.setViewName("pages/client/profile");
        return mv;
    }

    @PostMapping("/client/changePassword")
    public ModelAndView changePasswordPage(HttpSession session,
                                           @RequestParam("opass") String oldPassword,
                                           @RequestParam("npass") String newPassword,
                                           @RequestParam("rnpass") String renewPassword){
        ModelAndView mv = new ModelAndView("pages/client/profile");
        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/user/login");
        }
        // đổi thông tin cá nhân
        if (newPassword.equals(renewPassword)){
            if (userService.changePassword(user.getUserId(), oldPassword, newPassword)){
                session.removeAttribute("user");
                mv.setViewName("redirect:/home");
            }else{
                mv.addObject("wrongPass", "Mật khẩu cũ không khớp!");
            }
        }else{
            mv.addObject("wrongPass", "Nhập lại mật khẩu mới không đúng!");
        }
        return mv;
    }

    @PostMapping("/client/changeAvatar")
    public ModelAndView changeAvatarPage(HttpSession session,
                                         @RequestParam("avatar") MultipartFile avatarImage) throws IOException {
        ModelAndView mv = new ModelAndView("pages/client/profile");
        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/user/login");
        }else{
            // đổi avatar
            String avatar = fileUploadService.uploadFile(avatarImage);
            user.setAvatar(avatar);
            userService.changeAvatar(user.getUserId(), avatar);
            mv.setViewName("redirect:/home");
            return mv;
        }
        return mv;
    }

    @RequestMapping("/client/profile")
    public ModelAndView showProfilePage(HttpSession session) {
        ModelAndView mv = new ModelAndView("pages/client/profile");
        // Kiểm tra xem người dùng đã đăng nhập hay chưa
        UsersDTO user = (UsersDTO) session.getAttribute("user");
        if (user == null) {
            // Nếu chưa đăng nhập, bạn có thể điều hướng họ đến trang đăng nhập
            mv.setViewName("redirect:/client/login");
            return mv;
        }
        if(user.getRole().equals("ROLE_COURSEOWNER")){
            Users u = usersRepository.findById(user.getUserId()).get();
            Courseowner c = courseownerRepository.findByUser(u).get(0);
            String aboutme = c.getAboutme();
            System.out.println(aboutme);
            mv.addObject("aboutme", aboutme);
        }
        List<Category> listCategory = categoryService.getAll();
        mv.addObject("listCate", listCategory);
        return mv;
    }


}
