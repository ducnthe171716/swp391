package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Disscuss;
import com.swp391.onlinequiz.model.Feedbackdetail;
import com.swp391.onlinequiz.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Map;

public interface FeedbackdetailService {
    Map<Integer, Integer> countRating (Integer courseid);
    void saveFeedback(Feedbackdetail feedbackdetail);
    Feedbackdetail findFeedbackByUserAndCourse(Users user, Course course);

    void updateFeedback(Integer rating,String datepost, String opinion,Integer userid);



}
