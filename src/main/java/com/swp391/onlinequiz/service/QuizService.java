package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Question;
import com.swp391.onlinequiz.model.Quiz;

import java.time.LocalDateTime;
import java.util.List;

public interface QuizService {
    public Quiz getQuizByQuizId(Integer id);
    public void save(Quiz quiz);
    public List<Quiz> getQuizBySectionID(Integer sectionID);
    public void updateQuiz(String title, Integer time, Integer randquestion, Double passpoint, LocalDateTime quizdob, Integer quizid);
    public void deleteQuiz(Integer idQuiz);

}
