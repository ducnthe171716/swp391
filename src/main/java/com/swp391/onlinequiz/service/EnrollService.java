package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Users;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EnrollService {
    public List<Course> showMyCoursesByUserID(int userID);

    public List<Course> searchCoursesByName(String searchTerm);

    public List<Course> listCoursesByCategoryID(int userID, int category);

    public List<Course> listCoursesByCourseOwnerID(int userID, int courseOwnerID);
    Page<Enroll> listRevenueByCourseOwnerID(int courseOwnerID, int page, int size);
    public List<Users> listUsersByCourseID(int courseID);
    Enroll findByUserAndCourse(Integer userid, Integer courseid);

}

