package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Coursecontent;

public interface CoursecontentService {
    public Coursecontent findByContentID(Integer contentId);
    public Coursecontent findFirstContent(Integer courseID);
}
