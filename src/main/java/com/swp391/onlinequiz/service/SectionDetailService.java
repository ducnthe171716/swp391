package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.ContentStatusDetail;
import com.swp391.onlinequiz.model.Sectiondetail;

import java.util.List;

public interface SectionDetailService {
     List<Sectiondetail> findByEnrollID(Integer enrollid);
     Sectiondetail findSectionDetail(Integer enrollid, Integer sectionid);
}
