package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface QuestionService {
    public Question getQuestionById(Integer id);
    public List<Question> getQuestionBySectionId(Integer sectionId);
    void save(Question question);
    void updateQuestion(String title, Integer quizid);
    void deleteQuestion(Integer id);
    Page<Question> getQuestionByQuizId(Pageable pageable, Integer id);
}
