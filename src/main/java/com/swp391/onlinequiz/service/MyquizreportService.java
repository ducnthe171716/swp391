package com.swp391.onlinequiz.service;

import java.util.List;
import java.util.Map;

import com.swp391.onlinequiz.model.Myquizreport;
import com.swp391.onlinequiz.model.Quiz;
import com.swp391.onlinequiz.model.Users;

public interface MyquizreportService {
    public List<Myquizreport> getMyQuizReport(Users u, Quiz q);

    public Myquizreport getMyQuizReportById(Integer qrid);

    public void restoreQuizReport(Users u, Map<String, List<String>> resultdata, String[] flagq);

    public Myquizreport createMyQuizReport(Quiz q, Users u);
}
