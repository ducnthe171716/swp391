package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Coursesection;

public interface CoursesectionService {
    Coursesection findById(Integer id);

}
