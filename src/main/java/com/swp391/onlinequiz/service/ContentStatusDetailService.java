package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.ContentStatusDetail;

import java.util.List;

public interface ContentStatusDetailService {
     void updateStatus(Integer courseid, Integer enrollid);
     List<ContentStatusDetail> listContentStatusDetail(Integer enrollid);
}
