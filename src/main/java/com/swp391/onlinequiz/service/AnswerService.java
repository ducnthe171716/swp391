package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Answer;
import com.swp391.onlinequiz.model.Questionreport;
import com.swp391.onlinequiz.model.Users;

import java.util.List;

public interface AnswerService {
    public void removeAnswerReport(Users u, Questionreport qr);
    List<Answer> getAnswerByQuesID(Integer questionID);
    void saveAnswer (Answer answer);
    void updateAnswer(String title, Integer idQuestion);
    void deleteAnswer(Integer id);
    void updateRightAnswer(Integer id);
}
