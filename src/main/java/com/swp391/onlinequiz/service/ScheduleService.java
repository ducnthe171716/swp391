package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Schedule;

import java.util.List;

public interface ScheduleService {

    List<Schedule> getAllByUserID(int id);

    public void saveSchedule(Schedule schedule);
}

