package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Courseowner;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseownerService {
    public List<Courseowner> getAll();
    public Courseowner getCourseownerById(int id);

}
