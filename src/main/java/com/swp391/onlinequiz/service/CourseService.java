package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CourseService {
    public Page<Course> findPaginated(Pageable pageable, List<Course> courses);

    public List<Course> getAll();

    public List<Course> getCourseByCategory(Integer categoryID);

    public List<Course> getCourseBySearchValue(String searchValue);

    public Course getCourseByID(int id);

    public Page<Course> getCourseByOwnerID(Pageable pageable,int id,String searchValue,Integer reviews,Integer status,String orderBy,Integer categoryID);

    public Course save(Course course);


}
