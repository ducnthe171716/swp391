package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface AdminService{
    public List<Users> listAllRegister();

    public void CreateAccountByAdmin(@ModelAttribute("user") Users user);

    public String generatePassword();
}
