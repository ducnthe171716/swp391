package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Category;

import java.util.List;

public interface CategoryService {
    public List<Category> getAll();

    public Category getCategoryByID(int id);

    public Category saveCategory(String categoryname);
}
