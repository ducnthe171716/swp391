package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.ExceptionHandling.UsersNotfoundException;
import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Users;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.List;

public interface UserService {
    public UsersDTO getUserDTObyUserid(Integer uid);

    public List<UsersDTO> getUserList();

    public UsersDTO getLoginUser(String identify, String password);

    public String saveUser(Users user);

    public ModelAndView confirmEmail(String confirmationToken);

    public Users getUserByUserName(String username);

    public void updateResetPasswordToken(String token, String email) throws UsersNotfoundException;

    public Users getByResetPasswordToken(String token);

    public void updatePassword(Users user, String newPassword);

    public void updateUserProfile(Integer userId, String fullname, String email, LocalDateTime dob, String address, boolean gender);

    public boolean changePassword(int userId, String oldPassword, String newPassword);

    public boolean changeAvatar(int userId, String avatar);
    public Users getUserByID(int id);
}
