package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Disscuss;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface DisscussService {
    List<Disscuss> findAllParentComment(Integer courseId);
    List<Disscuss> findAllSubComment(Integer courseId, Integer parentId);
    Disscuss findParentById(Integer courseId, Integer parentId);
    void savecomment(Disscuss theDisscuss);
    Disscuss findCmtById(Integer parentId);
    List<Disscuss> findAllCommentById(Integer courseId);
    void changeStatus(Integer commentId, boolean status);
    List<Disscuss> findDisscussDuplicate();
}
