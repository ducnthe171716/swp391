package com.swp391.onlinequiz.service;

import com.swp391.onlinequiz.model.Questionreport;
import com.swp391.onlinequiz.model.Users;

public interface AnswerReportService {
    public void removeAnswerReport(Users u, Questionreport qr);
}
