package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Question;
import com.swp391.onlinequiz.model.Quiz;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

public interface QuestionRepository extends JpaRepository<Question, Integer> {
    @Query(value = "SELECT * FROM question q WHERE q.quizid = :#{#q.quizid} ORDER BY RAND() LIMIT :k", nativeQuery = true)
    List<Question> findRandomQuestionsByQuizId(Quiz q, int k);

    @Query(value = "Select * from question where quizid = ?1",nativeQuery = true)
    List<Question> getQuizBySectionID(Integer id);
    @Modifying
    @Transactional
    @Query(value = "Update Question set questioncontent = ?1 where questionid = ?2",nativeQuery = true)
    void updateQuestion(String title, Integer quizId);


    @Query(value = "Select * from question where quizid = ?1",nativeQuery = true)
    List<Question> getQuestionByQuizID(Integer id);

}
