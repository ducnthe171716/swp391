package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Feedbackdetail;
import com.swp391.onlinequiz.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Repository
public interface FeedbackdetailRepository extends JpaRepository<Feedbackdetail, Integer> {
    Integer countByFbratingAndCourse_Courseid(Integer rating, Integer courseid);
    Feedbackdetail findByUserAndCourse(Users user, Course course);

    @Modifying
    @Transactional
    @Query("Update Feedbackdetail  set fbrating = ?1,feedbackdate = ?2, feedbackcontent = ?3 where user.userid = ?4")
    void updateFeedbackdetailByUser(Integer rating,String feedbackdate,String feedbackcontent,Integer userid);


}
