package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Material;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialRepository extends JpaRepository<Material, Integer> {
}
