package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
