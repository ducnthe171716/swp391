package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Confirmationtoken;
import com.swp391.onlinequiz.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ConfirmationtokenRepository extends JpaRepository<Confirmationtoken, Integer> {
    Confirmationtoken findByConfirmationtoken(String confirmationToken);
    @Modifying
    @Query("DELETE FROM Confirmationtoken c WHERE c.expirationDate <= :expirationDate")
    void deleteByExpirationDateBefore(@Param("expirationDate") Date expirationDate);
    @Modifying
    @Query("DELETE FROM Confirmationtoken c WHERE c.expirationDate <= :expirationDate")
    void deleteByExpirationDate(@Param("expirationDate") Confirmationtoken expirationDate);

    List<Confirmationtoken> findByUser(Users user);


}
