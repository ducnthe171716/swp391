package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.ContentStatusDetail;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Sectiondetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SectionDetailRepository extends JpaRepository<Sectiondetail, Integer> {
   List<Sectiondetail> findByEnroll_Enrollid(Integer enrollid);
   Sectiondetail findByEnroll_EnrollidAndCoursesection_SectionId(Integer enrollid, Integer sectionid);

   @Modifying
   @Transactional
   @Query(value = "Delete from sectiondetail where section_id = ?1",nativeQuery = true)
   void deleteByCourseSectionID(Integer id);
}

