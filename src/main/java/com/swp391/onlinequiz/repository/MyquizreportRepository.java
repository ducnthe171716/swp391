package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Myquizreport;
import com.swp391.onlinequiz.model.Quiz;
import com.swp391.onlinequiz.model.Users;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MyquizreportRepository extends JpaRepository<Myquizreport, Integer> {
    List<Myquizreport> findByUserAndQuiz(Users user, Quiz quiz);
    List<Myquizreport> findByUser(Users user);
    Myquizreport findByQuiz(Quiz quiz);

    void removeByQuizAndUser(Quiz quiz, Users user);

    List<Myquizreport> findByMyquizreportid(Integer myquizreportid);

    
}
