package com.swp391.onlinequiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.swp391.onlinequiz.model.Language;

public interface LanguageRepository extends JpaRepository<Language, Integer>{
    
}
