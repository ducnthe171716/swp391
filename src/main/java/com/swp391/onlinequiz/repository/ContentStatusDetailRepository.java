package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.ContentStatusDetail;
import com.swp391.onlinequiz.model.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ContentStatusDetailRepository extends JpaRepository<ContentStatusDetail, Integer> {
   @Modifying
   @Transactional
   @Query(value = "UPDATE content_status_detail\n" +
           "SET\n" +
           "status = 1\n" +
           "WHERE content_id = ?1 and section_deatail_id = ?2", nativeQuery = true)
   void updateRightAnswer(Integer course_contentid, Integer sectionID);
   List<ContentStatusDetail> findBySectiondetail_SectionDeatailId(Integer sectionID);

   @Modifying
   @Transactional
   @Query(value = "Delete from content_status_detail where content_id = ?1",nativeQuery = true)
   void delete(Integer coursecontentID);

}

