package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Courseprocess;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseprocessRepository extends JpaRepository<Courseprocess, Integer> {
}
