package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Coursecontent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Repository
public interface CoursecontentRepository extends JpaRepository<Coursecontent, Integer> {
    @Query(value = "Select * from Coursecontent where content_id = :contentId",nativeQuery = true)
    public Coursecontent findContentById(Integer contentId);
    @Query(value = "select cc.* from coursecontent cc inner join coursesection cs on cc.coursesectionid = cs.section_id \n" +
            "where cs.courseid = :courseid \n" +
            "order by cs.section_id , cc.contentorder limit 1",nativeQuery = true)
    public Coursecontent findFirstContentOfCOurse(Integer courseid);

    @Query(value = "Select * from coursecontent where coursesectionid = ?1",nativeQuery = true)
    public List<Coursecontent> getCourseContentsBySectionID(Integer id);
    @Modifying
    @Transactional
    @Query(value = "Update Coursecontent set correct = 1 where answer_id = ?1",nativeQuery = true)
    void updateRightAnswer(Integer answerID);
}
