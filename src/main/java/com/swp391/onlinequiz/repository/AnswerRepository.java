package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Answer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    List<Answer> getByAnswerId(Integer answerId);
    @Query(value = "Select * from answer where questionId = ?1", nativeQuery = true)
    List<Answer> getAnswerByQuestionID(Integer quesId);
    @Modifying
    @Transactional
    @Query(value = "Update Answer set content = ?1 where answer_id = ?2",nativeQuery = true)
    void updateAnswer(String title, Integer idAnswer);

    @Modifying
    @Transactional
    @Query(value = "Update Answer set correct = 1 where answer_id = ?1",nativeQuery = true)
    void updateRightAnswer(Integer answerID);

}
