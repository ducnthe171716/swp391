package com.swp391.onlinequiz.repository.impl;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.repository.CourseRepositoryCustom;
import com.swp391.onlinequiz.ulti.Common;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CourseRepositoryImpl implements CourseRepositoryCustom {
    @Autowired
    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager entityManager;

    @Override
    public List<Course> findCourseByOwnerID(Integer userID, String searchValue, Integer reviews, Integer statusID, String orderBy, Integer categoryID) {
        String sql = "Select * from course c join courseowner ce on c.courseownerid = ce.ownerid where userid = :userID";
        Map<String,Object> maps = new HashMap<>();
        maps.put("userID",userID);
        if(searchValue != null && !searchValue.trim().isEmpty()){
            sql += " and coursename like :searchValue";
            maps.put("searchValue","%" + searchValue + "%");
        }
        if(reviews != null && reviews != 0){
            sql += " and courserating = :rating";
            maps.put("rating",reviews);
        }
        if(statusID != null && statusID != 0){
            sql += " and status_id = :statusID";
            maps.put("statusID",statusID);
        }
        if(categoryID != null && categoryID != 0){
            sql += " and categoryid = :categoryid";
            maps.put("categoryid",categoryID);
        }
        if(orderBy != null && !orderBy.equals("none")){
            sql += " order by " + orderBy;
        }
        Query query = entityManager.createNativeQuery(sql,Course.class);
        Common.setParams(query,maps);
        List<Course> list = query.getResultList();
        return list;
    }
}
