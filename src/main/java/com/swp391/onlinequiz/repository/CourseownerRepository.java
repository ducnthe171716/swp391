package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Courseowner;
import com.swp391.onlinequiz.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface CourseownerRepository extends JpaRepository<Courseowner, Integer> {
    // chart
    @Query(value = "SELECT\n" +
            "    CONCAT(YEAR(NOW()), '-', LPAD(months.month, 2, '0')) AS month,\n" +
            "    IFNULL(SUM(enroll.checkout), 0) AS total_checkout\n" +
            "FROM (\n" +
            "    SELECT 1 AS month\n" +
            "    UNION SELECT 2\n" +
            "    UNION SELECT 3\n" +
            "    UNION SELECT 4\n" +
            "    UNION SELECT 5\n" +
            "    UNION SELECT 6\n" +
            "    UNION SELECT 7\n" +
            "    UNION SELECT 8\n" +
            "    UNION SELECT 9\n" +
            "    UNION SELECT 10\n" +
            "    UNION SELECT 11\n" +
            "    UNION SELECT 12\n" +
            ") AS months\n" +
            "LEFT JOIN (\n" +
            "    SELECT MONTH(enrolldate) AS month, SUM(checkout) AS checkout\n" +
            "    FROM test2jpa.enroll AS e\n" +
            "    INNER JOIN test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "    INNER JOIN test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "    WHERE co.ownerid = :teacher_id AND YEAR(e.enrolldate) = YEAR(NOW())\n" +
            "    GROUP BY MONTH(enrolldate)\n" +
            ") AS enroll ON months.month = enroll.month\n" +
            "GROUP BY months.month;\n", nativeQuery = true)
    List<Object[]> findUsersRegisteredSince(@Param("teacher_id") int teacherId);
    @Query(value = "SELECT DATE_FORMAT(e.enrolldate, '%Y-%m') AS enrol_month,\n" +
            "       SUM(e.checkout) AS total_amount\n" +
            "FROM test2jpa.enroll AS e\n" +
            "INNER JOIN test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "INNER JOIN test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "WHERE co.ownerid = :teacher_id and enrolldate >= DATE_SUB(CURDATE(), INTERVAL :sqlTimeInterval DAY) \n" +
            "GROUP BY enrol_month", nativeQuery = true)
    List<Object[]> findUsersRegisterByTime(@Param("teacher_id") int teacherId, @Param("sqlTimeInterval") String sqlTimeInterval);
    @Query(value = "SELECT DATE_FORMAT(e.enrolldate, '%Y-%m-%d') AS enrol_date,\n" +
            "       SUM(e.checkout) AS total_amount\n" +
            "FROM test2jpa.enroll AS e\n" +
            "INNER JOIN test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "INNER JOIN test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "WHERE co.ownerid = :teacher_id AND enrolldate >= DATE_SUB(CURDATE(), INTERVAL :sqlTimeInterval DAY) \n" +
            "GROUP BY enrol_date\n" +
            "ORDER BY enrol_date asc\n",nativeQuery = true)
    List<Object[]> findUsersRegisterByDay(@Param("teacher_id") int teacherId, @Param("sqlTimeInterval") String sqlTimeInterval);
    //box
    @Query(value = "SELECT co.userid AS teacher_id, u.fullname AS teacher_name, " +
            "COUNT(DISTINCT c.courseid) AS total_courses_owned, " +
            "SUM(c.courseprice) AS total_earnings, " +
            "COUNT( e.userid) AS total_students, " +
            "SUM(e.checkout) AS total_revenue " +
            "FROM test2jpa.courseowner co " +
            "LEFT JOIN test2jpa.course c ON co.ownerid = c.courseownerid " +
            "LEFT JOIN test2jpa.enroll e ON c.courseid = e.courseid " +
            "LEFT JOIN test2jpa.users u ON co.userid = u.userid " +
            "WHERE co.userid = :teacher_id " +
            "GROUP BY teacher_id, teacher_name",
            nativeQuery = true)

    List<Object[]> findCourseOwnerStatistics(@Param("teacher_id") int teacherId);

    //list subject
    @Query(value = "SELECT\n" +
            "    c.courseid AS course_id,\n" +
            "    c.coursename AS course_name,\n" +
            "    COUNT(e.enrollid) AS enrollments,\n" +
            "    SUM(e.checkout) AS total_revenue\n" +
            "FROM test2jpa.course c\n" +
            "LEFT JOIN test2jpa.enroll e ON c.courseid = e.courseid\n" +
            "where c.courseid = :course_id\n" +
            "GROUP BY course_id, course_name;",nativeQuery = true)
    List<Object[]> findTotalRevenumBy1Course(@Param("course_id") int course_id);

    //list subject
    @Query(value = "SELECT\n" +
            "    c.courseid AS course_id,\n" +
            "    c.coursename AS course_name,\n" +
            "    COUNT(e.enrollid) AS enrollments,\n" +
            "    SUM(e.checkout) AS total_revenue\n" +
            "FROM test2jpa.course c\n" +
            "LEFT JOIN test2jpa.enroll e ON c.courseid = e.courseid\n" +
            "WHERE c.courseid = :course_id\n" +
            "    AND MONTH(e.enrolldate) = MONTH(CURRENT_DATE())\n" +
            "GROUP BY course_id, course_name;",nativeQuery = true)
    List<Object[]> findTotalRevenumByMonth(@Param("course_id") int course_id);
    //box
    @Query(value = "SELECT COUNT(DISTINCT fd.feedbackid) AS total_feedback\n" +
            "FROM test2jpa.users u\n" +
            "LEFT JOIN test2jpa.courseowner co ON u.userid = co.userid\n" +
            "LEFT JOIN test2jpa.course c ON co.ownerid = c.courseownerid\n" +
            "LEFT JOIN test2jpa.feedbackdetail fd ON c.courseid = fd.courseid\n" +
            "WHERE u.userid = :teacher_id\n", nativeQuery = true)
    int findTotalFeedback(@Param("teacher_id") int teacher_id);

    // revenue report
    @Query(value = "SELECT \n" +
            "    u.fullname AS user_name,\n" +
            "    c.coursename As course_name,\n" +
            "    e.enrolldate AS enrollment_time,\n" +
            "    e.checkout AS checkout_amount,\n" +
            "    c.courseprice AS course_price,\n" +
            "\t(e.checkout - (e.checkout * 0.1)) AS income\n" +
            "FROM test2jpa.enroll e\n" +
            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
            "WHERE c.courseownerid = :courseownerid", nativeQuery = true)
    List<Object[]> listCheckOut(@Param("courseownerid") int courseownerid);

    // revenue report
    @Query(value = "SELECT\n" +
            "    u.fullname AS user_name,\n" +
            "    c.coursename AS course_name,\n" +
            "    e.enrolldate AS enrollment_time,\n" +
            "    e.checkout AS checkout_amount,\n" +
            "    c.courseprice AS course_price,\n" +
            "    (e.checkout - (e.checkout * 0.1)) AS income\n" +
            "FROM test2jpa.enroll e\n" +
            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
            "WHERE c.courseownerid = :courseownerid and Month(e.enrolldate) = :month\n",nativeQuery = true)
    List<Object[]> listCheckOutByMonth(@Param("courseownerid") int courseownerid, @Param("month") int month);
    // revenue report
    @Query(value = "SELECT\n" +
            "    u.fullname AS user_name,\n" +
            "    c.coursename AS course_name,\n" +
            "    e.enrolldate AS enrollment_time,\n" +
            "    e.checkout AS checkout_amount,\n" +
            "    c.courseprice AS course_price,\n" +
            "    (e.checkout - (e.checkout * 0.1)) AS income\n" +
            "FROM test2jpa.enroll e\n" +
            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
            "WHERE c.courseownerid = :courseownerid and Year(e.enrolldate) = :year\n",nativeQuery = true)
    List<Object[]> listCheckOutByYear(@Param("courseownerid") int courseownerid, @Param("year") int year);

    // revenue report

    @Query(value = "SELECT\n" +
            "    u.fullname AS user_name,\n" +
            "    c.coursename AS course_name,\n" +
            "    e.enrolldate AS enrollment_time,\n" +
            "    e.checkout AS checkout_amount,\n" +
            "    c.courseprice AS course_price,\n" +
            "    (e.checkout - (e.checkout * 0.1)) AS income\n" +
            "FROM test2jpa.enroll e\n" +
            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
            "WHERE c.courseownerid = :courseownerid and c.coursename LIKE %:coursename%\n",nativeQuery = true)
    List<Object[]> listCheckOutByName(@Param("courseownerid") int courseownerid, @Param("coursename") String coursename);
    //Notification
    @Query(value = "SELECT\n" +
            "    u.fullname AS user_name,\n" +
            "    c.coursename AS course_name,\n" +
            "    e.enrolldate AS enrollment_time,\n" +
            "    e.checkout AS checkout_amount,\n" +
            "    c.courseprice AS course_price,\n" +
            "    (e.checkout - (e.checkout * 0.1)) AS income\n" +
            "FROM test2jpa.enroll e\n" +
            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
            "WHERE c.courseownerid = :courseownerid\n" +
            "ORDER BY e.enrolldate DESC \n" +
            "LIMIT 5; \n" +
            "\n", nativeQuery = true)
    List<Object[]> listtop5CheckOut(@Param("courseownerid") int courseownerid);
//    @Query(value = "SELECT\n" +
//            "    u.fullname AS user_name,\n" +
//            "    c.coursename AS course_name,\n" +
//            "    e.enrolldate AS enrollment_time,\n" +
//            "    e.checkout AS checkout_amount,\n" +
//            "    c.courseprice AS course_price,\n" +
//            "    (e.checkout - (e.checkout * 0.1)) AS income\n" +
//            "FROM test2jpa.enroll e\n" +
//            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
//            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
//            "WHERE c.courseid = :courseid;", nativeQuery = true)
//    List<Object[]> listCheckOutBy1Course(@Param("courseid") int courseid);


    @Query(value = "SELECT \n" +
            "    u.fullname AS user_name,\n" +
            "    c.coursename As course_name,\n" +
            "    e.enrolldate AS enrollment_time,\n" +
            "    e.checkout AS checkout_amount,\n" +
            "    c.courseprice AS course_price,\n" +
            "\t(e.checkout - (e.checkout * 0.1)) AS income\n" +
            "FROM test2jpa.enroll e\n" +
            "LEFT JOIN test2jpa.users u ON e.userid = u.userid\n" +
            "LEFT JOIN test2jpa.course c ON e.courseid = c.courseid\n" +
            "WHERE c.courseid = :courseownerid\n", nativeQuery = true)
    List<Object[]> listCheckOutByCourse(@Param("courseownerid") int courseownerid);

    //box change date
    @Query(value = "SELECT\n" +
            "    co.userid AS teacher_id,\n" +
            "    u.fullname AS teacher_name,\n" +
            "    COUNT(DISTINCT c.courseid) AS total_courses_owned,\n" +
            "    SUM(c.courseprice) AS total_earnings,\n" +
            "    COUNT(e.userid) AS total_students,\n" +
            "    SUM(e.checkout) AS total_revenue\n" +
            "FROM test2jpa.courseowner co\n" +
            "LEFT JOIN test2jpa.course c ON co.ownerid = c.courseownerid\n" +
            "LEFT JOIN test2jpa.enroll e ON c.courseid = e.courseid\n" +
            "LEFT JOIN test2jpa.users u ON co.userid = u.userid\n" +
            "WHERE co.userid = :teacher_id\n" +
            "    AND e.enrolldate >= DATE_SUB(CURDATE(), INTERVAL :sqlTimeInterval DAY )\n" +
            "GROUP BY teacher_id, teacher_name", nativeQuery = true)
    List<Object[]> findCourseOwnerStatisticsByTime(@Param("teacher_id") int teacherId, @Param("sqlTimeInterval") String sqlTimeInterval);
    @Query(value = "SELECT u.username,e.enrolldate, c.coursename,e.checkout as Price_paid,(e.checkout - (e.checkout * 0.1)) as Income\n" +
            "FROM test2jpa.enroll AS e\n" +
            "INNER JOIN test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "INNER JOIN test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "INNER JOIN test2jpa.users AS u ON e.userid = u.userid\n" +
            "WHERE co.ownerid = :teacher_id\n" +
            "  AND DATE_FORMAT(e.enrolldate, '%Y-%m') = :timeParam",nativeQuery = true)
    List<Object[]> showEnrollByMonth(@Param("teacher_id") int teacherId,  @Param("timeParam") String timeParam);

    @Query(value = "SELECT DATE_FORMAT(e.enrolldate, '%Y-%m') AS month,\n" +
            "       c.coursename,\n" +
            "       COUNT(DISTINCT e.userid) AS enroll_count,\n" +
            "       FORMAT(SUM(e.checkout), 2) AS total_revenue\n" +
            "FROM test2jpa.enroll AS e\n" +
            "INNER JOIN test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "INNER JOIN test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "WHERE co.ownerid = :teacher_id and DATE_FORMAT(e.enrolldate, '%Y-%m') = :timeParam\n" +
            "GROUP BY month, c.coursename",nativeQuery = true)
   List<Object[]> competeCoure(@Param("teacher_id") int teacherId, @Param("timeParam") String timeParam);

    @Query(value = "SELECT DATE_FORMAT(e.enrolldate, '%Y-%m') AS month,\n" +
            "       c.coursename,\n" +
            "       COUNT(DISTINCT e.userid) AS enroll_count,\n" +
            "       FORMAT(SUM(e.checkout), 2) AS total_revenue\n" +
            "FROM test2jpa.enroll AS e\n" +
            "INNER JOIN test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "INNER JOIN test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "WHERE co.ownerid = :teacher_id AND DATE_FORMAT(e.enrolldate, '%Y-%m') = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL :timeParam MONTH), '%Y-%m')\n" +
            "GROUP BY month, c.coursename;\n",nativeQuery = true)
    List<Object[]> competeEachMonth(@Param("teacher_id") int teacherId, @Param("timeParam") String timeParam);


    @Query(value = "SELECT\n" +
            "    cat.categoryname,\n" +
            "    COALESCE(COUNT(e.courseid), 0) AS total_sales\n" +
            "FROM\n" +
            "    test2jpa.category cat\n" +
            "LEFT JOIN\n" +
            "    test2jpa.course c ON cat.categoryid = c.categoryid\n" +
            "LEFT JOIN\n" +
            "    test2jpa.enroll e ON c.courseid = e.courseid\n" +
            "LEFT JOIN\n" +
            "    test2jpa.courseowner co ON c.courseownerid = co.ownerid\n" +
            "WHERE\n" +
            "    co.ownerid = :teacher_id AND DATE_FORMAT(e.enrolldate, '%Y-%m') = :timeParam\n" +
            "GROUP BY\n" +
            "    cat.categoryid, cat.categoryname\n" +
            "ORDER BY\n" +
            "    total_sales DESC\n",nativeQuery = true)
    List<Object[]> competeCategory(@Param("teacher_id") int teacherId, @Param("timeParam") String timeParam);


    @Query(value = "SELECT\n" +
            "    FORMAT(SUM(e.checkout), 2) AS total_revenue\n" +
            "FROM\n" +
            "    test2jpa.enroll AS e\n" +
            "INNER JOIN\n" +
            "    test2jpa.course AS c ON e.courseid = c.courseid\n" +
            "INNER JOIN\n" +
            "    test2jpa.courseowner AS co ON c.courseownerid = co.ownerid\n" +
            "WHERE\n" +
            "    co.ownerid = :teacher_id AND DATE_FORMAT(e.enrolldate, '%Y-%m') = :timeParam\n" +
            "ORDER BY\n" +
            "    e.enrolldate DESC\n" +
            "LIMIT 1;\n" , nativeQuery = true)
    List<Object[]> showTotal(@Param("teacher_id") int teacherId, @Param("timeParam") String timeParam);

    List<Courseowner> findByUser(Users user);
}
