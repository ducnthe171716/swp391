package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
    @Query("select s from Schedule s where s.user.userid = :id")
    List<Schedule> findAllByUserID(int id);
}
