package com.swp391.onlinequiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.swp391.onlinequiz.model.Preneeded;

public interface PreneedRepository extends JpaRepository<Preneeded, Integer>{
    
}
