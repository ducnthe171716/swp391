package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Course;

import java.util.List;

public interface CourseRepositoryCustom {
    public List<Course> findCourseByOwnerID(Integer userID,String searchValue,Integer reviews,Integer statusID,String orderBy,Integer categoryID);
}
