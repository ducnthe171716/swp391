package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Discount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscountRepository extends JpaRepository<Discount, Integer> {
}
