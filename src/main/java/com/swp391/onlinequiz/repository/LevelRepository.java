package com.swp391.onlinequiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.swp391.onlinequiz.model.Level;

public interface LevelRepository extends JpaRepository<Level, Integer>{
    
}
