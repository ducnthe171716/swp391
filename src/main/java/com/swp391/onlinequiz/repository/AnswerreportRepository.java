package com.swp391.onlinequiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.swp391.onlinequiz.model.Answer;
import com.swp391.onlinequiz.model.Answerreport;
import com.swp391.onlinequiz.model.Questionreport;
import com.swp391.onlinequiz.model.Users;

import java.util.List;
import java.util.Optional;



public interface AnswerreportRepository extends JpaRepository<Answerreport, Integer>{

    Optional<Answerreport> findById(Integer id);

    void deleteByUserAndQuestionreport(Users user, Questionreport questionreport);

    List<Answerreport> findByQuestionreport(Questionreport questionreport);
    
}
