package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Coursecontent;
import com.swp391.onlinequiz.model.Quiz;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface QuizRepository extends JpaRepository<Quiz, Integer> {
    List<Quiz> findByQuizid(Integer quizid);

    @Query(value = "Select * from quiz where coursesectionid = ?1",nativeQuery = true)
    List<Quiz> getQuizzesByCourseID(Integer courseID);

    @Query(value = "Select * from quiz where coursesectionid = ?1",nativeQuery = true)
    List<Quiz> getQuizBySectionID(Integer id);
    @Modifying
    @Transactional
    @Query("Update Quiz  set quizcontent = ?1,time = ?2, randquestion = ?3,passpoint= ?4, quizdob= ?5 where quizid = ?6")
    void updateFeedbackdetailByUser(String title, Integer time, Integer randquestion, Double passpoint, LocalDateTime quizdob, Integer quizid);
}
