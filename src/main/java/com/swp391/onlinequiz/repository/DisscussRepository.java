package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Disscuss;
import com.swp391.onlinequiz.model.Users;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface DisscussRepository extends JpaRepository<Disscuss, Integer> {
    @Query(value = "Select * from Disscuss where isnull(parent_id) and  courseid= :courseId", nativeQuery = true)
    public List<Disscuss> AllParentDisscus (Integer courseId);
    @Query(value = "Select * from Disscuss where parent_id = :parentId and courseid= :courseId", nativeQuery = true)
    public List<Disscuss> AllSubDisscuss(Integer courseId, Integer parentId);
    @Query(value = "Select * from Disscuss where commentid = :parentId and courseid= :courseId", nativeQuery = true)
    public Disscuss ParentDisscuss( Integer courseId, Integer parentId);
    @Query(value = "Select * from Disscuss where commentid = :parentId", nativeQuery = true)
    public Disscuss findCmtByID(Integer parentId);
    @Query(value = "Select * from Disscuss where courseId = :courseId", nativeQuery = true)
    List<Disscuss> findAllCommentById(Integer courseId);

    @Modifying
    @Query("Update Disscuss d Set d.status = :status where d.commentid = :commentId")
    public void updateStatus(Integer commentId, boolean status);

    @Query(value = "SELECT * FROM disscuss\n" +
            "WHERE content IN (\n" +
            "    SELECT content\n" +
            "    FROM disscuss\n" +
            "    GROUP BY content\n" +
            "    HAVING COUNT(*) > 1\n" +
            ");", nativeQuery = true)
    List<Disscuss> findDisscussDuplicate();

}
