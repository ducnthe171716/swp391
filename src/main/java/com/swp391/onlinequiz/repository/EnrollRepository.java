package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.time.LocalDateTime;


@Repository
public interface EnrollRepository extends JpaRepository<Enroll, Integer> {
    @Query("Select e from Enroll e where e.user.userid = :userID")
    List<Enroll> findByUserID(int userID);

    @Query("Select e from Enroll e where e.course.coursename like %:name%")
    List<Enroll> findByName(String name);

    @Query("Select e from Enroll e where e.user.userid = :userID and  e.course.category.categoryid = :category")
    List<Enroll> findByCategoryID(int userID, int category);

    @Query("Select e from Enroll e where e.user.userid = :userID and e.course.courseowner.ownerid = :courseOwnerID")
    List<Enroll> findByCourseOwnerID(int userID, int courseOwnerID);


    @Query("Select e from Enroll e where e.course.courseid =:courseID")
    List<Enroll> findByCourseID(int courseID);

    List<Enroll> findByEnrollid(Integer enrollid);

    List<Enroll> findByUserAndCourse(Users user, Course course);

    @Query("Select e from Enroll e where e.user.userid = :userID and e.processstatus = true")
    List<Enroll> findFinishCourse(int userID);

    @Query("Select e from Enroll e where Month(enrolldate) = :m AND year(enrolldate) = :y")
    List<Enroll> findByEnrolldate(int m, int y);

    @Query("SELECT DISTINCT YEAR(e.enrolldate) FROM Enroll e")
    List<Integer> findDistinctEnrollyears();
    Enroll findByUser_UseridAndCourse_Courseid(Integer userid, Integer courseID);
}
