package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AdminRepository  extends CrudRepository<Users, Integer> {

    @Query("Select u from Users u where u.role = ?1 or u.role = ?2")
    List<Users> findByRole(String user, String course);
    @Query("Select u from Users u where u.role = :user")
    List<Users> findBy1Role(String user);
    Users findByUserid(int userid);


}
