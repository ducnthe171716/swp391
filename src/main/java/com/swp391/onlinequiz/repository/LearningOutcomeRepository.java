package com.swp391.onlinequiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.swp391.onlinequiz.model.Learningoutcome;

/**
 * LearningOutcomeRepository
 */
public interface LearningOutcomeRepository extends JpaRepository<Learningoutcome, Integer>{

    
}