package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Users;
import org.apache.catalina.User;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {
    @Query("Select u from Users u")
    List<Users> getAllUsers();

    List<Users> findByUsernameAndPassword(String identify, String password);

    @Query("Select u from Users u where (u.username = ?1 or u.email = ?1)")
    List<Users> findAcount(String identify);

     Users findByEmailIgnoreCase(String emailId);

    Boolean existsByEmail(String email);

    boolean existsByUsername(String username);
    @Query("Select u from Users u where u.email = ?1")
    Users findByEmail(String email);
    Users findByResetPasswordToken(String token);

    Optional<Users> findByUsername(String username);

    @Query("Select u from Users u where u.userid = ?1")
    Users findById(int ID);

    @Query("Select u from Users u where u.username = ?1")
    Users findByUserName(String username);

    @Query(value = "select * from Users where email like %?1% and role=?2", nativeQuery = true)
    List<Users> findAllByEmailAndRoleId(String email, String id);

    @Query(value = "select * from Users where email like %?1%", nativeQuery = true)
    List<Users> findAllByEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE Users u SET u.createdDate = :createdDate WHERE u.email = :email")
    void updateCreatedDateByEmail(@Param("createdDate") Date createdDate, @Param("email") String email);

    List<Users> findByIsenabled(Boolean isenabled);
}
