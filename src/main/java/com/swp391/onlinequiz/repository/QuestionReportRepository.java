package com.swp391.onlinequiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.swp391.onlinequiz.model.Myquizreport;
import com.swp391.onlinequiz.model.Question;
import com.swp391.onlinequiz.model.Questionreport;
import com.swp391.onlinequiz.model.Users;

import java.util.List;
import java.util.Optional;



public interface QuestionReportRepository extends JpaRepository<Questionreport, Integer>{

    Optional<Questionreport> findById(Integer id);

    List<Questionreport> findByMyquizreport(Myquizreport myquizreport);

    @Query(value = "select * from questionreport where questionid = ?1",nativeQuery = true)
    List<Questionreport> findByQuestion(Integer questionID);

}
