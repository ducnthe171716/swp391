package com.swp391.onlinequiz.repository;



import org.springframework.data.jpa.repository.JpaRepository;


import com.swp391.onlinequiz.model.ProgressStatus;
import java.util.List;



public interface ProgressStatusRepository extends JpaRepository<ProgressStatus, Integer>{
    List<ProgressStatus> findByStatusId(Integer statusId);
}
