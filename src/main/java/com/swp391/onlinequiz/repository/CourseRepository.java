package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Courseowner;
import com.swp391.onlinequiz.model.ProgressStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CourseRepository extends JpaRepository<Course, Integer>,CourseRepositoryCustom{
    @Query(value = "SELECT * FROM course where categoryid = :categoryID AND status_id = 3",nativeQuery = true)
    public List<Course> findCourseByCategory(Integer categoryID);

    @Query(value = "SELECT * FROM course where coursename like %:value% AND status_id = 3",nativeQuery = true)
    public List<Course> findCourseBySearchValue(String value);

    List<Course> findByCourseowner(Courseowner courseowner);

    List<Course> findByStatus(ProgressStatus status);

    List<Course> findByCourseownerAndStatus(Courseowner courseowner, ProgressStatus status);

}
