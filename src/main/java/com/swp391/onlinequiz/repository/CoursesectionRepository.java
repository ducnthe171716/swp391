package com.swp391.onlinequiz.repository;

import com.swp391.onlinequiz.model.Coursesection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CoursesectionRepository extends JpaRepository<Coursesection, Integer> {
    @Query(value = "Select * from coursesection where courseid = ?1",nativeQuery = true)
    public List<Coursesection> getCourseSectionsByCourseID(Integer courseID);
    public Coursesection findBySectionId(Integer id);

}
