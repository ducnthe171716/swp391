package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Enroll;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.CourseRepository;
import com.swp391.onlinequiz.repository.CourseownerRepository;
import com.swp391.onlinequiz.repository.EnrollRepository;
import com.swp391.onlinequiz.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EnrollServiceImpl implements EnrollService {
    private final EnrollRepository enrollRepository;
    private final CourseRepository courseRepository;
    @Autowired
    private CourseownerRepository courseownerRepository;
    public EnrollServiceImpl(EnrollRepository enrollRepository, CourseRepository courseRepository) {
        this.enrollRepository = enrollRepository;
        this.courseRepository = courseRepository;
    }

    public List<Course> showMyCoursesByUserID(int userID) {
        // Truy vấn danh sách enroll của người dùng dựa trên userID
        List<Enroll> enrollList = enrollRepository.findByUserID(userID);

        // Tạo danh sách để lưu thông tin về các khóa học đã đăng ký
        List<Course> myCourses = new ArrayList<>();

        // Duyệt qua danh sách enroll và lấy thông tin về các khóa học tương ứng
        for (Enroll enroll : enrollList) {
            Course course = enroll.getCourse();
            if (course != null) {
                myCourses.add(course);
            }
        }
        return myCourses;
    }

    public List<Course> searchCoursesByName(String searchTerm){
        List<Enroll> foundList = enrollRepository.findByName(searchTerm);

        List<Course> myCourses = new ArrayList<>();

        for (Enroll enroll : foundList) {
            Course course = enroll.getCourse();
            if (course != null) {
                myCourses.add(course);
            }
        }
        return myCourses;
    }

    public List<Course> listCoursesByCategoryID(int userID, int category) {
        List<Enroll> foundList = enrollRepository.findByCategoryID(userID, category);
        List<Course> myCourses = new ArrayList<>();

        for (Enroll enroll : foundList) {
            Course course = enroll.getCourse();
            if (course != null) {
                myCourses.add(course);
            }
        }

        return myCourses;
    }

    public List<Course> listCoursesByCourseOwnerID(int userID, int courseOwnerID){
        List<Enroll> foundList = enrollRepository.findByCourseOwnerID(userID, courseOwnerID);
        List<Course> myCourses = new ArrayList<>();

        for (Enroll enroll : foundList) {
            Course course = enroll.getCourse();
            if (course != null) {
                myCourses.add(course);
            }
        }
        return myCourses;
    }


    public List<Users> listUsersByCourseID(int courseID){
        List<Enroll> courseList = enrollRepository.findByCourseID(courseID);
        List<Users> userList = new ArrayList<>();

        for (Enroll enroll : courseList) {
            Users user = enroll.getUser();
            if (user != null) {
                userList.add(user);
            }
        }
        return userList;
    }

    @Override

    public Enroll findByUserAndCourse(Integer userid, Integer courseid) {
        return enrollRepository.findByUser_UseridAndCourse_Courseid(userid,courseid);
    }

    public Page<Enroll> listRevenueByCourseOwnerID(int courseOwnerID, int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);

        return enrollRepository.findAll(pageable);
    }

}
