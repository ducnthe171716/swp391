package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.AdminRepository;
import com.swp391.onlinequiz.repository.UsersRepository;
import com.swp391.onlinequiz.service.AdminService;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import java.util.List;
import java.util.Random;

@Service
public class AdminServiceImpls implements AdminService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private UsersRepository userRepository;
    @Autowired
    private JavaMailSenderImpl mailSender;

    @Override
    public List<Users> listAllRegister() {
        return (List<Users>) adminRepository.findByRole("ROLE_MENTEE", "ROLE_COURSEOWNER");
    }

    @Override
    public void CreateAccountByAdmin(Users user) {
        user.setRegisterdob(LocalDate.now());
        user.setIsenabled(true);
        String password = generatePassword();
        user.setPassword(password);
        user.setDob(user.getDob());
        userRepository.save(user);

        MimeMessage message = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(user.getEmail());
            helper.setSubject("Account Approved!!");

            String htmlContent = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "  <body>\n" +
                    "    <div style=\"font-family: Helvetica, Arial, sans-serif; min-width: 1000px; overflow: auto; line-height: 2\">\n" +
                    "      <div style=\"margin: 50px auto; width: 70%; padding: 20px 0\">\n" +
                    "        <div style=\"border-bottom: 1px solid #eee\">\n" +
                    "          <a href=\"http://localhost:8080/home\" style=\"font-size: 1.4em; color: #2a1777; text-decoration: none; font-weight: 600\">Educham</a>\n" +
                    "        </div>\n" +
                    "        <p style=\"font-size: 1.1em\">Hi,</p>\n" +
                    "        <p>Hi there. Use the following password to log in to the system.</p>\n" +
                    "        <h2 style=\"background: #2a1777; margin: 0 auto; width: max-content; padding: 10px; color: #fff; border-radius: 4px;\">" +
                    user.getPassword() +
                    "</h2>\n" +
                    "        <p style=\"font-size: 0.9em;\">Regards,<br />Educhamp</p>\n" +
                    "        <hr style=\"border: none; border-top: 1px solid #eee\" />\n" +
                    "        <div style=\"float: right; padding: 8px 0; color: #aaa; font-size: 0.8em; line-height: 1; font-weight: 300\">\n" +
                    "          <p>Educhamp Inc</p>\n" +
                    "          <p>FPT university</p>\n" +
                    "          <p>Hoa Lac</p>\n" +
                    "        </div>\n" +
                    "      </div>\n" +
                    "    </div>\n" +
                    "  </body>\n" +
                    "</html>";

            helper.setText(htmlContent, true);
            mailSender.send(message);
        } catch (Exception e) {
            // Xử lý ngoại lệ hoặc in ra thông báo lỗi nếu cần
        }
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userRepository.save(user);
    }

    @Override
    public String generatePassword() {
        String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String specialCharacters = "!@#$";
        String numbers = "1234567890";
        String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
        Random random = new Random();
        String password = "";
        password += lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
        password += capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
        password += specialCharacters.charAt(random.nextInt(specialCharacters.length()));
        password += numbers.charAt(random.nextInt(numbers.length()));
        for (int i = 4; i < 10; i++) {
            password += combinedChars.charAt(random.nextInt(combinedChars.length()));
        }
        return password;
    }


}


