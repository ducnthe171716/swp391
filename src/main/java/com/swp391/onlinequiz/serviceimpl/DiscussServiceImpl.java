package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Disscuss;
import com.swp391.onlinequiz.model.Myquizreport;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.DisscussRepository;
import com.swp391.onlinequiz.repository.QuestionReportRepository;
import com.swp391.onlinequiz.service.DisscussService;
import com.swp391.onlinequiz.service.QuestionReportService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DiscussServiceImpl implements DisscussService{

    @Autowired
    private DisscussRepository disscussRepository ;


    @Override
    public List<Disscuss> findAllParentComment(Integer courseId) {
        return disscussRepository.AllParentDisscus(courseId);
    }

    @Override
    public List<Disscuss> findAllSubComment(Integer courseId, Integer parentId) {
        return disscussRepository.AllSubDisscuss(courseId,parentId);
    }

    @Override
    public Disscuss findParentById(Integer courseId, Integer parentId) {
        return disscussRepository.ParentDisscuss(courseId,parentId);
    }

    @Override
    public void savecomment(Disscuss theDisscuss) {
        disscussRepository.save(theDisscuss);
    }


    @Override
    public Disscuss findCmtById(Integer parentId) {
        return disscussRepository.findCmtByID(parentId);
    }

    @Override
    public List<Disscuss> findAllCommentById(Integer courseId){
        return disscussRepository.findAllCommentById(courseId);
    }

    @Override
    public void changeStatus(Integer commentId, boolean status){
        disscussRepository.updateStatus(commentId, status);
    }

    @Override
    public List<Disscuss> findDisscussDuplicate(){
        List<Disscuss> duplicate = disscussRepository.findDisscussDuplicate();
        return duplicate;
    }
}
