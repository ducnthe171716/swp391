package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Category;
import com.swp391.onlinequiz.repository.CategoryRepository;
import com.swp391.onlinequiz.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepository.findAll();
    }

    @Override
    public Category getCategoryByID(int id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public Category saveCategory(String categoryname) {
        Category category = new Category();
        category.setCategoryname(categoryname);
        return categoryRepository.save(category);
    }
}
