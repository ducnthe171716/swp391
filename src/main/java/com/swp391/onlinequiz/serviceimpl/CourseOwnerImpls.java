package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Courseowner;
import com.swp391.onlinequiz.repository.CourseownerRepository;
import com.swp391.onlinequiz.service.CourseownerService;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class CourseOwnerImpls implements CourseownerService {
    private final CourseownerRepository courseownerRepository;

    public CourseOwnerImpls(CourseownerRepository courseownerRepository) {
        this.courseownerRepository = courseownerRepository;
    }

    @Override
    public Courseowner getCourseownerById(int id) {
        return courseownerRepository.findById(id).get();
    }


    public List<Courseowner> getAll(){return courseownerRepository.findAll();}
}
