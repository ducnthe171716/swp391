package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Coursesection;
import com.swp391.onlinequiz.repository.CoursesectionRepository;
import com.swp391.onlinequiz.service.CourseService;
import com.swp391.onlinequiz.service.CoursesectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseSectionServiceImpl implements CoursesectionService {
    @Autowired
    public CoursesectionRepository coursesectionRepository;
    @Override
    public Coursesection findById(Integer id) {
        return coursesectionRepository.findBySectionId(id);
    }
}
