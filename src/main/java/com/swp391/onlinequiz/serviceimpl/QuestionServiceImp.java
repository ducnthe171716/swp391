package com.swp391.onlinequiz.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.swp391.onlinequiz.model.Question;
import com.swp391.onlinequiz.repository.QuestionRepository;
import com.swp391.onlinequiz.service.QuestionService;

import java.util.Collections;
import java.util.List;


@Service
@Transactional
public class QuestionServiceImp implements QuestionService
{

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public Question getQuestionById(Integer id) {
        return (Question)questionRepository.findById(id).get();
    }

    @Override
    public List<Question> getQuestionBySectionId(Integer sectionId) {
        return questionRepository.getQuizBySectionID(sectionId);
    }

    @Override
    public void save(Question question) {
        questionRepository.save(question);
    }

    @Override
    public void updateQuestion(String title, Integer quizid) {
        questionRepository.updateQuestion(title, quizid);
    }

    @Override
    public void deleteQuestion(Integer id) {
        questionRepository.deleteById(id);
    }

    @Override
    public Page<Question> getQuestionByQuizId(Pageable pageable, Integer id) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Question> questionList = questionRepository.getQuestionByQuizID(id);
        List<Question> list;
        Page<Question> coursePage;
        if (questionList.size() < startItem) {
            list = Collections.emptyList();
            coursePage
                    = new PageImpl<Question>(list, PageRequest.of(currentPage, pageSize), questionList.size());
        } else {
            int toIndex = Math.min(startItem + pageSize, questionList.size());
            list = questionList.subList(startItem, toIndex);
            coursePage
                    = new PageImpl<Question>(list, PageRequest.of(currentPage, pageSize),questionList.size());
        }
        return coursePage;
    }

}
