package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Coursecontent;
import com.swp391.onlinequiz.repository.CoursecontentRepository;
import com.swp391.onlinequiz.service.CoursecontentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CourseContentServiceImpl implements CoursecontentService {
    private CoursecontentRepository coursecontentRepository;

    public CourseContentServiceImpl(CoursecontentRepository coursecontentRepository) {
        this.coursecontentRepository = coursecontentRepository;
    }

    @Override
    public Coursecontent findByContentID(Integer contentId) {
        return coursecontentRepository.findContentById(contentId);
    }

    @Override
    public Coursecontent findFirstContent(Integer courseID) {
        return coursecontentRepository.findFirstContentOfCOurse(courseID);
    }
}
