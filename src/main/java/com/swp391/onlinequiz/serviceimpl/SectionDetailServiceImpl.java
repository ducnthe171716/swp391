package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.ContentStatusDetail;
import com.swp391.onlinequiz.model.Sectiondetail;
import com.swp391.onlinequiz.repository.ContentStatusDetailRepository;
import com.swp391.onlinequiz.repository.SectionDetailRepository;
import com.swp391.onlinequiz.service.ContentStatusDetailService;
import com.swp391.onlinequiz.service.SectionDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SectionDetailServiceImpl implements SectionDetailService {
    private SectionDetailRepository sectionDetailRepository;

    public SectionDetailServiceImpl(SectionDetailRepository sectionDetailRepository) {
        this.sectionDetailRepository = sectionDetailRepository;
    }

    @Override
    public List<Sectiondetail> findByEnrollID(Integer enrollid) {
        return sectionDetailRepository.findByEnroll_Enrollid(enrollid);
    }

    @Override
    public Sectiondetail findSectionDetail(Integer enrollid, Integer sectionid) {
        return sectionDetailRepository.findByEnroll_EnrollidAndCoursesection_SectionId(enrollid, sectionid);
    }
}
