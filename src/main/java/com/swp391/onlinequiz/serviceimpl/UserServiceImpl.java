package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.ExceptionHandling.UsersNotfoundException;
import com.swp391.onlinequiz.dto.UsersDTO;
import com.swp391.onlinequiz.model.Confirmationtoken;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.ConfirmationtokenRepository;
import com.swp391.onlinequiz.repository.UsersRepository;
import com.swp391.onlinequiz.service.UserService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;

import java.time.ZoneId;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private final UsersRepository usersRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JavaMailSender emailSender;
    private final ConfirmationtokenRepository confirmationtokenRepository;

    private final EmailServiceImpl emailService;

    public Users getUsersByUserDTOid(Integer id) {
        return usersRepository.findById(id).get();
    }


    public UserServiceImpl(UsersRepository usersRepository, ConfirmationtokenRepository confirmationtokenRepository, EmailServiceImpl emailService) {
        this.usersRepository = usersRepository;
        this.confirmationtokenRepository = confirmationtokenRepository;
        this.emailService = emailService;
    }

    @Override
    public List<UsersDTO> getUserList() {
        List<Users> uList = usersRepository.getAllUsers();
        List<UsersDTO> udtoList = new ArrayList<UsersDTO>();
        for (Users users : uList) {
            udtoList.add(new UsersDTO(users));
        }
        return udtoList;
    }

    @Override
    public UsersDTO getLoginUser(String identify, String password) {
        List<Users> loginUsers = usersRepository.findAcount(identify);
        if (loginUsers.isEmpty()) {
            return null;
        }

        String encodedPassword = loginUsers.get(0).getPassword();


        if (passwordEncoder.matches(password, encodedPassword)) {
            return new UsersDTO(loginUsers.get(0));
        } else {
            return null;
        }
    }



    @Override

    public String saveUser(Users user) {

        user.setIsenabled(false);
        user.setRole("ROLE_MENTEE");
        user.setDob(LocalDateTime.now());

        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        LocalDateTime currentTime = LocalDateTime.now();
        user.setRegisterdob(LocalDate.now());
        usersRepository.save(user);

        Confirmationtoken confirmationToken = new Confirmationtoken(user);

        try {
            confirmationtokenRepository.save(confirmationToken);

            MimeMessage mimeMessage = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

            helper.setTo(user.getEmail());
            helper.setSubject("Complete Registration!");

            String confirmationLink = "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationtoken();

            String htmlContent = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "  <body>\n" +
                    "    <div style=\"font-family: Helvetica, Arial, sans-serif; min-width: 1000px; overflow: auto; line-height: 2\">\n" +
                    "      <div style=\"margin: 50px auto; width: 70%; padding: 20px 0\">\n" +
                    "        <div style=\"border-bottom: 1px solid #eee\">\n" +
                    "          <a href=\"http://localhost:8080/home\" style=\"font-size: 1.4em; color: #2a1777; text-decoration: none; font-weight: 600\">Educhamp</a>\n" +
                    "        </div>\n" +
                    "        <p style=\"font-size: 1.1em\">Hi,</p>\n" +
                    "        <p>Hi there. Use the following link to verify your account:</p>\n" +
                    "        <a href=\"" + confirmationLink + "\" style=\" margin: 0 auto; width: max-content; padding: 10px; color: #fff; text-decoration: none; background-color: #2a1777; border-radius: 4px; display: inline-block;\">Click here to verify your account</a>\n" +
                    "        <p style=\"font-size: 0.9em;\">Regards,<br />Educhamp</p>\n" +
                    "        <hr style=\"border: none; border-top: 1px solid #eee\" />\n" +
                    "        <div style=\"float: right; padding: 8px 0; color: #aaa; font-size: 0.8em; line-height: 1; font-weight: 300\">\n" +
                    "          <p>Educhamp Inc</p>\n" +
                    "          <p>FPT university</p>\n" +
                    "          <p>Hoa Lac</p>\n" +
                    "        </div>\n" +
                    "      </div>\n" +
                    "    </div>\n" +
                    "  </body>\n" +
                    "</html>";


            helper.setText(htmlContent, true);

            emailSender.send(mimeMessage);
        } catch (MailException | MessagingException e) {
            // Handle exceptions, e.g., log them
            e.printStackTrace();
            return "errorPage"; // Return an error page or handle the error accordingly
        }

        return "pages/client/register";
    }


    @Override
    public ModelAndView confirmEmail(String confirmationToken) {
        Confirmationtoken token = confirmationtokenRepository.findByConfirmationtoken(confirmationToken);
        ModelAndView model = new ModelAndView();

        model.addObject("token", token);
        if (token == null) {
            model.addObject("title", "Invalid Token!");
            model.addObject("message", " Please check again!");
            model.setViewName("pages/client/message");
        } else {
            if (new Date().getTime() - token.getCreatedDate().getTime() > 30000) {
                confirmationtokenRepository.deleteByExpirationDateBefore(token.getCreatedDate());
                usersRepository.deleteById(token.getUser().getUserid());
                model.addObject("title", "Token is expired!");
                model.addObject("message", " Please register again!");
                model.setViewName("pages/client/message");
            } else {
                Users user = usersRepository.findByEmailIgnoreCase(token.getUser().getEmail());

                user.setIsenabled(true);
                usersRepository.save(user);
                model.addObject("title", "Account");
                model.addObject("message", "Verify successfully!");
                model.setViewName("pages/client/message");
            }
        }
        return model;
    }

    @Override
    public void updateResetPasswordToken(String token, String email) throws UsersNotfoundException {
        Users users = usersRepository.findByEmailIgnoreCase(email);
        if (users != null) {
            users.setResetPasswordToken(token);
            usersRepository.save(users);
        } else {
            throw new UsersNotfoundException("Could not find any customer with the email " + email);
        }
    }

    @Override
    public Users getByResetPasswordToken(String token) {
        return usersRepository.findByResetPasswordToken(token);
    }

    @Override
    public void updatePassword(Users user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);

        user.setResetPasswordToken(null);
        usersRepository.save(user);
    }

    @Override
    public void updateUserProfile(Integer userId, String fullname, String email, LocalDateTime dob, String address, boolean gender) {

        // Lấy thông tin người dùng cần cập nhật từ cơ sở dữ liệu
        Users existingUser = usersRepository.findById(userId).get();

        if (existingUser != null) {
            // Cập nhật thông tin cá nhân dựa trên updatedUser DTO
            existingUser.setFullname(fullname);
            existingUser.setEmail(email);
            existingUser.setDob(dob);
            existingUser.setAddress(address);
            existingUser.setGender(gender);

            // Lưu thông tin người dùng đã được cập nhật vào cơ sở dữ liệu
            usersRepository.save(existingUser);
        }
    }

    @Override
    public boolean changePassword(int userId, String oldPassword, String newPassword) {
        // Lấy thông tin người dùng cần cập nhật từ cơ sở dữ liệu
        Users existingUser = usersRepository.findById(userId);

        if (existingUser != null) {
            if (passwordEncoder.matches(oldPassword, existingUser.getPassword())) {
                // Cập nhật thông tin cá nhân dựa trên updatedUser DTO
                existingUser.setPassword(passwordEncoder.encode(newPassword));
                // Lưu thông tin người dùng đã được cập nhật vào cơ sở dữ liệu
                usersRepository.save(existingUser);
                return true;
            } else {
                System.out.println("Mật khẩu không khớp");
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean changeAvatar(int userId, String avatar) {
        // Lấy thông tin người dùng cần cập nhật từ cơ sở dữ liệu
        Users existingUser = usersRepository.findById(userId);

        if (existingUser != null) {
            existingUser.setAvatar(avatar);
            usersRepository.save(existingUser);
            return true;
        }
        return false;
    }

    @Override
    public Users getUserByID(int id) {
        return usersRepository.findById(id);
    }

    @Override
    public Users getUserByUserName(String username) {
        return usersRepository.findByUsername(username).get();
    }


    @Override
    public UsersDTO getUserDTObyUserid(Integer uid) {
        Users u = usersRepository.findById(uid).get();
        return new UsersDTO(u);
    }

}
