package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Schedule;
import com.swp391.onlinequiz.repository.ScheduleRepository;
import com.swp391.onlinequiz.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private  final ScheduleRepository scheduleRepository;

    public ScheduleServiceImpl(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    @Override
    public List<Schedule> getAllByUserID(int id){return scheduleRepository.findAllByUserID(id);}

    public void saveSchedule(Schedule schedule){scheduleRepository.save(schedule);}
}
