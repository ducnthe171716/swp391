package com.swp391.onlinequiz.serviceimpl;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.swp391.onlinequiz.model.Answer;
import com.swp391.onlinequiz.model.Answerreport;
import com.swp391.onlinequiz.model.Myquizreport;
import com.swp391.onlinequiz.model.Question;
import com.swp391.onlinequiz.model.Questionreport;
import com.swp391.onlinequiz.model.Quiz;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.AnswerreportRepository;
import com.swp391.onlinequiz.repository.MyquizreportRepository;
import com.swp391.onlinequiz.repository.QuestionReportRepository;
import com.swp391.onlinequiz.repository.QuestionRepository;
import com.swp391.onlinequiz.service.MyquizreportService;

@Service
@Transactional
public class MyQuizReportImpl implements MyquizreportService {

    @Autowired
    private MyquizreportRepository myquizreportRepository;

    @Autowired
    private AnswerreportRepository answerreportRepository;

    @Autowired
    private QuestionReportRepository questionReportRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public List<Myquizreport> getMyQuizReport(Users u, Quiz q) {
        return myquizreportRepository.findByUserAndQuiz(u, q);
    }

    @Override
    public Myquizreport createMyQuizReport(Quiz q, Users u) {
        Myquizreport myQuizReport = myquizreportRepository.findByUserAndQuiz(u, q).get(0);
        if (myQuizReport != null) {
            // Xóa MyQuizReport để kèm theo việc xóa các QuestionReport và AnswerReport
            deleteQuestionReports(myQuizReport);
        } else {
            myQuizReport = new Myquizreport(u, q, null, LocalDateTime.now());
        }

        List<Question> randomQuestions = questionRepository.findRandomQuestionsByQuizId(q, q.getRandquestion());
        Integer i = 0;
        for (Question question : randomQuestions) {
            i++;
            Questionreport questionReport = new Questionreport(u, question, myQuizReport, null, i);
            questionReportRepository.save(questionReport);

            Set<Answer> answers = question.getAnswerSet();

            for (Answer answer : answers) {
                Answerreport answerReport = new Answerreport(u, answer, null, questionReport);
                answerreportRepository.save(answerReport);
                questionReport.getAnswerreports().add(answerReport);
            }

            myQuizReport.getQuestionreport().add(questionReport);
        }
        return myquizreportRepository.save(myQuizReport);

    }

    // Thêm phương thức xóa các QuestionReport
    public void deleteQuestionReports(Myquizreport myquizreport) {
        List<Questionreport> questionreports = questionReportRepository.findByMyquizreport(myquizreport);
        for (Questionreport questionreport : questionreports) {
            myquizreport.getQuestionreport().remove(questionreport);
            questionReportRepository.delete(questionreport);
        }
    }

    // Thêm phương thức xóa các AnswerReport
    public void deleteAnswerReports(Questionreport questionreport) {
        List<Answerreport> answerreports = answerreportRepository.findByQuestionreport(questionreport);
        for (Answerreport answerreport : answerreports) {
            questionreport.getAnswerreports().remove(answerreport);
            answerreportRepository.delete(answerreport);
        }
    }

    @Override
    public Myquizreport getMyQuizReportById(Integer qrid) {
        return myquizreportRepository.findByMyquizreportid(qrid).get(0);
    }

    @Override
    public void restoreQuizReport(Users u, Map<String, List<String>> resultdata, String[] flagq) {
        // TODO Auto-generated method stub
        for (String questionresult : resultdata.keySet()) {
            Integer questionrpid = Integer.parseInt(questionresult.substring(1));

            Questionreport qsrp = questionReportRepository.findById(questionrpid).get();
            qsrp.setIscorrect(true);
            if (flagq != null) {
                for (String flag : flagq) {
                    if (qsrp.getId() == Integer.parseInt(flag)) {
                        qsrp.setIsflag(true);
                        break;
                    }
                    qsrp.setIscorrect(false);
                }
            }

            for (String arptid : resultdata.get(questionresult)) {
                Answerreport asrp = answerreportRepository.findById(Integer.parseInt(arptid)).get();
                asrp.setSelectreport(true);
                answerreportRepository.save(asrp);
                if (asrp.getAnswer().getCorrect() == false) {
                    qsrp.setIscorrect(false);
                }
            }
            questionReportRepository.save(qsrp);

        }
    }

}
