package com.swp391.onlinequiz.serviceimpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.swp391.onlinequiz.model.Enroll;

@Service
public class CertificateService {
    public static ByteArrayInputStream certificateExport(Enroll enroll) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String templatePath = "src/main/resources/templates/certificatetemplate/CertificateTemplate.pdf";
        try {
            PdfReader reader = new PdfReader(templatePath);
            PdfStamper stamper = new PdfStamper(reader, outputStream);

            // Chèn nội dung (tên người học) vào tệp PDF
            PdfContentByte content = stamper.getOverContent(1); // Trang đầu tiên của tệp template

            //date
            int DATE_SIZE = 12; // Cỡ chữ
            Font fontdate = new Font(FontFactory.getFont(FontFactory.COURIER_BOLD, DATE_SIZE));
            LocalDateTime finishdate = enroll.getFinishdate();
            int day = finishdate.getDayOfMonth();
            int month = finishdate.getMonthValue();
            int year = finishdate.getYear();
            String time = day+"/"+month+"/"+year;
            Paragraph date = new Paragraph(time, fontdate);
            date.setAlignment(Element.ALIGN_CENTER);
            ColumnText.showTextAligned(content, Element.ALIGN_LEFT, date, 105, 390, 0); 

            //name
            int NAME_SIZE = 28; // Cỡ chữ
            Font fontname = new Font(FontFactory.getFont(FontFactory.COURIER_OBLIQUE, NAME_SIZE));
            Paragraph name = new Paragraph(enroll.getUser().getFullname(), fontname);
            name.setAlignment(Element.ALIGN_CENTER);
            ColumnText.showTextAligned(content, Element.ALIGN_LEFT, name, 100, 350, 0); 

            //success
            int SIZE_1 = 12; // Cỡ chữ
            Font success = new Font(FontFactory.getFont(FontFactory.COURIER_OBLIQUE, SIZE_1));
            Paragraph paragraph = new Paragraph("has successfully completed", success);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            ColumnText.showTextAligned(content, Element.ALIGN_LEFT, paragraph, 105, 320, 0);

            //course
            int COURSENAME_SIZE = 19; // Cỡ chữ
            Font coursenamefont = new Font(FontFactory.getFont(FontFactory.TIMES_BOLDITALIC, COURSENAME_SIZE));
            Paragraph coursename = new Paragraph(enroll.getCourse().getCoursename(), coursenamefont);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            ColumnText.showTextAligned(content, Element.ALIGN_LEFT, coursename, 105, 290, 0);

            //description
            
            int SIZE_2 = 12; // Cỡ chữ
            Font font_2 = new Font(FontFactory.getFont(FontFactory.COURIER_OBLIQUE, SIZE_2));
            Paragraph paragraph2 = new Paragraph("an online non-credit course authorized by"+enroll.getCourse().getCourseowner().getUser().getFullname(), font_2);
            paragraph2.setAlignment(Element.ALIGN_CENTER);
            ColumnText.showTextAligned(content, Element.ALIGN_LEFT, paragraph2, 105, 270, 0);

            int SIZE_3 = 12; // Cỡ chữ
            Font font_3 = new Font(FontFactory.getFont(FontFactory.COURIER_OBLIQUE, SIZE_3));
            Paragraph paragraph3 = new Paragraph("and offered through EduChamps", font_3);
            paragraph2.setAlignment(Element.ALIGN_CENTER);
            ColumnText.showTextAligned(content, Element.ALIGN_LEFT, paragraph3, 105, 255, 0);
            stamper.close();
            reader.close();

            byte[] byteArray = outputStream.toByteArray();
            return new ByteArrayInputStream(byteArray);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
