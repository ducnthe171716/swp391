package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Answer;
import com.swp391.onlinequiz.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.swp391.onlinequiz.model.Questionreport;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.AnswerreportRepository;
import com.swp391.onlinequiz.service.AnswerService;

import java.util.List;


@Service
@Transactional
public class AnswerServiceImpl implements AnswerService{

    @Autowired
    private AnswerreportRepository answerreportRepository;
    @Autowired
    private AnswerRepository answerRepository;

    @Override
    public void removeAnswerReport(Users u, Questionreport qr) {
        answerreportRepository.deleteByUserAndQuestionreport(u, qr);
    }

    @Override
    public List<Answer> getAnswerByQuesID(Integer questionID) {
        return answerRepository.getAnswerByQuestionID(questionID);
    }

    @Override
    public void saveAnswer(Answer answer) {
        answerRepository.save(answer);
    }

    @Override
    public void updateAnswer(String title, Integer idQuestion) {
        answerRepository.updateAnswer(title, idQuestion);
    }

    @Override
    public void deleteAnswer(Integer id) {
        answerRepository.deleteById(id);
    }

    @Override
    public void updateRightAnswer(Integer id) {
        answerRepository.updateRightAnswer(id);
    }


}
