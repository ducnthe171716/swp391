package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.model.Feedbackdetail;
import com.swp391.onlinequiz.model.Users;
import com.swp391.onlinequiz.repository.FeedbackdetailRepository;
import com.swp391.onlinequiz.service.FeedbackdetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FeebackDetailServiceImpl implements FeedbackdetailService {
    public FeedbackdetailRepository repository;

    @Autowired
    public FeebackDetailServiceImpl(FeedbackdetailRepository repository) {
        this.repository = repository;
    }

    @Override
    public Map<Integer, Integer> countRating(Integer courseid) {
        Map<Integer, Integer> ratings = new HashMap<>();
        List<Integer> stars = new ArrayList<>();
        stars.add(1);
        stars.add(2);
        stars.add(3);
        stars.add(4);
        stars.add(5);
        for (Integer ratingStar : stars) {
            Integer count = repository.countByFbratingAndCourse_Courseid(ratingStar, courseid);
            ratings.put(ratingStar, count);
        }
        return ratings;
    }

    @Override
    public void saveFeedback(Feedbackdetail feedbackdetail) {
        repository.save(feedbackdetail);
    }

    @Override
    public Feedbackdetail findFeedbackByUserAndCourse(Users user, Course course) {
        return repository.findByUserAndCourse(user, course );
    }

    @Override
    public void updateFeedback(Integer rating, String datepost, String opinion, Integer userid) {
        repository.updateFeedbackdetailByUser(rating, datepost, opinion, userid);
    }


}
