package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.Course;
import com.swp391.onlinequiz.repository.CourseRepository;
import com.swp391.onlinequiz.service.CourseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRespository;

    public CourseServiceImpl(CourseRepository courseRespository) {
        this.courseRespository = courseRespository;
    }
    @Override
    public Page<Course> findPaginated(Pageable pageable, List<Course> courses) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Course> list;

        if (courses.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, courses.size());
            list = courses.subList(startItem, toIndex);
        }

        Page<Course> coursePage
                = new PageImpl<Course>(list, PageRequest.of(currentPage, pageSize), courses.size());
        return coursePage;
    }

    @Override
    public List<Course> getAll() {
        return courseRespository.findAll();
    }

    @Override
    public List<Course> getCourseByCategory(Integer categoryID) {
        return courseRespository.findCourseByCategory(categoryID);
    }

    @Override
    public List<Course>  getCourseBySearchValue(String searchValue) {
        return courseRespository.findCourseBySearchValue(searchValue);
    }

    @Override
    public Course getCourseByID(int id) {
        return courseRespository.findById(id).get();
    }

    @Override
    public Page<Course> getCourseByOwnerID(Pageable pageable, int id, String searchValue, Integer reviews, Integer statusID, String orderBy, Integer categoryID) {
        List<Course> courses = courseRespository.findCourseByOwnerID(id,searchValue,reviews,statusID,orderBy,categoryID);
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Course> list;
        if (courses.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, courses.size());
            list = courses.subList(startItem, toIndex);
        }

        Page<Course> coursePage
                = new PageImpl<Course>(list, PageRequest.of(currentPage, pageSize), courses.size());
        return coursePage;
    }

    @Override
    public Course save(Course course) {
        return courseRespository.save(course);
    }


}
