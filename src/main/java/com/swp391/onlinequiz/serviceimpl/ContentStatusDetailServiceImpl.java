package com.swp391.onlinequiz.serviceimpl;

import com.swp391.onlinequiz.model.ContentStatusDetail;
import com.swp391.onlinequiz.repository.ContentStatusDetailRepository;
import com.swp391.onlinequiz.service.ContentStatusDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ContentStatusDetailServiceImpl implements ContentStatusDetailService {
    private ContentStatusDetailRepository contentStatusDetailRepository;

    public ContentStatusDetailServiceImpl(ContentStatusDetailRepository contentStatusDetailRepository) {
        this.contentStatusDetailRepository = contentStatusDetailRepository;
    }

    @Override
    public void updateStatus(Integer courseid, Integer sectionid) {
        contentStatusDetailRepository.updateRightAnswer(courseid, sectionid);
    }

    @Override
    public List<ContentStatusDetail> listContentStatusDetail(Integer sectionid) {
        return contentStatusDetailRepository.findBySectiondetail_SectionDeatailId(sectionid);
    }

}
