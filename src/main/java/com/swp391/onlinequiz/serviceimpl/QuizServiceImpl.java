package com.swp391.onlinequiz.serviceimpl;

import java.time.LocalDateTime;
import java.util.List;

import com.swp391.onlinequiz.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.swp391.onlinequiz.model.Quiz;
import com.swp391.onlinequiz.repository.QuizRepository;
import com.swp391.onlinequiz.service.QuizService;



@Service
@Transactional
public class QuizServiceImpl implements QuizService{

    @Autowired
    private QuizRepository quizRepository;

    @Override
    public Quiz getQuizByQuizId(Integer id) {
        List<Quiz> quiz = quizRepository.findByQuizid(id);
        if(!quiz.isEmpty()){
            return quiz.get(0);
        }
        return null;
    }

    @Override
    public void save(Quiz quiz) {
        quizRepository.save(quiz);
    }

    @Override
    public List<Quiz> getQuizBySectionID(Integer sectionID) {
        return quizRepository.getQuizBySectionID(sectionID);
    }

    @Override
    public void updateQuiz(String title, Integer time, Integer randquestion, Double passpoint, LocalDateTime quizdob, Integer quizid) {
        quizRepository.updateFeedbackdetailByUser(title,time,randquestion,passpoint,quizdob,quizid);
    }

    @Override
    public void deleteQuiz(Integer idQuiz) {
            quizRepository.deleteById(idQuiz);
    }






}
