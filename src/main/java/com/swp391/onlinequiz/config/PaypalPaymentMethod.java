package com.swp391.onlinequiz.config;

/**
 * PaypalPaymentMethod
 */
public enum PaypalPaymentMethod {
    credit_card, paypal
    
}
