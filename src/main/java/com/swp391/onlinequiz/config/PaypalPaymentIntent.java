package com.swp391.onlinequiz.config;

public enum PaypalPaymentIntent {
    sale, authorize, order
}
