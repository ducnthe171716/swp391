package com.swp391.onlinequiz.model;

import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.transaction.Transactional;

@Entity
@Transactional
public class Language {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer languageid;

    private String languagen;

    @OneToMany(mappedBy = "language")
    private Set<Course> courses;

    public Language() {
    }

    public Language(Integer languageid, String languagen) {
        this.languageid = languageid;
        this.languagen = languagen;
    }

    public Integer getLanguageid() {
        return languageid;
    }

    public void setLanguageid(Integer languageid) {
        this.languageid = languageid;
    }

    public String getLanguagen() {
        return languagen;
    }

    public void setLanguagen(String languagen) {
        this.languagen = languagen;
    }

    
}
