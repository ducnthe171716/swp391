package com.swp391.onlinequiz.model;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer categoryid;

    private String categoryname;

    @OneToMany(mappedBy = "category")
    private Set<Course> courseSet;

    public Category() {
    }

    public Category(Integer categoryid, String categoryname, Set<Course> courseSet) {
        this.categoryid = categoryid;
        this.categoryname = categoryname;
        this.courseSet = courseSet;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Set<Course> getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(Set<Course> courseSet) {
        this.courseSet = courseSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(categoryid, category.categoryid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryid);
    }
}
