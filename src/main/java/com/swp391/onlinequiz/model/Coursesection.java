package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Coursesection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer sectionId;

    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course;

    private String sectiontitle;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime sectiondob;

    private Integer sectionorder;

    @OneToOne(mappedBy = "coursesection")
    private Quiz quizSet;

    @OneToMany(mappedBy = "coursesection")
    private Set<Coursecontent> coursecontents;


    @OneToMany(mappedBy = "coursesection")
    private Set<Sectiondetail> sectiondetailSet;

    public Coursesection() {
    }


    public Coursesection(Integer sectionId, Course course, String sectiontitle, LocalDateTime sectiondob, Integer sectionorder, Quiz quizSet, Set<Coursecontent> coursecontents, Set<Sectiondetail> sectiondetailSet) {
        this.sectionId = sectionId;
        this.course = course;
        this.sectiontitle = sectiontitle;
        this.sectiondob = sectiondob;
        this.sectionorder = sectionorder;
        this.quizSet = quizSet;
        this.coursecontents = coursecontents;
        this.sectiondetailSet = sectiondetailSet;
    }

    public Set<Sectiondetail> getSectiondetailSet() {
        return sectiondetailSet;
    }

    public void setSectiondetailSet(Set<Sectiondetail> sectiondetailSet) {
        this.sectiondetailSet = sectiondetailSet;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getSectiontitle() {
        return sectiontitle;
    }

    public void setSectiontitle(String sectiontitle) {
        this.sectiontitle = sectiontitle;
    }

    public LocalDateTime getSectiondob() {
        return sectiondob;
    }

    public void setSectiondob(LocalDateTime sectiondob) {
        this.sectiondob = sectiondob;
    }

    public Integer getSectionorder() {
        return sectionorder;
    }

    public void setSectionorder(Integer sectionorder) {
        this.sectionorder = sectionorder;
    }

    public Quiz getQuizSet() {
        return quizSet;
    }

    public void setQuizSet(Quiz quizSet) {
        this.quizSet = quizSet;
    }

    public Set<Coursecontent> getCoursecontents() {
        return coursecontents;
    }

    public void setCoursecontents(Set<Coursecontent> coursecontents) {
        this.coursecontents = coursecontents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coursesection that = (Coursesection) o;
        return Objects.equals(sectionId, that.sectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sectionId);
    }
}
