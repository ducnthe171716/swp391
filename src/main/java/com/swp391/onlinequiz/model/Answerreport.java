package com.swp391.onlinequiz.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Transactional
public class Answerreport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "answerid")
    private Answer answer;

    private Boolean selectreport;

    @ManyToOne
    @JoinColumn(name = "questionreportid")
    private Questionreport questionreport;

    public Answerreport(Users user, Answer answer, Boolean selectreport, Questionreport questionreport) {
        this.user = user;
        this.answer = answer;
        this.selectreport = selectreport;
        this.questionreport = questionreport;
    }
}
