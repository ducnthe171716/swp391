package com.swp391.onlinequiz.model;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.util.Objects;

@Entity
@Transactional
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer answerId;

    @ManyToOne
    @JoinColumn(name = "questionid")
    private Question question;

    private String content;

    private Boolean correct;

    private Integer answerorder;

    public Answer() {
    }

    public Answer(Integer answerId, Question question, String content, Boolean correct, Integer answerorder) {
        this.answerId = answerId;
        this.question = question;
        this.content = content;
        this.correct = correct;
        this.answerorder = answerorder;
    }

    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Integer getAnswerorder() {
        return answerorder;
    }

    public void setAnswerorder(Integer answerorder) {
        this.answerorder = answerorder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return Objects.equals(answerId, answer.answerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(answerId);
    }
}
