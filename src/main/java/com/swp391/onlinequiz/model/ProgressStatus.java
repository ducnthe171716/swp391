package com.swp391.onlinequiz.model;

import jakarta.persistence.*;

import java.util.Set;

@Entity
public class ProgressStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer statusId;

    private String statusName;
    @OneToMany(mappedBy = "status")
    private Set<Course> courseSet;

    public ProgressStatus() {
    }

    public ProgressStatus(Integer statusId, String statusName, Set<Course> courseSet) {
        this.statusId = statusId;
        this.statusName = statusName;
        this.courseSet = courseSet;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Set<Course> getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(Set<Course> courseSet) {
        this.courseSet = courseSet;
    }
}
