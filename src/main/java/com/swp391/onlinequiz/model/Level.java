package com.swp391.onlinequiz.model;

import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.transaction.Transactional;

@Entity
@Transactional
public class Level {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer levelid;

    private String levelname;

    @OneToMany(mappedBy = "level")
    private Set<Course> courses;

    public Level(Integer levelid, String levelname) {
        this.levelid = levelid;
        this.levelname = levelname;
    }

    

    public Level(String levelname) {
        this.levelname = levelname;
    }

    public Level() {
    }

    public Integer getLevelid() {
        return levelid;
    }

    public void setLevelid(Integer levelid) {
        this.levelid = levelid;
    }

    public String getLevelname() {
        return levelname;
    }

    public void setLevelname(String levelname) {
        this.levelname = levelname;
    }

    

}
