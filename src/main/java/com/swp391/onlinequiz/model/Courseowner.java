package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Courseowner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ownerid;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime beownerdob;

    private String phone;

    private String major;

    @Column(columnDefinition = "TEXT")
    private String aboutme;

    public String getAboutme() {
        return aboutme;
    }

    public void setAboutme(String aboutme) {
        this.aboutme = aboutme;
    }

    @OneToMany(mappedBy = "courseowner")
    private Set<Course> courseSet;

    public Courseowner() {
    }

    public Courseowner(Integer ownerid, Users user, LocalDateTime beownerdob, String phone, String major, Set<Course> courseSet) {
        this.ownerid = ownerid;
        this.user = user;
        this.beownerdob = beownerdob;
        this.phone = phone;
        this.major = major;
        this.courseSet = courseSet;
    }

    public Integer getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(Integer ownerid) {
        this.ownerid = ownerid;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public LocalDateTime getBeownerdob() {
        return beownerdob;
    }

    public void setBeownerdob(LocalDateTime beownerdob) {
        this.beownerdob = beownerdob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Set<Course> getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(Set<Course> courseSet) {
        this.courseSet = courseSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courseowner that = (Courseowner) o;
        return Objects.equals(ownerid, that.ownerid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ownerid);
    }

    public int getTotalRating(){
        int totalRating = 0;
        for (Course course : courseSet) {
            totalRating += course.getFeedbackdetailSet().size();
        }
        return totalRating;
    }
    public int getAverageRating(){
        int  totalRating = 0;
        for (Course course : courseSet) {
            totalRating += course.getAverageRating();
        }
        return totalRating/courseSet.size();
    }
}
