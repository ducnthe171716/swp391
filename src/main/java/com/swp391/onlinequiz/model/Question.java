package com.swp391.onlinequiz.model;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer questionid;

    @ManyToOne
    @JoinColumn(name = "quizid")
    private Quiz quiz;

    private String questioncontent;

    private Integer questionorder;

    @OneToMany(mappedBy = "question")
    private Set<Answer> answerSet;

    public Question() {
    }

    public Question(Integer questionid, Quiz quiz, String questioncontent, Integer questionorder, Set<Answer> answerSet) {
        this.questionid = questionid;
        this.quiz = quiz;
        this.questioncontent = questioncontent;
        this.questionorder = questionorder;
        this.answerSet = answerSet;
    }

    public Integer getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Integer questionid) {
        this.questionid = questionid;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getQuestioncontent() {
        return questioncontent;
    }

    public void setQuestioncontent(String questioncontent) {
        this.questioncontent = questioncontent;
    }

    public Integer getQuestionorder() {
        return questionorder;
    }

    public void setQuestionorder(Integer questionorder) {
        this.questionorder = questionorder;
    }

    public Set<Answer> getAnswerSet() {
        return answerSet;
    }

    public void setAnswerSet(Set<Answer> answerSet) {
        this.answerSet = answerSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return Objects.equals(questionid, question.questionid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionid);
    }
}
