package com.swp391.onlinequiz.model;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import java.util.Calendar;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Transactional
public class Confirmationtoken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer tokenid;
    @Column(name="confirmationtoken")
    private String confirmationtoken;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @OneToOne(targetEntity = Users.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "userid")
    private Users user;



    public Confirmationtoken() {
    }

    public Confirmationtoken(Users user) {
        this.user = user;
        createdDate = new Date();
        confirmationtoken = UUID.randomUUID().toString();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 1);
        calendar.setTime(createdDate);
        expirationDate = calendar.getTime();
    }

    public Confirmationtoken(Integer tokenid, String confirmationtoken, Date createdDate, Users user) {
        this.tokenid = tokenid;
        this.confirmationtoken = confirmationtoken;
        this.createdDate = createdDate;
        this.user = user;
    }

    public Integer getTokenid() {
        return tokenid;
    }

    public void setTokenid(Integer tokenid) {
        this.tokenid = tokenid;
    }

    public String getConfirmationtoken() {
        return confirmationtoken;
    }

    public void setConfirmationtoken(String confirmationtoken) {
        this.confirmationtoken = confirmationtoken;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Confirmationtoken that = (Confirmationtoken) o;
        return Objects.equals(tokenid, that.tokenid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenid);
    }
}
