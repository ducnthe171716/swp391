package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class ContentStatusDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer contentStatusDetailID;

    @ManyToOne
    @JoinColumn(name = "sectionDeatailId")
    private Sectiondetail sectiondetail;
    @ManyToOne
    @JoinColumn(name = "contentId")
    private Coursecontent coursecontent;

    private boolean status;

    public ContentStatusDetail() {
    }

    public ContentStatusDetail(Integer contentStatusDetailID, Sectiondetail sectiondetail, Coursecontent coursecontent, boolean status) {
        this.contentStatusDetailID = contentStatusDetailID;
        this.sectiondetail = sectiondetail;
        this.coursecontent = coursecontent;
        this.status = status;
    }

    public Integer getContentStatusDetailID() {
        return contentStatusDetailID;
    }

    public void setContentStatusDetailID(Integer contentStatusDetailID) {
        this.contentStatusDetailID = contentStatusDetailID;
    }

    public Sectiondetail getSectiondetail() {
        return sectiondetail;
    }

    public void setSectiondetail(Sectiondetail sectiondetail) {
        this.sectiondetail = sectiondetail;
    }

    public Coursecontent getCoursecontent() {
        return coursecontent;
    }

    public void setCoursecontent(Coursecontent coursecontent) {
        this.coursecontent = coursecontent;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContentStatusDetail that = (ContentStatusDetail) o;
        return Objects.equals(contentStatusDetailID, that.contentStatusDetailID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contentStatusDetailID);
    }
}
