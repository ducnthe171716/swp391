package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
public class Schedule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer scheduleid;

    private String titleschedule;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "courseid")
    private Course course;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime settime;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime reminderafter;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    public Schedule() {
    }

    public Schedule(Integer scheduleid, String titleschedule, Course course, LocalDateTime settime, LocalDateTime reminderafter, Users user) {
        this.scheduleid = scheduleid;
        this.titleschedule = titleschedule;
        this.course = course;
        this.settime = settime;
        this.reminderafter = reminderafter;
        this.user = user;
    }

    public Integer getScheduleid() {
        return scheduleid;
    }

    public void setScheduleid(Integer scheduleid) {
        this.scheduleid = scheduleid;
    }

    public String getTitleschedule() {
        return titleschedule;
    }

    public void setTitleschedule(String titleschedule) {
        this.titleschedule = titleschedule;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public LocalDateTime getSettime() {
        return settime;
    }

    public void setSettime(LocalDateTime settime) {
        this.settime = settime;
    }

    public LocalDateTime getReminderafter() {
        return reminderafter;
    }

    public void setReminderafter(LocalDateTime reminderafter) {
        this.reminderafter = reminderafter;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return Objects.equals(scheduleid, schedule.scheduleid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheduleid);
    }
}
