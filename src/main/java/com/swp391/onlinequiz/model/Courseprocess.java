package com.swp391.onlinequiz.model;

import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.util.Objects;

@Entity
@Transactional
public class Courseprocess {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer processid;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "contentid")
    private Coursecontent coursecontent;

    private Boolean contentstatus;

    public Courseprocess() {
    }

    public Courseprocess(Integer processid, Users user, Coursecontent coursecontent, Boolean contentstatus) {
        this.processid = processid;
        this.user = user;
        this.coursecontent = coursecontent;
        this.contentstatus = contentstatus;
    }

    public Integer getProcessid() {
        return processid;
    }

    public void setProcessid(Integer processid) {
        this.processid = processid;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Coursecontent getCoursecontent() {
        return coursecontent;
    }

    public void setCoursecontent(Coursecontent coursecontent) {
        this.coursecontent = coursecontent;
    }

    public Boolean getContentstatus() {
        return contentstatus;
    }

    public void setContentstatus(Boolean contentstatus) {
        this.contentstatus = contentstatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courseprocess that = (Courseprocess) o;
        return Objects.equals(processid, that.processid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processid);
    }
}
