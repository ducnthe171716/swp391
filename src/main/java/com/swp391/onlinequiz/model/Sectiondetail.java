package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Sectiondetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer sectionDeatailId;

    @ManyToOne
    @JoinColumn(name = "enrollid")
    private Enroll enroll;
    @OneToMany(mappedBy = "sectiondetail")
    private Set<ContentStatusDetail> contentStatusDetailSet;
    @ManyToOne
    @JoinColumn(name = "sectionId")
    private Coursesection coursesection;



    public Sectiondetail(Integer sectionDeatailId, Enroll enroll, Set<ContentStatusDetail> contentStatusDetailSet, Coursesection coursesection, Myquizreport myquizreport) {
        this.sectionDeatailId = sectionDeatailId;
        this.enroll = enroll;
        this.contentStatusDetailSet = contentStatusDetailSet;
        this.coursesection = coursesection;
    }
    public Sectiondetail() {

    }

    public Integer getSectionDeatailId() {
        return sectionDeatailId;
    }

    public void setSectionDeatailId(Integer sectionDeatailId) {
        this.sectionDeatailId = sectionDeatailId;
    }

    public Enroll getEnroll() {
        return enroll;
    }

    public void setEnroll(Enroll enroll) {
        this.enroll = enroll;
    }

    public Set<ContentStatusDetail> getContentStatusDetailSet() {
        return contentStatusDetailSet;
    }

    public void setContentStatusDetailSet(Set<ContentStatusDetail> contentStatusDetailSet) {
        this.contentStatusDetailSet = contentStatusDetailSet;
    }

    public Coursesection getCoursesection() {
        return coursesection;
    }

    public void setCoursesection(Coursesection coursesection) {
        this.coursesection = coursesection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sectiondetail that = (Sectiondetail) o;
        return Objects.equals(sectionDeatailId, that.sectionDeatailId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sectionDeatailId);
    }
}
