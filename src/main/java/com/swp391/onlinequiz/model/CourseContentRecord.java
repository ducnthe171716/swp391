package com.swp391.onlinequiz.model;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class CourseContentRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer contentRecordID;

    @ManyToOne
    @JoinColumn(name = "coursesectionid")
    private Coursesection coursesection;
}
