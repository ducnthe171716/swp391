package com.swp391.onlinequiz.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.transaction.Transactional;

@Entity
@Transactional
public class Preneeded {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer neededid;

    @ManyToOne
    @JoinColumn(name = "course_id") // Tên cột trong bảng Learningoutcome để liên kết với Course
    private Course course;

    @Column(columnDefinition = "TEXT") // Định nghĩa kiểu dữ liệu TEXT cho outcomeitem
    private String neededitem;

    public String getNeededitem() {
        return neededitem;
    }

    public void setNeededitem(String neededitem) {
        this.neededitem = neededitem;
    }

    public Preneeded() {
    }

    public Preneeded(Course course, String neededitem) {
        this.course = course;
        this.neededitem = neededitem;
    }

    public Integer getNeededid() {
        return neededid;
    }

    public void setNeededid(Integer neededid) {
        this.neededid = neededid;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
}
