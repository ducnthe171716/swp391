package com.swp391.onlinequiz.model;
import jakarta.persistence.*;
import java.util.Objects;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Entity
@Transactional
@RequiredArgsConstructor
public class Disscuss {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer commentid;

    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users  user;

    @ManyToOne
    @JoinColumn(name = "content_id")
    private Coursecontent coursecontent;

    private Integer parentId;

    @Column(columnDefinition = "TEXT")
    private String content;

    private String postdate;

    private Integer vote;

    private boolean status;

    public Disscuss(Course course, Users user, Integer parentId, String content, String postdate) {
        this.course = course;
        this.user = user;
        this.parentId = parentId;
        this.content = content;
        this.postdate = postdate;
    }
    public Disscuss(Course course, Users user, Coursecontent coursecontent, String content, String postdate) {
        this.course = course;
        this.user = user;
        this.coursecontent = coursecontent;
        this.content = content;
        this.postdate = postdate;
    }

    public Coursecontent getCoursecontent() {
        return coursecontent;
    }

    public void setCoursecontent(Coursecontent coursecontent) {
        this.coursecontent = coursecontent;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disscuss disscuss = (Disscuss) o;
        return Objects.equals(commentid, disscuss.commentid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentid);
    }
}
