package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;

@Entity
@Transactional
public class Myquizreport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer myquizreportid;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "quizid")
    private Quiz quiz;

    private Double result;

    
    private boolean ispass;

    public Boolean getIspass() {
        return ispass;
    }


    public void setIspass(Boolean ispass) {
        this.ispass = ispass;
    }


    public Boolean isPassed() {
        
        if(Double.valueOf(getFinalReults()).intValue() >= quiz.getPasspoint()){
            return true;
        }
        return false;
    }


    public Double getResult() {
        return result;
    }


    public void setResult(Double result) {
        this.result = result;
    }

    @OneToMany(mappedBy = "myquizreport")
    private List<Questionreport> questionreport = new ArrayList<>();

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime reportdob;

    private Integer timeleft;

    public String getTimeleft() {
        int minutes = timeleft / 60;
        int remainingSeconds = timeleft % 60;
        return String.format("%02d:%02d", minutes, remainingSeconds);
    }


    public void setTimeleft(Integer timeleft) {
        this.timeleft = timeleft;
    }


    public Myquizreport() {
    }


    public Myquizreport(Users user, Quiz quiz, Double result, LocalDateTime reportdob) {
        this.user = user;
        this.quiz = quiz;
        this.result = result;
        this.reportdob = reportdob;
        this.ispass = false;
    }

    public Integer getMyquizreportid() {
        return myquizreportid;
    }

    public void setMyquizreportid(Integer myquizreportid) {
        this.myquizreportid = myquizreportid;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }


    public LocalDateTime getReportdob() {
        return reportdob;
    }

    public void setReportdob(LocalDateTime reportdob) {
        this.reportdob = reportdob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Myquizreport that = (Myquizreport) o;
        return Objects.equals(myquizreportid, that.myquizreportid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(myquizreportid);
    }

    public List<Questionreport> getQuestionreport() {
        return questionreport;
    }

    public void setQuestionreport(List<Questionreport> questionreport) {
        this.questionreport = questionreport;
    }

    public double getFinalReults(){
        int nquestion = questionreport.size();
        int correctq = 0;
        for (Questionreport qsrp : questionreport) {
            if(qsrp.getIscorrect()!=null&&qsrp.getIscorrect()){
                correctq++;
            }
        }
        return correctq*100/nquestion;
    }

    public int notAnswerQuestion(){
        int i = 0;
        
        for (Questionreport question : questionreport) {
            
            if(question.getIscorrect()==null){
                i++;
            }
        }
        return i;
    }

    public int correctAnswerQuestion(){
        int i = 0;
        
        for (Questionreport question : questionreport) {
            
            if(question.getIscorrect()!= null && question.getIscorrect()){
                i++;
            }
        }
        return i;
    }

    public int wrongAnswerQuestion(){
        int i = 0;
        
        for (Questionreport question : questionreport) {
            
            if(question.getIscorrect()!= null && !question.getIscorrect()){
                i++;
            }
        }
        return i;
    }
}
