package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Enroll {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer enrollid;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime enrolldate;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime finishdate;
    @OneToMany(mappedBy = "enroll")
    private Set<Sectiondetail> sectiondetailSet;

    public LocalDateTime getFinishdate() {
        return finishdate;
    }

    public void setFinishdate(LocalDateTime finishdate) {
        this.finishdate = finishdate;
    }

    private Double checkout;

    private Boolean processstatus;

    private String emailpaypaladdress;

    public String getEmailpaypaladdress() {
        return emailpaypaladdress;
    }

    public void setEmailpaypaladdress(String emailpaypaladdress) {
        this.emailpaypaladdress = emailpaypaladdress;
    }

    public Enroll() {
    }

    public Enroll(Integer enrollid, Users user, Course course, LocalDateTime enrolldate, Double checkout, Boolean processstatus) {
        this.enrollid = enrollid;
        this.user = user;
        this.course = course;
        this.enrolldate = enrolldate;
        this.checkout = checkout;
        this.processstatus = processstatus;
    }

    public Enroll(Integer enrollid, Users user, Course course, LocalDateTime enrolldate, LocalDateTime finishdate, Set<Sectiondetail> sectiondetailSet, Double checkout, Boolean processstatus, String emailpaypaladdress) {
        this.enrollid = enrollid;
        this.user = user;
        this.course = course;
        this.enrolldate = enrolldate;
        this.finishdate = finishdate;
        this.sectiondetailSet = sectiondetailSet;
        this.checkout = checkout;
        this.processstatus = processstatus;
        this.emailpaypaladdress = emailpaypaladdress;
    }

    public Integer getEnrollid() {
        return enrollid;
    }

    public void setEnrollid(Integer enrollid) {
        this.enrollid = enrollid;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public LocalDateTime getEnrolldate() {
        return enrolldate;
    }

    public void setEnrolldate(LocalDateTime enrolldate) {
        this.enrolldate = enrolldate;
    }

    public Double getCheckout() {
        return checkout;
    }

    public void setCheckout(Double checkout) {
        this.checkout = checkout;
    }

    public Boolean getProcessstatus() {
        return processstatus;
    }

    public void setProcessstatus(Boolean processstatus) {
        this.processstatus = processstatus;
    }

    public Set<Sectiondetail> getSectiondetailSet() {
        return sectiondetailSet;
    }

    public void setSectiondetailSet(Set<Sectiondetail> sectiondetailSet) {
        this.sectiondetailSet = sectiondetailSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enroll enroll = (Enroll) o;
        return Objects.equals(enrollid, enroll.enrollid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enrollid);
    }

    // Thêm getters và setters
}
