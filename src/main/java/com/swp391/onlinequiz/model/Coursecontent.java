package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Coursecontent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer contentId;

    @ManyToOne
    @JoinColumn(name = "coursesectionid")
    private Coursesection coursesection;

    private String contenttitle;

    private String linkvideo;

    private Boolean ispublic;

    public Boolean getIspublic() {
        return ispublic;
    }

    public void setIspublic(Boolean ispublic) {
        this.ispublic = ispublic;
    }


    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime contentdob;

    private Integer contentorder;

    @OneToMany(mappedBy = "coursecontent")
    private Set<Courseprocess> courseprocessSet;

    @OneToMany(mappedBy = "coursecontent")
    private Set<Disscuss> disscussSet;

    @OneToMany(mappedBy = "coursecontent")
    private Set<ContentStatusDetail> contentStatusDetailSet;

    public Coursecontent() {
    }

    public Coursecontent(Integer contentId, Coursesection coursesection, String contenttitle, String linkvideo, LocalDateTime contentdob, Integer contentorder, Set<Courseprocess> courseprocessSet, Set<Disscuss> disscussSet) {
        this.contentId = contentId;
        this.coursesection = coursesection;
        this.contenttitle = contenttitle;
        this.linkvideo = linkvideo;
        this.contentdob = contentdob;
        this.contentorder = contentorder;
        this.courseprocessSet = courseprocessSet;
        this.disscussSet = disscussSet;
    }

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

    public Coursesection getCoursesection() {
        return coursesection;
    }

    public void setCoursesection(Coursesection coursesection) {
        this.coursesection = coursesection;
    }

    public String getContenttitle() {
        return contenttitle;
    }

    public void setContenttitle(String contenttitle) {
        this.contenttitle = contenttitle;
    }

    public String getLinkvideo() {
        return linkvideo;
    }

    public void setLinkvideo(String linkvideo) {
        this.linkvideo = linkvideo;
    }

    public LocalDateTime getContentdob() {
        return contentdob;
    }

    public void setContentdob(LocalDateTime contentdob) {
        this.contentdob = contentdob;
    }

    public Integer getContentorder() {
        return contentorder;
    }

    public void setContentorder(Integer contentorder) {
        this.contentorder = contentorder;
    }

    public Set<Courseprocess> getCourseprocessSet() {
        return courseprocessSet;
    }

    public void setCourseprocessSet(Set<Courseprocess> courseprocessSet) {
        this.courseprocessSet = courseprocessSet;
    }

    public Set<Disscuss> getDisscussSet() {
        return disscussSet;
    }

    public void setDisscussSet(Set<Disscuss> disscussSet) {
        this.disscussSet = disscussSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coursecontent that = (Coursecontent) o;
        return Objects.equals(contentId, that.contentId);
    }


    @Override
    public int hashCode() {
        return Objects.hash(contentId);
    }
}
