package com.swp391.onlinequiz.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.transaction.Transactional;
import lombok.NoArgsConstructor;


@Entity
@NoArgsConstructor
@Transactional
public class Questionreport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "questionid")
    private Question question;

    @ManyToOne
    @JoinColumn(name = "myquizreportid")
    private Myquizreport myquizreport;

    private Boolean iscorrect;

    private Boolean isflag;

    private Integer qorder;

    

    public Integer getQorder() {
        return qorder;
    }

    public void setOrder(Integer qorder) {
        this.qorder = qorder;
    }

    public Boolean getIsflag() {
        return isflag;
    }

    public void setIsflag(Boolean isflag) {
        this.isflag = isflag;
    }

    @OneToMany(mappedBy = "questionreport", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Answerreport> answerreports = new ArrayList<>();

    public Questionreport(Users user, Question question, Myquizreport myquizreport, Boolean iscorrect, Integer qorder) {
        this.user = user;
        this.question = question;
        this.myquizreport = myquizreport;
        this.iscorrect = iscorrect;
        this.qorder = qorder;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Myquizreport getMyquizreport() {
        return myquizreport;
    }

    public void setMyquizreport(Myquizreport myquizreport) {
        this.myquizreport = myquizreport;
    }

    public Boolean getIscorrect() {
        return iscorrect;
    }

    public void setIscorrect(Boolean iscorrect) {
        this.iscorrect = iscorrect;
    }

    public List<Answerreport> getAnswerreports() {
        return answerreports;
    }

    public void setAnswerreports(List<Answerreport> answerreports) {
        this.answerreports = answerreports;
    }

    // Các trường khác để lưu trạng thái tiến trình học tập cho Section, ví dụ: đã xem, chưa xem.

    // Getter và setter

    
}


