package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Transactional
public class Material {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer materialid;

    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course;

    private String materiallink;

    private String materialtitle;

    private String materialname;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime materialdob;

    public Material() {
    }

    public Material(Integer materialid, Course course, String materiallink, String materialtitle, String materialname, LocalDateTime materialdob) {
        this.materialid = materialid;
        this.course = course;
        this.materiallink = materiallink;
        this.materialtitle = materialtitle;
        this.materialname = materialname;
        this.materialdob = materialdob;
    }

    public Integer getMaterialid() {
        return materialid;
    }

    public void setMaterialid(Integer materialid) {
        this.materialid = materialid;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getMateriallink() {
        return materiallink;
    }

    public void setMateriallink(String materiallink) {
        this.materiallink = materiallink;
    }

    public String getMaterialtitle() {
        return materialtitle;
    }

    public void setMaterialtitle(String materialtitle) {
        this.materialtitle = materialtitle;
    }

    public String getMaterialname() {
        return materialname;
    }

    public void setMaterialname(String materialname) {
        this.materialname = materialname;
    }

    public LocalDateTime getMaterialdob() {
        return materialdob;
    }

    public void setMaterialdob(LocalDateTime materialdob) {
        this.materialdob = materialdob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material = (Material) o;
        return Objects.equals(materialid, material.materialid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(materialid);
    }
}
