package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Transactional
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userid;

    private String fullname;

    private String email;

    private Boolean gender;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dob;

    private String address;

    private String username;

    private String password;

    private String role;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDate registerdob;

    private String avatar;

    private Boolean isenabled;

    public Users(Integer userid, String fullname, String email, Boolean gender, LocalDateTime dob, String address, String username, String password, String role, LocalDate registerdob, String avatar, Boolean isenabled, Set<Courseprocess> courseprocessSet, Set<Schedule> scheduleSet, Set<Courseowner> courseownerSet, Set<Enroll> enrollSet, Set<Disscuss> disscussSet, Set<Myquizreport> myquizreportSet, Confirmationtoken confirmationtoken, String resetPasswordToken, Date createdDate) {
        this.userid = userid;
        this.fullname = fullname;
        this.email = email;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.username = username;
        this.password = password;
        this.role = role;
        this.registerdob = registerdob;
        this.avatar = avatar;
        this.isenabled = isenabled;
        this.courseprocessSet = courseprocessSet;
        this.scheduleSet = scheduleSet;
        this.courseownerSet = courseownerSet;
        this.enrollSet = enrollSet;
        this.disscussSet = disscussSet;
        this.myquizreportSet = myquizreportSet;
        this.confirmationtoken = confirmationtoken;
        this.resetPasswordToken = resetPasswordToken;
        this.createdDate = createdDate;
    }

    @OneToMany(mappedBy = "user")
    private Set<Courseprocess> courseprocessSet;

    @OneToMany(mappedBy = "user")
    private Set<Schedule> scheduleSet;

    @OneToMany(mappedBy = "user")
    private Set<Courseowner> courseownerSet;

    @OneToMany(mappedBy = "user")
    private Set<Enroll> enrollSet;
//    @OneToOne(mappedBy = "user")
//    private Feedbackdetail feedbackdetail;

    @OneToMany(mappedBy = "user")
    private Set<Disscuss> disscussSet;

    @OneToMany(mappedBy = "user")
    private Set<Myquizreport> myquizreportSet;

    @OneToOne(mappedBy = "user")
    private Confirmationtoken confirmationtoken;
    @Column(name = "reset_password_token")
    private String resetPasswordToken;
    private Date createdDate;
    @OneToMany(mappedBy = "user")
    private Set<Feedbackdetail> feedbackdetailSet;
    public Users() {
    }

    public Set<Feedbackdetail> getFeedbackdetailSet() {
        return feedbackdetailSet;
    }

    public void setFeedbackdetailSet(Set<Feedbackdetail> feedbackdetailSet) {
        this.feedbackdetailSet = feedbackdetailSet;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public LocalDateTime getDob() {
        return dob;
    }

    public void setDob(LocalDateTime dob) {
        this.dob = dob;
    }

    public void setRegisterdob(LocalDate registerdob) {
        this.registerdob = registerdob;
    }

    public Confirmationtoken getConfirmationtoken() {
        return confirmationtoken;
    }

    public void setConfirmationtoken(Confirmationtoken confirmationtoken) {
        this.confirmationtoken = confirmationtoken;
    }



    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getIsenabled() {
        return isenabled;
    }

    public void setIsenabled(Boolean isenabled) {
        this.isenabled = isenabled;
    }

    public Set<Courseprocess> getCourseprocessSet() {
        return courseprocessSet;
    }

    public void setCourseprocessSet(Set<Courseprocess> courseprocessSet) {
        this.courseprocessSet = courseprocessSet;
    }

    public Set<Schedule> getScheduleSet() {
        return scheduleSet;
    }

    public void setScheduleSet(Set<Schedule> scheduleSet) {
        this.scheduleSet = scheduleSet;
    }

    public Set<Courseowner> getCourseownerSet() {
        return courseownerSet;
    }

    public void setCourseownerSet(Set<Courseowner> courseownerSet) {
        this.courseownerSet = courseownerSet;
    }

    public Set<Enroll> getEnrollSet() {
        return enrollSet;
    }

    public void setEnrollSet(Set<Enroll> enrollSet) {
        this.enrollSet = enrollSet;
    }

//    public Feedbackdetail getFeedbackdetail() {
//        return feedbackdetail;
//    }
//
//    public void setFeedbackdetail(Feedbackdetail feedbackdetail) {
//        this.feedbackdetail = feedbackdetail;
//    }

    public Set<Myquizreport> getMyquizreportSet() {
        return myquizreportSet;
    }

    public void setMyquizreportSet(Set<Myquizreport> myquizreportSet) {
        this.myquizreportSet = myquizreportSet;
    }

    public LocalDate getRegisterdob() {
        return registerdob;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public Set<Disscuss> getDisscussSet() {
        return disscussSet;
    }

    public void setDisscussSet(Set<Disscuss> disscussSet) {
        this.disscussSet = disscussSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return Objects.equals(userid, users.userid);
    }

    public String getFormRole(){
        return role.substring(5);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userid);
    }
}
