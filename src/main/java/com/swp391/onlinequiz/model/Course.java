package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Entity
@Transactional
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer courseid;

    private String coursename;

    @ManyToOne
    @JoinColumn(name = "categoryid")
    private Category category;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createdate;

    private Double courseprice;

    @ManyToOne
    @JoinColumn(name = "courseownerid")
    private Courseowner courseowner;

    private String description;

    private String image;

    private Integer courserating;
    @ManyToOne
    @JoinColumn(name = "statusId")
    private ProgressStatus status;
    //--------------------new
    @ManyToOne
    @JoinColumn(name = "levelid")
    private Level level;

    @ManyToOne
    @JoinColumn(name = "languageid")
    private Language language;

    @OneToMany(mappedBy = "course") // Sử dụng mappedBy để chỉ định trường trong class Learningoutcome
    private Set<Learningoutcome> outcomes;

    public Set<Learningoutcome> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(Set<Learningoutcome> outcomes) {
        this.outcomes = outcomes;
    }

    @OneToMany(mappedBy = "course") // Sử dụng mappedBy để chỉ định trường trong class Learningoutcome
    private Set<Preneeded> preneeds;
    public Set<Preneeded> getPreneeds() {
        return preneeds;
    }

    public void setPreneeds(Set<Preneeded> preneeds) {
        this.preneeds = preneeds;
    }

    //---------------------
    @OneToMany(mappedBy = "course")
    private Set<Feedbackdetail> feedbackdetailSet;

    @OneToMany(mappedBy = "course")
    private Set<Enroll> enrollSet;

    @OneToMany(mappedBy = "course")
    private Set<Disscuss> disscussSet;

    @OneToMany(mappedBy = "course")
    private Set<Material> materialSet;

    @OneToMany(mappedBy = "course")
    private Set<Coursesection> coursesectionSet;

    @OneToMany(mappedBy = "course")
    private Set<Discount> discountSet;

    @OneToMany(mappedBy = "course")
    private Set<Schedule> scheduleSet;

    public Course() {
    }

    public Course(Integer courseid, String coursename, Category category, LocalDateTime createdate, Double courseprice, Courseowner courseowner, String description, String image, Integer courserating, ProgressStatus coursestatus, Set<Feedbackdetail> feedbackdetailSet, Set<Enroll> enrollSet, Set<Disscuss> disscussSet, Set<Material> materialSet, Set<Coursesection> coursesectionSet, Set<Discount> discountSet, Set<Schedule> scheduleSet) {
        this.courseid = courseid;
        this.coursename = coursename;
        this.category = category;
        this.createdate = createdate;
        this.courseprice = courseprice;
        this.courseowner = courseowner;
        this.description = description;
        this.image = image;
        this.courserating = courserating;
        this.status = coursestatus;
        this.feedbackdetailSet = feedbackdetailSet;
        this.enrollSet = enrollSet;
        this.disscussSet = disscussSet;
        this.materialSet = materialSet;
        this.coursesectionSet = coursesectionSet;
        this.discountSet = discountSet;
        this.scheduleSet = scheduleSet;
    }

    public Course(String coursename, Category category, LocalDateTime createdate, Double courseprice, Courseowner courseowner, String description, String image,Integer courserating) {
        this.coursename = coursename;
        this.category = category;
        this.createdate = createdate;
        this.courseprice = courseprice;
        this.courseowner = courseowner;
        this.description = description;
        this.image = image;
        this.courserating = courserating;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LocalDateTime getCreatedate() {
        return createdate;
    }

    public void setCreatedate(LocalDateTime createdate) {
        this.createdate = createdate;
    }

    public Double getCourseprice() {
        return courseprice;
    }

    public void setCourseprice(Double courseprice) {
        this.courseprice = courseprice;
    }

    public Courseowner getCourseowner() {
        return courseowner;
    }

    public void setCourseowner(Courseowner courseowner) {
        this.courseowner = courseowner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getCourserating() {
        return courserating;
    }

    public void setCourserating(Integer courserating) {
        this.courserating = courserating;
    }

    public ProgressStatus getCoursestatus() {
        return status;
    }

    public void setCoursestatus(ProgressStatus coursestatus) {
        this.status = coursestatus;
    }

    public Set<Feedbackdetail> getFeedbackdetailSet() {
        return feedbackdetailSet;
    }

    public void setFeedbackdetailSet(Set<Feedbackdetail> feedbackdetailSet) {
        this.feedbackdetailSet = feedbackdetailSet;
    }

    public Set<Enroll> getEnrollSet() {
        return enrollSet;
    }

    public void setEnrollSet(Set<Enroll> enrollSet) {
        this.enrollSet = enrollSet;
    }

    public Set<Disscuss> getDisscussSet() {
        return disscussSet;
    }

    public void setDisscussSet(Set<Disscuss> disscussSet) {
        this.disscussSet = disscussSet;
    }

    public Set<Material> getMaterialSet() {
        return materialSet;
    }

    public void setMaterialSet(Set<Material> materialSet) {
        this.materialSet = materialSet;
    }

    public Set<Coursesection> getCoursesectionSet() {
        return coursesectionSet;
    }

    public void setCoursesectionSet(Set<Coursesection> coursesectionSet) {
        this.coursesectionSet = coursesectionSet;
    }

    public Set<Discount> getDiscountSet() {
        return discountSet;
    }

    public void setDiscountSet(Set<Discount> discountSet) {
        this.discountSet = discountSet;
    }

    public Set<Schedule> getScheduleSet() {
        return scheduleSet;
    }

    public void setScheduleSet(Set<Schedule> scheduleSet) {
        this.scheduleSet = scheduleSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(courseid, course.courseid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseid);
    }

    public Double getCurrentPrice(){
        Optional<Discount> optionalDiscount = discountSet.stream().filter(discount -> discount.getStartdate().compareTo(LocalDateTime.now()) <= 0 && discount.getEnddate().compareTo(LocalDateTime.now()) >= 0).findFirst();
        if(optionalDiscount.isPresent()){
            return courseprice * (100 - optionalDiscount.get().getDiscountvalue());
        }
        return null;
    }

    public Integer getTotalQuiz(){
        int i = 0;
        for (Coursesection coursesection : coursesectionSet) {
            if(coursesection.getQuizSet() != null){
                i++;
            }
        }
        return i;
    }


    public Integer getStarPercent(int type){
        int i = 0;
        if(feedbackdetailSet.size()==0) return 0;
        for (Feedbackdetail fb : feedbackdetailSet) {
            if(fb.getFbrating() == type){
                i++;
            }
        }
        return (i*100/feedbackdetailSet.size());
    }

    public Integer getStarNumber(int type){
        int i = 0;
        if(feedbackdetailSet.size()==0) return 0;
        for (Feedbackdetail fb : feedbackdetailSet) {
            if(fb.getFbrating() == type){
                ++i;
            }
        }
        return i;
    }

    public Double getAverageRating(){
        Double average = 0.0;
        if(feedbackdetailSet.size()==0) return 0.0;
        for(int i = 1; i < 6; i++){
            average += i*getStarNumber(i);
        }
        return average/feedbackdetailSet.size();
    }

    public String getDuration(){
        try {
            FFprobe fFprobe = new FFprobe();
            long totalDuration = 0;

            for (Coursesection cs : coursesectionSet) {
                for(Coursecontent cc : cs.getCoursecontents()){
                    FFmpegProbeResult probeResult = fFprobe.probe("/img/video-spring-1.mp4");
                    long durationInSeconds = Math.round(probeResult.getFormat().duration);
                    totalDuration += durationInSeconds;
                }

            }

            long hours = totalDuration / 3600;
            // long minutes = (totalDuration % 3600) / 60;
            // long seconds = totalDuration % 60;

            return hours + " hours";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }
    }

    public ProgressStatus getStatus() {
        return status;
    }

    public void setStatus(ProgressStatus status) {
        this.status = status;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseid=" + courseid +
                ", coursename='" + coursename + '\'' +
                ", category=" + category +
                ", createdate=" + createdate +
                ", courseprice=" + courseprice +
                ", courseowner=" + courseowner +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", courserating=" + courserating +
                ", status=" + status +
                ", level=" + level +
                ", language=" + language +
                ", outcomes=" + outcomes +
                ", preneeds=" + preneeds +
                ", feedbackdetailSet=" + feedbackdetailSet +
                ", enrollSet=" + enrollSet +
                ", disscussSet=" + disscussSet +
                ", materialSet=" + materialSet +
                ", coursesectionSet=" + coursesectionSet +
                ", discountSet=" + discountSet +
                ", scheduleSet=" + scheduleSet +
                '}';
    }
}
