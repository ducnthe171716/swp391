package com.swp391.onlinequiz.model;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Transactional
public class Feedbackdetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer feedbackid;

    @ManyToOne
    @JoinColumn(name = "userid")
    private Users user;
    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course;

    @Column(columnDefinition = "TEXT")
    private String feedbackcontent;

    private String feedbackdate;

    private Integer fbrating;

    public Feedbackdetail() {
    }

    public Feedbackdetail(Integer feedbackid, Users user, Course course, String feedbackcontent, String feedbackdate, Integer fbrating) {
        this.feedbackid = feedbackid;
        this.user = user;
        this.course = course;
        this.feedbackcontent = feedbackcontent;
        this.feedbackdate = feedbackdate;
        this.fbrating = fbrating;
    }

    public Feedbackdetail(Users user, Course course, String feedbackcontent, String feedbackdate, Integer fbrating) {
        this.user = user;
        this.course = course;
        this.feedbackcontent = feedbackcontent;
        this.feedbackdate = feedbackdate;
        this.fbrating = fbrating;
    }

    public Integer getFeedbackid() {
        return feedbackid;
    }

    public void setFeedbackid(Integer feedbackid) {
        this.feedbackid = feedbackid;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

        public String getFeedbackcontent() {
        return feedbackcontent;
    }

    public void setFeedbackcontent(String feedbackcontent) {
        this.feedbackcontent = feedbackcontent;
    }

    public String getFeedbackdate() {
        return feedbackdate;
    }

    public void setFeedbackdate(String feedbackdate) {
        this.feedbackdate = feedbackdate;
    }

    public Integer getFbrating() {
        return fbrating;
    }

    public void setFbrating(Integer fbrating) {
        this.fbrating = fbrating;
    }
}
