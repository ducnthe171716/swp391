package com.swp391.onlinequiz.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.transaction.Transactional;

@Entity
@Transactional
public class Learningoutcome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer outcomeid;

    @ManyToOne
    @JoinColumn(name = "courseid") // Tên cột trong bảng Learningoutcome để liên kết với Course
    private Course course;

    @Column(columnDefinition = "TEXT") // Định nghĩa kiểu dữ liệu TEXT cho outcomeitem
    private String outcomeitem;

    public Integer getOutcomeid() {
        return outcomeid;
    }

    public void setOutcomeid(Integer outcomeid) {
        this.outcomeid = outcomeid;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getOutcomeitem() {
        return outcomeitem;
    }

    public void setOutcomeitem(String outcomeitem) {
        this.outcomeitem = outcomeitem;
    }

    public Learningoutcome() {
    }

    public Learningoutcome(Integer outcomeid, Course course, String outcomeitem) {
        this.outcomeid = outcomeid;
        this.course = course;
        this.outcomeitem = outcomeitem;
    }

    public Learningoutcome(Course course, String outcomeitem) {
        this.course = course;
        this.outcomeitem = outcomeitem;
    }

    

}
