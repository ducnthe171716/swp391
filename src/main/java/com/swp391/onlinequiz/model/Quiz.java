package com.swp391.onlinequiz.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Transactional
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer quizid;

    @OneToOne
    @JoinColumn(name = "coursesectionid")
    private Coursesection coursesection;

    private Integer randquestion;

    private Integer time;

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    private Double passpoint;

    public Double getPasspoint() {
        return passpoint;
    }

    public void setPasspoint(Double passpoint) {
        this.passpoint = passpoint;
    }

    public Integer getRandquestion() {
        return randquestion;
    }

    public void setRandquestion(Integer randquestion) {
        this.randquestion = randquestion;
    }

    private String quizcontent;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime quizdob;

    @OneToMany(mappedBy = "quiz")
    private Set<Question> questionSet;

    public Quiz() {
    }

    public Quiz(Integer quizid, Coursesection coursesection, String quizcontent, LocalDateTime quizdob, Set<Question> questionSet, Integer randquestion) {
        this.quizid = quizid;
        this.coursesection = coursesection;
        this.quizcontent = quizcontent;
        this.quizdob = quizdob;
        this.questionSet = questionSet;
        this.randquestion = randquestion;
    }


    public Integer getQuizid() {
        return quizid;
    }

    public void setQuizid(Integer quizid) {
        this.quizid = quizid;
    }

    public Coursesection getCoursesection() {
        return coursesection;
    }

    public void setCoursesection(Coursesection coursesection) {
        this.coursesection = coursesection;
    }

    public String getQuizcontent() {
        return quizcontent;
    }

    public void setQuizcontent(String quizcontent) {
        this.quizcontent = quizcontent;
    }

    public LocalDateTime getQuizdob() {
        return quizdob;
    }

    public void setQuizdob(LocalDateTime quizdob) {
        this.quizdob = quizdob;
    }

    public Set<Question> getQuestionSet() {
        return questionSet;
    }

    public void setQuestionSet(Set<Question> questionSet) {
        this.questionSet = questionSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Quiz quiz = (Quiz) o;
        return Objects.equals(quizid, quiz.quizid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quizid);
    }
}
